package hu.peet.budfoglalo.client.ui.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class UserNameOrEmailExistsException extends RuntimeException {
    public UserNameOrEmailExistsException(final String message) {
        super(message);
    }
}