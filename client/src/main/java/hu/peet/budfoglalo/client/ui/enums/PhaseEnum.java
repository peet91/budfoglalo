package hu.peet.budfoglalo.client.ui.enums;

public enum PhaseEnum {
    BASE_DISTRICT_SELECT,
    EXPANSION_ONE,
    EXPANSION_TWO,
    EXPANSION_THREE,
    EXPANSION_FOUR,
    DIVISION,
    WAR_ONE,
    WAR_TWO,
    WAR_THREE,
    WAR_FOUR,
    WAR_FIVE,
    WAR_SIX,
    GAME_OVER
}
