package hu.peet.budfoglalo.client.ui.presenter.game;

import com.fasterxml.jackson.annotation.JsonProperty;
import hu.peet.budfoglalo.client.ui.enums.PhaseEnum;
import hu.peet.budfoglalo.client.ui.enums.SubPhaseEnum;
import hu.peet.budfoglalo.client.ui.presenter.district.DistrictPresenter;
import hu.peet.budfoglalo.client.ui.presenter.lobby.PlayerPresenter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GameState {
    @JsonProperty
    private List<DistrictPresenter> districts;

    @JsonProperty
    private PhaseEnum currentPhase;

    @JsonProperty
    private SubPhaseEnum currentSubPhase;

    @JsonProperty
    private List<PlayerPresenter> players;

    @JsonProperty
    private Long activePlayerId;

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof GameState)) {
            return false;
        }

        GameState gameState = ((GameState) o);

        for (int i = 0; i < districts.size(); ++i) {
            if (!this.districts.get(i).equals(gameState.getDistricts().get(i))) {
                return false;
            }
        }
        if (!this.currentPhase.equals(gameState.getCurrentPhase())) {
            return false;
        }
        if (!this.currentSubPhase.equals(gameState.getCurrentSubPhase())) {
            return false;
        }
        for (int i = 0; i < players.size(); ++i) {
            if (!this.players.get(i).equals(gameState.getPlayers().get(i))) {
                return false;
            }
        }
        if (!this.activePlayerId.equals(gameState.getActivePlayerId())) {
            return false;
        }
        return true;
    }
}
