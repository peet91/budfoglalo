package hu.peet.budfoglalo.client.ui.presenter.lobby;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivePlayerListPresenter {
    @JsonProperty
    List<PlayerPresenter> activePlayers;
}
