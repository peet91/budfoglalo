package hu.peet.budfoglalo.client.ui.component;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PlayerPanel extends VerticalLayout {
    private Label nameLabel;
    private Label pointsLabel;

    public PlayerPanel(String name, String points) {
        this.getStyle().set("border", "3px");
        nameLabel = new Label(name);
        pointsLabel = new Label("Points: " + points);

        add(nameLabel);
        add(pointsLabel);
    }

    public void updatePoints(String points) {
        pointsLabel = new Label("Points:" + points);
    }
}
