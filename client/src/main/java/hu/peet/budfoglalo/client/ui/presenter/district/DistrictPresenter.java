package hu.peet.budfoglalo.client.ui.presenter.district;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DistrictPresenter {

    @JsonProperty
    private Long id;

    @JsonProperty
    private String name;

    @JsonProperty
    private Boolean base;

    @JsonProperty
    private Integer health;

    @JsonProperty
    private Long value;

    @JsonProperty
    private Long ownerId;

    @JsonProperty
    private Long invaderId;
}
