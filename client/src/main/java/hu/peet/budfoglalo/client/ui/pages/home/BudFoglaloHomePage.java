package hu.peet.budfoglalo.client.ui.pages.home;

import com.vaadin.componentfactory.selectiongrid.SelectionGrid;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.selection.SingleSelect;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import hu.peet.budfoglalo.client.ui.pages.login.BudFoglaloLoginPage;
import hu.peet.budfoglalo.client.ui.presenter.lobby.LobbyPresenter;
import hu.peet.budfoglalo.client.ui.presenter.lobby.PlayerPresenter;
import hu.peet.budfoglalo.client.ui.presenter.user.UserInformationPresenter;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.*;
import java.util.stream.Collectors;

@Route("")
@Theme(value = Lumo.class, variant = Lumo.DARK)
public class BudFoglaloHomePage extends VerticalLayout implements BeforeEnterObserver, BeforeLeaveObserver {

    private final Logger log =
            LoggerFactory.getLogger(this.getClass());

    @Autowired
    private HomePageService homePageService;

    private HorizontalLayout headerLayout;

    private HorizontalLayout mainLayout;

    private VerticalLayout profileContainer;
    private Label userName;
    private Label registered;
    private Label level;
    private Label xp;
    private Button logoutButton;
    private Button profileButton;

    private VerticalLayout availablePlayersContainer;
    private Label playersHeader;
    private Grid<PlayerPresenter> availablePlayersGrid;
    private Set<PlayerPresenter> selectedPlayers;

    private HorizontalLayout availablePlayersActionContainer;
    private Button invitePlayersButton;

    private VerticalLayout lobbiesContainer;
    private Label LobbiesHeader;
    private Grid<LobbyPresenter> lobbyGrid;
    private LobbyPresenter selectedLobby;

    private HorizontalLayout lobbyActionContainer;
    private Button readyButton;
    private Button denyButton;

    private Registration registration;

    public BudFoglaloHomePage() {
        setSizeFull();
        setSpacing(false);
        setPadding(false);
        this.addClassName("budfoglalo.home");
        this.getStyle().set("width", "90%");
        this.getStyle().set("padding", "20px");
        this.getStyle().set("margin-left", "auto");
        this.getStyle().set("margin-right", "auto");
        this.setJustifyContentMode(JustifyContentMode.AROUND);

        initHeader();

        mainLayout = new HorizontalLayout();
        mainLayout.setSizeFull();
        mainLayout.setSpacing(true);
        mainLayout.setPadding(true);

        initProfileContainer();

        initAvailablePlayersContainer();

        initLobbiesContainer();

        this.add(headerLayout);
        this.add(mainLayout);
        mainLayout.add(profileContainer);
        mainLayout.add(availablePlayersContainer);
        mainLayout.add(lobbiesContainer);

        log.info("Instantiated BudFoglaloHomePage");
    }

    private void initHeader() {
        //header component
        headerLayout = new HorizontalLayout();
        headerLayout.setWidthFull();
        headerLayout.setHeight("10%");
        headerLayout.setSpacing(true);
        headerLayout.setPadding(true);
        headerLayout.setAlignSelf(Alignment.CENTER);
        headerLayout.setAlignItems(Alignment.CENTER);
        headerLayout.setJustifyContentMode(JustifyContentMode.CENTER);

        Icon headerIcon = new Icon(VaadinIcon.VAADIN_H);
        headerIcon.setSize("40px");

        Label headerTitle = new Label("BudFoglalo");
        headerTitle.setSizeUndefined();
        headerTitle.getStyle().set("font-size", "30px");

        headerLayout.add(headerIcon);
        headerLayout.add(headerTitle);
    }

    private void initProfileContainer() {
        profileContainer = new VerticalLayout();
        profileContainer.setWidth("20%");
        profileContainer.setAlignSelf(Alignment.CENTER);
    }

    private void initAvailablePlayersContainer() {
        availablePlayersContainer = new VerticalLayout();

        playersHeader = new Label("Available players");
        availablePlayersContainer.setSpacing(true);
        availablePlayersContainer.setPadding(true);
        availablePlayersContainer.setWidth("30%");
        availablePlayersContainer.setHeightFull();

        availablePlayersGrid = new SelectionGrid<>();
        selectedPlayers = new HashSet<>();

        availablePlayersGrid.setSizeFull();
        availablePlayersGrid.addColumn(PlayerPresenter::getId).setHeader("id");
        availablePlayersGrid.addColumn(PlayerPresenter::getUserName).setHeader("username");
        availablePlayersGrid.addColumn(PlayerPresenter::getLevel).setHeader("level");

        availablePlayersGrid.setSelectionMode(Grid.SelectionMode.MULTI);

        availablePlayersGrid.asMultiSelect().addValueChangeListener(check -> {
            if (!check.getValue().isEmpty()) {
                //TODO this should be changed, is we enable 2+ players
                if (check.getValue().size() == 1) {
                    selectedPlayers = new HashSet<>();
                    selectedPlayers.addAll(Collections.singletonList(
                            SerializationUtils.clone(check.getValue().stream().findFirst().get())));
                } else {
                    Notification.show("Currently only one opponent is supported!", 2000,
                            Notification.Position.MIDDLE);
                    availablePlayersGrid.deselectAll();
                    selectedPlayers = new HashSet<>();
                }
            }
        });

        availablePlayersActionContainer = new HorizontalLayout();
        availablePlayersActionContainer.setHeight("20%");

        invitePlayersButton = new Button();
        invitePlayersButton.setText("Invite player(s)");
        invitePlayersButton.addClickListener(buttonClickEvent -> createLobby(selectedPlayers));
        availablePlayersActionContainer.add(invitePlayersButton);

        availablePlayersContainer.add(playersHeader);
        availablePlayersContainer.add(availablePlayersGrid);
        availablePlayersContainer.add(availablePlayersActionContainer);
    }

    private void initLobbiesContainer() {
        lobbiesContainer = new VerticalLayout();

        LobbiesHeader = new Label("Game invites");
        lobbiesContainer.setSpacing(true);
        lobbiesContainer.setPadding(true);
        lobbiesContainer.setWidth("30%");
        lobbiesContainer.setHeightFull();

        lobbyGrid = new Grid<>(LobbyPresenter.class);

        lobbyGrid.setSizeFull();
        lobbyGrid.setColumns("lobbyId", "creatorId", "players");
//        invitesGrid.getColumnByKey("lobbyId").setWidth("50px").setFlexGrow(0);

        lobbyGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        SingleSelect<Grid<LobbyPresenter>, LobbyPresenter> inviteSelect =
                lobbyGrid.asSingleSelect();
        inviteSelect.addValueChangeListener(check -> {
            LobbyPresenter selected = check.getValue();
            if (selected != null) {
                selectedLobby = selected;
            }
        });

        lobbyActionContainer = new HorizontalLayout();
        lobbyActionContainer.setHeight("20%");

        readyButton = new Button();
        readyButton.setText("Ready");
        readyButton.addClickListener(buttonClickEvent -> readyPlayer(selectedLobby.getLobbyId()));

        denyButton = new Button();
        denyButton.setText("Deny");
        denyButton.addClickListener(buttonClickEvent -> denyInvite(selectedLobby.getLobbyId()));

        lobbyActionContainer.add(readyButton);
        lobbyActionContainer.add(denyButton);

        lobbiesContainer.add(LobbiesHeader);
        lobbiesContainer.add(lobbyGrid);
        lobbiesContainer.add(lobbyActionContainer);
    }

    private void sendHeartbeat() {
//        log.info("sending heartbeat");
        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        homePageService.sendHeartbeat(userInformationPresenter.getId());
    }

    private void refreshActivePlayerList() {
//        log.info("refreshing active player grid");
        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        List<PlayerPresenter> activePlayerList =
                homePageService.getActivePlayerList(userInformationPresenter.getId()).getActivePlayers();
        availablePlayersGrid.setItems(activePlayerList);

        if (selectedPlayers != null && !selectedPlayers.isEmpty()) {
            Set<PlayerPresenter> playersToSelect = new HashSet<>();
            activePlayerList.stream().forEach(playerPresenter -> {
                if (selectedPlayers.stream().map(PlayerPresenter::getId).collect(Collectors.toSet())
                        .contains(playerPresenter.getId())) {
                    playersToSelect.add(playerPresenter);
                }
            });
            availablePlayersGrid.asMultiSelect().select(playersToSelect);
        }
    }

    private void refreshLobbyList() {
//        log.info("refreshing game invites grid");
        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        List<LobbyPresenter> lobbyList =
                homePageService.getLobbies(userInformationPresenter.getId()).getLobbies();
        lobbyGrid.setItems(lobbyList);
        if (selectedLobby != null) {
            lobbyGrid.select(selectedLobby);
        }
    }

    private void initProfile() {
        profileContainer.add(new Label("Profile"));

        //getting the user information from the backend, and saving it to the session
        VaadinSession.getCurrent().setAttribute("user", homePageService.getUserProfile());

        //instead of getting the profile info from the backend, we get it from the vaadin session
        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        userName = new Label(userInformationPresenter.getUserName());
        registered = new Label("Registered: " + String.valueOf(userInformationPresenter.getRegistered()));
        level = new Label("Level: " + String.valueOf(userInformationPresenter.getLevel()));
        xp = new Label("XP: " + String.valueOf(userInformationPresenter.getXp()));

        profileContainer.add(userName);
        profileContainer.add(registered);
        profileContainer.add(level);
        profileContainer.add(xp);

        logoutButton = new Button();
        logoutButton.setText("Logout");
        logoutButton.addClickListener(buttonClickEvent -> logout());
        profileContainer.add(logoutButton);

        profileButton = new Button();
        profileButton.setText("Edit profile");
        profileButton.addClickListener(buttonClickEvent ->
                profileButton.getUI().ifPresent(ui -> ui.navigate("profile")));
        profileContainer.add(profileButton);
    }


    private void logout() {
        UI.getCurrent().getSession().close();
    }

    private void createLobby(Set<PlayerPresenter> opponents) {
        if (opponents.isEmpty()) {
            Notification.show("Select at least one player!", 2000, Notification.Position.MIDDLE);
            ;
            return;
        }
        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        List<Long> opponentIds = opponents.stream().map(PlayerPresenter::getId).collect(Collectors.toList());

        ResponseEntity<UUID> lobbyCreationResponse =
                homePageService.createLobby(userInformationPresenter.getId(), opponentIds);

        if (lobbyCreationResponse.getStatusCode().equals(HttpStatus.OK)) {
            Notification.show("Game invitation sent to player(s): "
                    + opponents.stream().map(PlayerPresenter::getUserName).collect(Collectors.toList()))
                    .setPosition(Notification.Position.MIDDLE);
        }
    }

    private void readyPlayer(UUID uuid) {
        if (uuid == null) {
            Notification.show("Select an invite!").setPosition(Notification.Position.MIDDLE);
            return;
        }

        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        HttpStatus acceptInviteResponse = homePageService.playerReady(userInformationPresenter.getId(), uuid);

        if (acceptInviteResponse.equals(HttpStatus.OK)) {
            Notification.show("Invitation accepted for game: " + uuid, 2000, Notification.Position.MIDDLE);
        }
    }

    private void denyInvite(UUID uuid) {
        if (uuid == null) {
            Notification.show("Select an invite!").setPosition(Notification.Position.MIDDLE);
            return;
        }

        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        HttpStatus denyInviteResponse = homePageService.playerDeny(userInformationPresenter.getId(), uuid);

        if (denyInviteResponse.equals(HttpStatus.OK)) {
            Notification.show("Invitation denied for game: " + uuid)
                    .setPosition(Notification.Position.MIDDLE);
        }
    }

    private void checkGameSessionReady() {

        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        UUID gameSessionId = homePageService.checkGameIsReady(userInformationPresenter.getId());
        if (gameSessionId != null) {
            VaadinSession.getCurrent().setAttribute("gameSessionId", gameSessionId);
            getUI().ifPresent(ui -> ui.navigate("game"));
        }
    }

    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        if (VaadinSession.getCurrent().getAttribute("token") == null) {
            beforeEnterEvent.forwardTo(BudFoglaloLoginPage.class);
        }
    }

    @Override
    public void beforeLeave(BeforeLeaveEvent event) {
        // Turn off  Refresh
        if (registration != null) {
            registration.remove();
        }
    }

    @Override
    public void onAttach(AttachEvent event) {
        initProfile();
        refreshActivePlayerList();
        refreshLobbyList();

        UI.getCurrent().setPollInterval(3000);
        registration = UI.getCurrent().addPollListener(pollEvent -> {
            checkGameSessionReady();
            sendHeartbeat();
            refreshActivePlayerList();
            refreshLobbyList();
        });
    }
}
