package hu.peet.budfoglalo.client.ui.presenter.district;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InteractableDistrictPresenter {

    @JsonProperty
    Set<Long> interactableDistricts;
}
