package hu.peet.budfoglalo.client.ui.presenter.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor
public class UserListPresenter {
    @JsonProperty
    private List<UserInformationPresenter> userList;
}
