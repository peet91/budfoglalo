package hu.peet.budfoglalo.client.ui.pages.login;

import com.vaadin.flow.component.notification.Notification;
import hu.peet.budfoglalo.client.ui.presenter.user.LoginRequest;
import hu.peet.budfoglalo.client.ui.security.JwtResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class LoginPageService {
    private final Logger log =
            LoggerFactory.getLogger(this.getClass());

    RestTemplate restTemplate = new RestTemplate();

    @Value("${budfoglalo.app.backend.url}")
    private String backendUrl;

    @Value("${budfoglalo.app.backend.port}")
    private String backendPort;

    private static final String URL_POSTFIX = "/api/user/login";


    public ResponseEntity<JwtResponse> createLoginRequest(LoginRequest LoginRequest) {
        String loginEndpointUrl = backendUrl + ":" + backendPort + URL_POSTFIX;
        try {
            log.info("Login request sent:" + LoginRequest.toString());

            ResponseEntity<JwtResponse> response =
                    restTemplate.postForEntity(loginEndpointUrl, LoginRequest, JwtResponse.class);
            return response;
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            Notification.show("login error").setPosition(Notification.Position.MIDDLE);
            return null;
        }
    }
}
