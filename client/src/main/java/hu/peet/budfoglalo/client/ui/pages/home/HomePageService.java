package hu.peet.budfoglalo.client.ui.pages.home;

import com.vaadin.flow.server.VaadinSession;
import hu.peet.budfoglalo.client.ui.presenter.lobby.ActivePlayerListPresenter;
import hu.peet.budfoglalo.client.ui.presenter.lobby.CreateLobbyRequest;
import hu.peet.budfoglalo.client.ui.presenter.lobby.LobbiesPresenter;
import hu.peet.budfoglalo.client.ui.presenter.lobby.PlayerReadyRequest;
import hu.peet.budfoglalo.client.ui.presenter.user.UserInformationPresenter;
import hu.peet.budfoglalo.client.ui.presenter.user.UserListPresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.UUID;

@Service
public class HomePageService {
    private final Logger log =
            LoggerFactory.getLogger(this.getClass());

    @Value("${budfoglalo.app.backend.url}")
    private String backendUrl;

    @Value("${budfoglalo.app.backend.port}")
    private String backendPort;

    private static final String PROFILE_URL_POSTFIX = "/api/user/profile";
    private static final String USER_LIST_URL_POSTFIX = "/api/user/list";

    private static final String ACTIVE_PLAYER_LIST_URL_POSTFIX = "/api/game/active_players";
    private static final String GAME_INVITE_URL_POSTFIX = "/api/game/create_lobby";
    private static final String INVITE_LIST_URL_POSTFIX = "/api/game/lobbies/{playerId}";
    private static final String GAME_ACCEPT_URL_POSTFIX = "/api/game/ready";
    private static final String GAME_DENY_URL_POSTFIX = "/api/game/deny";

    private static final String GAME_ID_URL_POSTFIX = "/api/game/get";
    private static final String HEARTBEAT_URL_POSTFIX = "/api/game/heartbeat/{playerId}";

    public void sendHeartbeat(Long playerId) {
        RestTemplate restTemplate = new RestTemplate();
        String heartbeatEndpointUrl = backendUrl + ":" + backendPort + HEARTBEAT_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(heartbeatEndpointUrl);

        restTemplate.postForLocation(builder.buildAndExpand(playerId).toUriString(), httpEntity);
    }

    public UserInformationPresenter getUserProfile() {
        RestTemplate restTemplate = new RestTemplate();
        String profileEndpointUrl = backendUrl + ":" + backendPort + PROFILE_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        ResponseEntity<UserInformationPresenter> responseEntity =
                restTemplate.exchange(profileEndpointUrl, HttpMethod.GET,
                        httpEntity, UserInformationPresenter.class);
        return responseEntity.getBody();
    }

    public ActivePlayerListPresenter getActivePlayerList(long playerId) {
        RestTemplate restTemplate = new RestTemplate();
        String activePlayerListEndpointUrl = backendUrl + ":" + backendPort + ACTIVE_PLAYER_LIST_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(activePlayerListEndpointUrl).queryParam("playerId", playerId);

        ResponseEntity<ActivePlayerListPresenter> responseEntity =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET,
                        httpEntity, ActivePlayerListPresenter.class);

        return responseEntity.getBody();
    }

    public UserListPresenter getUserList() {
        RestTemplate restTemplate = new RestTemplate();
        String userListEndpointUrl = backendUrl + ":" + backendPort + USER_LIST_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        ResponseEntity<UserListPresenter> responseEntity =
                restTemplate.exchange(userListEndpointUrl, HttpMethod.GET,
                        httpEntity, UserListPresenter.class);
        return responseEntity.getBody();
    }

    public ResponseEntity createLobby(long playerId, List<Long> opponentIds) {
        RestTemplate restTemplate = new RestTemplate();
        String gameInviteEndpointUrl = backendUrl + ":" + backendPort + GAME_INVITE_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());

        CreateLobbyRequest gameInviteRequest = new CreateLobbyRequest();
        gameInviteRequest.setPlayerId(playerId);
        gameInviteRequest.setOpponentIds(opponentIds);

        HttpEntity<CreateLobbyRequest> httpEntity = new HttpEntity(gameInviteRequest, httpHeaders);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(gameInviteEndpointUrl);

        ResponseEntity<UUID> responseEntity = restTemplate.exchange(
                builder.build().toUriString(), HttpMethod.POST, httpEntity, UUID.class);

        return responseEntity;
    }

    public LobbiesPresenter getLobbies(long playerId) {
        RestTemplate restTemplate = new RestTemplate();
        String gameInvitesEndpointUrl = backendUrl + ":" + backendPort + INVITE_LIST_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(gameInvitesEndpointUrl);

        ResponseEntity<LobbiesPresenter> responseEntity =
                restTemplate.exchange(builder.buildAndExpand(playerId).toUriString(), HttpMethod.GET,
                        httpEntity, LobbiesPresenter.class);

        return responseEntity.getBody();
    }

    public HttpStatus playerReady(Long playerId, UUID inviteId) {
        RestTemplate restTemplate = new RestTemplate();
        String acceptGameInviteEndpointUrl = backendUrl + ":" + backendPort + GAME_ACCEPT_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());

        PlayerReadyRequest acceptGameInviteRequest = new PlayerReadyRequest(playerId, inviteId);

        HttpEntity<PlayerReadyRequest> httpEntity = new HttpEntity(acceptGameInviteRequest, httpHeaders);

        ResponseEntity responseEntity = restTemplate.exchange(
                acceptGameInviteEndpointUrl, HttpMethod.POST, httpEntity, Object.class);

        return responseEntity.getStatusCode();
    }

    public HttpStatus playerDeny(Long playerId, UUID inviteId) {
        RestTemplate restTemplate = new RestTemplate();
        String denyGameInviteEndpointUrl = backendUrl + ":" + backendPort + GAME_DENY_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());

        PlayerReadyRequest denyGameInviteRequest = new PlayerReadyRequest(playerId, inviteId);

        HttpEntity<PlayerReadyRequest> httpEntity = new HttpEntity(denyGameInviteRequest, httpHeaders);

        ResponseEntity responseEntity = restTemplate.exchange(
                denyGameInviteEndpointUrl, HttpMethod.POST, httpEntity, Object.class);

        return responseEntity.getStatusCode();
    }

    public UUID checkGameIsReady(Long playerId) {
        RestTemplate restTemplate = new RestTemplate();
        String gameReadyEndpointUrl = backendUrl + ":" + backendPort + GAME_ID_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(gameReadyEndpointUrl);

        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        ResponseEntity<UUID> responseEntity = null;

        try {
            responseEntity = restTemplate.exchange(builder.queryParam("playerId", playerId).build().toUriString(),
                    HttpMethod.GET, httpEntity, UUID.class);
            return responseEntity.getBody();
        } catch (HttpClientErrorException e) {
            return null;
        }
    }
}
