package hu.peet.budfoglalo.client.ui.presenter.question;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GuessingQuestionPresenter {

    @JsonProperty
    private Long questionId;

    @JsonProperty
    private String question;
}
