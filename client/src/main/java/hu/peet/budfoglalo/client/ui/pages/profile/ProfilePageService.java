package hu.peet.budfoglalo.client.ui.pages.profile;

import com.vaadin.flow.server.VaadinSession;
import hu.peet.budfoglalo.client.ui.presenter.user.ProfileUpdateRequest;
import hu.peet.budfoglalo.client.ui.presenter.user.UserInformationPresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ProfilePageService {
    private final Logger log =
            LoggerFactory.getLogger(this.getClass());

    @Value("${budfoglalo.app.backend.url}")
    private String backendUrl;

    @Value("${budfoglalo.app.backend.port}")
    private String backendPort;

    private static final String PROFILE_URL_POSTFIX = "/api/user/profile";

    private static final String PROFILE_UPDATE_URL_POSTFIX = "/api/user/profile/update";

    public UserInformationPresenter getUserProfile() {
        RestTemplate restTemplate = new RestTemplate();
        String profileEndpointUrl = backendUrl + ":" + backendPort + PROFILE_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        ResponseEntity<UserInformationPresenter> responseEntity =
                restTemplate.exchange(profileEndpointUrl, HttpMethod.GET,
                        httpEntity, UserInformationPresenter.class);
        return responseEntity.getBody();
    }

    public HttpStatus updateUserProfile(ProfileUpdateRequest profileUpdateRequest) {
        RestTemplate restTemplate = new RestTemplate();
        String profileEndpointUrl = backendUrl + ":" + backendPort + PROFILE_UPDATE_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());

        HttpEntity httpEntity = new HttpEntity(profileUpdateRequest, httpHeaders);

        ResponseEntity responseEntity =
                restTemplate.postForEntity(profileEndpointUrl, httpEntity, Object.class);

        return responseEntity.getStatusCode();
    }
}
