package hu.peet.budfoglalo.client.ui.presenter.lobby;

import com.fasterxml.jackson.annotation.JsonProperty;
import hu.peet.budfoglalo.client.ui.presenter.game.GameState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlayerPresenter implements Serializable {
    @JsonProperty
    private Long id;

    @JsonProperty
    private String userName;

    @JsonProperty
    private Integer level;

    @JsonProperty
    private Long points;

    private LocalTime lastHeartbeat;

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof PlayerPresenter)) {
            return false;
        }

        PlayerPresenter playerPresenter = ((PlayerPresenter) o);
        if (!id.equals(playerPresenter.getId())) {
            return false;
        }
        if (!userName.equals(playerPresenter.getUserName())) {
            return false;
        }
        if (!level.equals(playerPresenter.getLevel())) {
            return false;
        }
        if (!points.equals(playerPresenter.getPoints())) {
            return false;
        }
        return true;
    }
}
