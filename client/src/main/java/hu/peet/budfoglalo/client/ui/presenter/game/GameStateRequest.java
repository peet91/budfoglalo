package hu.peet.budfoglalo.client.ui.presenter.game;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GameStateRequest {
    @JsonProperty
    private UUID uuid;

    @JsonProperty
    private Long playerId;
}
