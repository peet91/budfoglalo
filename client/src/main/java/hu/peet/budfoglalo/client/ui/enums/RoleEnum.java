package hu.peet.budfoglalo.client.ui.enums;

public enum RoleEnum {
    ROLE_USER,
    ROLE_ADMIN
}
