package hu.peet.budfoglalo.client.ui.presenter.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequest {
    @NotNull
    @NotEmpty
    @JsonProperty
    private String username;

    @NotNull
    @NotEmpty
    @JsonProperty
    private String password;
}
