package hu.peet.budfoglalo.client.ui;

import com.vaadin.flow.server.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;

public class BudfoglaloServlet extends VaadinServlet implements SessionInitListener, SessionDestroyListener {
    private static final Logger log =
            LoggerFactory.getLogger(BudfoglaloServlet.class);

    @Override
    protected void servletInitialized() throws ServletException {
        super.servletInitialized();
        getService().addSessionInitListener(this);
        getService().addSessionDestroyListener(this);
        log.info("BudfoglaloServlet initialized!");
    }

    @Override
    public void sessionInit(SessionInitEvent sessionInitEvent) throws ServiceException {
        getService().addSessionInitListener(this);
        getService().addSessionDestroyListener(this);
        log.info("Budfoglalo Session initialized!");
    }

    @Override
    public void sessionDestroy(SessionDestroyEvent sessionDestroyEvent) {
        log.info("Budfoglalo Session destroyed!");
    }
}
