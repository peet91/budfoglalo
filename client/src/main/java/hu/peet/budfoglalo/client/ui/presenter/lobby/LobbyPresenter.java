package hu.peet.budfoglalo.client.ui.presenter.lobby;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LobbyPresenter {
    private UUID lobbyId;

    private Long creatorId;

    private Map<Long, Boolean> players;
}
