package hu.peet.budfoglalo.client.ui.pages.login;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import hu.peet.budfoglalo.client.ui.presenter.user.LoginRequest;
import hu.peet.budfoglalo.client.ui.security.JwtResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

@Route("login")
@Theme(value = Lumo.class, variant = Lumo.DARK)
public class BudFoglaloLoginPage extends VerticalLayout {
    private final Logger log =
            LoggerFactory.getLogger(this.getClass());

    @Autowired
    private LoginPageService loginPageService;

    private HorizontalLayout headerLayout;

    private FormLayout loginForm;

    Binder<LoginRequest> loginBinder;

    private TextField usernameField;

    private PasswordField passwordField;

    private HorizontalLayout actions;

    private Button loginButton;

    private Button registrationButton;

    public BudFoglaloLoginPage() {
        setSpacing(false);
        setPadding(false);
        this.addClassName("budfoglalo.login");
        this.getStyle().set("width", "40%");
        this.getStyle().set("padding", "20px");
        this.getStyle().set("margin-left", "auto");
        this.getStyle().set("margin-right", "auto");
        this.setAlignSelf(Alignment.CENTER);
        this.setAlignItems(Alignment.CENTER);
        this.setJustifyContentMode(JustifyContentMode.CENTER);

        initHeader();

        initLoginForm();

        this.add(headerLayout);
        this.add(loginForm);

        log.info("Instantiated BudFoglaloLoginPage");
    }

    private void initHeader() {
        //header component
        headerLayout = new HorizontalLayout();
        headerLayout.setWidthFull();
        headerLayout.setHeight("10%");
        headerLayout.setSpacing(true);
        headerLayout.setPadding(true);
        headerLayout.setAlignSelf(Alignment.CENTER);
        headerLayout.setAlignItems(Alignment.CENTER);
        headerLayout.setJustifyContentMode(JustifyContentMode.CENTER);

        Icon headerIcon = new Icon(VaadinIcon.VAADIN_H);
        headerIcon.setSize("40px");

        Label headerTitle = new Label("BudFoglalo");
        headerTitle.setSizeUndefined();
        headerTitle.getStyle().set("font-size", "30px");

        headerLayout.add(headerIcon);
        headerLayout.add(headerTitle);
    }

    private void initLoginForm() {
        //form component
        loginForm = new FormLayout();
        loginForm.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1)); //TODO
        loginForm.getStyle().set("padding", "10px");
        loginForm.getStyle().set("horizontal-spacing", "20px");

        usernameField = new TextField();
        usernameField.setLabel("Username");
        usernameField.setPlaceholder("NoobMaster69");
        usernameField.addClassName("username");

        passwordField = new PasswordField("Password");
        passwordField.setPlaceholder("WAR MACHINE ROX");

        loginBinder = new Binder<>(LoginRequest.class);

        loginBinder.forField(usernameField).asRequired("Username is required!").withValidator(
                userName -> userName.length() >= 3,
                "Username should be at least 3 characters long!").bind(
                LoginRequest::getUsername, LoginRequest::setUsername);

        loginBinder.forField(passwordField).asRequired("Password is required!").bind(
                LoginRequest::getPassword, LoginRequest::setPassword);

        actions = new HorizontalLayout();
        actions.getStyle().set("align-items", "center");
        actions.getStyle().set("margin-top", "20%");
        actions.getStyle().set("margin-left", "10%");
        actions.getStyle().set("margin-right", "10%");
        actions.getStyle().set("padding", "5%");

        loginButton = new Button("Login");
        loginButton.getStyle().set("align", "right");
        loginButton.addClickListener(buttonClickEvent -> loginButtonClick());
        loginButton.addClickShortcut(Key.ENTER);

        registrationButton = new Button("Registration");
        registrationButton.getStyle().set("align", "left");
        registrationButton.addClickListener(buttonClickEvent ->
                registrationButton.getUI().ifPresent(ui -> ui.navigate("registration")));


        actions.add(loginButton);
        actions.add(registrationButton);

        loginForm.add(usernameField);
        loginForm.add(passwordField);
        loginForm.add(actions);
    }

    private void loginButtonClick() {
        BinderValidationStatus<LoginRequest> status = loginBinder.validate();

        if (status.isOk()) {
            userLogin();
        } else log.error("Form validation error!");
    }

    private void userLogin() {
        LoginRequest loginRequest = new LoginRequest();

        try {
            loginBinder.writeBean(loginRequest);
            loginRequest.setPassword(loginRequest.getPassword());
        } catch (ValidationException e) {
            e.printStackTrace();
        }

        ResponseEntity loginResponse = loginPageService.createLoginRequest(loginRequest);

        if (loginResponse != null) {
            VaadinSession.getCurrent().setAttribute("token", ((JwtResponse) loginResponse.getBody()).getToken());
            getUI().ifPresent(ui -> ui.navigate(""));
        }
    }
}
