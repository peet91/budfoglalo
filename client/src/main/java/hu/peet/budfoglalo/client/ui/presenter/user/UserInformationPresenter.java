package hu.peet.budfoglalo.client.ui.presenter.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class UserInformationPresenter {
    @JsonProperty
    private long id;

    @JsonProperty
    private String userName;

    @JsonProperty
    private String email;

    @JsonProperty
    private LocalDate registered;

    @JsonProperty
    private String firstName;

    @JsonProperty
    private String lastName;

    @JsonProperty
    private long xp;

    @JsonProperty
    private int level;
}
