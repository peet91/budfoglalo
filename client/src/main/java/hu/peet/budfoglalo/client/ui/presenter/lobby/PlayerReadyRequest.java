package hu.peet.budfoglalo.client.ui.presenter.lobby;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlayerReadyRequest {

    @JsonProperty
    private Long playerId;

    @JsonProperty()
    private UUID uuid;
}
