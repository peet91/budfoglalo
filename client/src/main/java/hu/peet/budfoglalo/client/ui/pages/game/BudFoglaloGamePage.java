package hu.peet.budfoglalo.client.ui.pages.game;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.svg.Svg;
import com.vaadin.flow.component.svg.elements.Path;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import hu.peet.budfoglalo.client.ui.component.District;
import hu.peet.budfoglalo.client.ui.component.DistrictPath;
import hu.peet.budfoglalo.client.ui.component.PlayerPanel;
import hu.peet.budfoglalo.client.ui.enums.PhaseEnum;
import hu.peet.budfoglalo.client.ui.enums.SubPhaseEnum;
import hu.peet.budfoglalo.client.ui.pages.home.BudFoglaloHomePage;
import hu.peet.budfoglalo.client.ui.pages.home.HomePageService;
import hu.peet.budfoglalo.client.ui.pages.login.BudFoglaloLoginPage;
import hu.peet.budfoglalo.client.ui.presenter.game.GameState;
import hu.peet.budfoglalo.client.ui.presenter.lobby.PlayerPresenter;
import hu.peet.budfoglalo.client.ui.presenter.question.GuessingQuestionPresenter;
import hu.peet.budfoglalo.client.ui.presenter.question.TriviaQuestionPresenter;
import hu.peet.budfoglalo.client.ui.presenter.user.UserInformationPresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Route("game")
@Theme(value = Lumo.class, variant = Lumo.DARK)
public class BudFoglaloGamePage extends VerticalLayout implements BeforeEnterObserver, BeforeLeaveObserver {
    private final Logger log =
            LoggerFactory.getLogger(this.getClass());

    @Autowired
    private HomePageService homePageService;

    @Autowired
    private GamePageService gamePageService;

    private HorizontalLayout headerLayout;

    private HorizontalLayout mainLayout;

    private VerticalLayout playerContainer;
    private PlayerPanel playerPanel;

    private Button leaveGameButton;

    //TEMP to interact
    //removed due to game map implementation
//    private TextField districtField;
//    private String districtFieldValue;
//    private Button interactButton;

    private VerticalLayout gameContainer;
    private Svg gameCanvas;
    private VerticalLayout opponentsContainer;
    private PlayerPanel opponentPanel1;
    private PlayerPanel opponentPanel2;

    private Registration registration;

    Dialog triviaDialog = null;
    Dialog guessingDialog = null;
    Dialog gameOverDialog = null;

    GameState lastState;

    public BudFoglaloGamePage() {
        setSizeFull();
        setSpacing(false);
        setPadding(false);
        this.addClassName("budfoglalo.game");
        this.getStyle().set("width", "90%");
        this.getStyle().set("padding", "20px");
        this.getStyle().set("margin-left", "auto");
        this.getStyle().set("margin-right", "auto");
        this.setJustifyContentMode(JustifyContentMode.AROUND);

        initHeader();

        mainLayout = new HorizontalLayout();
        mainLayout.setSizeFull();
        mainLayout.setSpacing(true);
        mainLayout.setPadding(true);

        initPlayerContainer();

        initGameContainer();

        initOpponentsContainer();

        this.add(headerLayout);
        this.add(mainLayout);
        mainLayout.add(playerContainer);
        mainLayout.add(gameContainer);
        mainLayout.add(opponentsContainer);

        log.info("Instantiated BudfoglaloGamePage");
    }

    private void initHeader() {
        //header component
        headerLayout = new HorizontalLayout();
        headerLayout.setWidthFull();
        headerLayout.setHeight("10%");
        headerLayout.setSpacing(true);
        headerLayout.setPadding(true);
        headerLayout.setAlignSelf(Alignment.CENTER);
        headerLayout.setAlignItems(Alignment.CENTER);
        headerLayout.setJustifyContentMode(JustifyContentMode.CENTER);

        Icon headerIcon = new Icon(VaadinIcon.VAADIN_H);
        headerIcon.setSize("40px");

        Label headerTitle = new Label("BudFoglalo");
        headerTitle.setSizeUndefined();
        headerTitle.getStyle().set("font-size", "30px");

        headerLayout.add(headerIcon);
        headerLayout.add(headerTitle);
    }

    private void initPlayerContainer() {
        playerContainer = new VerticalLayout();
        playerContainer.setWidth("20%");
        playerContainer.setHeight("50%");
        playerContainer.setAlignSelf(Alignment.CENTER);

        leaveGameButton = new Button("Leave game");
        leaveGameButton.addClickListener(buttonClickEvent -> leaveGame());

        playerContainer.add(leaveGameButton);

        //removed due to game map implementation
        /*
        districtField = new TextField("District: ");
        districtField.addValueChangeListener(valueChangeEvent -> districtFieldValue = valueChangeEvent.getValue());
        interactButton = new Button("interact");
        interactButton.addClickListener(buttonClickEvent -> {
            if (districtFieldValue != null && !districtFieldValue.isEmpty()) {
                interact(Long.valueOf(districtFieldValue));
            } else log.error("districtId is null or empty upon interact!");
        });
        */
    }

    private void initGameContainer() {
        gameContainer = new VerticalLayout();
        gameContainer.setWidth("60%");
        gameContainer.setHeightFull();
        gameContainer.setAlignSelf(Alignment.CENTER);
    }

    private void initOpponentsContainer() {
        opponentsContainer = new VerticalLayout();
        opponentsContainer.setWidth("20%");
        opponentsContainer.setHeight("50%");
        opponentsContainer.setAlignSelf(Alignment.CENTER);
    }

    private void sendHeartbeat() {
//        log.info("sending heartbeat");
        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        UUID gameSessionId = (UUID) VaadinSession.getCurrent().getAttribute("gameSessionId");

        gamePageService.sendHeartbeat(userInformationPresenter.getId(), gameSessionId);
    }

    private void leaveGame() {
        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        UUID gameSessionId = (UUID) VaadinSession.getCurrent().getAttribute("gameSessionId");

        gamePageService.leave(userInformationPresenter.getId(), gameSessionId);
    }

    private void refreshGame() {
        //instead of getting the profile info from the backend, we get it from the vaadin session
        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        UUID gameSessionId = (UUID) VaadinSession.getCurrent().getAttribute("gameSessionId");

        GameState gameState = gamePageService.getGameState(userInformationPresenter.getId(), gameSessionId);

        if (!gameState.equals(lastState)) {
//            log.info("gameState changed");
            lastState = gameState;
            if (triviaDialog != null && triviaDialog.isOpened()) {
                triviaDialog.close();
            }
            triviaDialog = null;
            if (guessingDialog != null && guessingDialog.isOpened()) {
                guessingDialog.close();
            }
            guessingDialog = null;


            String playerPoints = String.valueOf(gameState.getPlayers().stream().filter(playerPresenter ->
                    playerPresenter.getId() == userInformationPresenter.getId()).findFirst().get().getPoints());

            playerContainer.removeAll();

            if (gameState.getActivePlayerId().equals(userInformationPresenter.getId())) {
                playerContainer.getStyle().set("background-color", "#4682B4");
                opponentsContainer.getStyle().remove("background-color");
            } else {
                playerContainer.getStyle().remove("background-color");
                opponentsContainer.getStyle().set("background-color", "#4682B4");
            }

            playerContainer.add(new PlayerPanel(userInformationPresenter.getUserName(), playerPoints));
            playerContainer.add(leaveGameButton);

            //removed due to game map implementation
//        playerContainer.add(districtField);
//        playerContainer.add(interactButton);

            String opponentName = gameState.getPlayers().stream().filter(playerPresenter ->
                    playerPresenter.getId() != userInformationPresenter.getId()).findFirst().get().getUserName();

            String opponentPoints = String.valueOf(gameState.getPlayers().stream().filter(playerPresenter ->
                    playerPresenter.getId() != userInformationPresenter.getId()).findFirst().get().getPoints());

            opponentsContainer.removeAll();
            opponentsContainer.add(new PlayerPanel(opponentName, opponentPoints));

            gameContainer.removeAll();

            gameContainer.add(new Label("Current Phase: " + gameState.getCurrentPhase().toString()));
            gameContainer.add(new Label("Current SubPhase: " + gameState.getCurrentSubPhase().toString()));

            refreshGameCanvas(gameState);
            gameContainer.add(gameCanvas);
        }
    }

    private void refreshGameCanvas(GameState gameState) {
        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        UUID gameSessionId = (UUID) VaadinSession.getCurrent().getAttribute("gameSessionId");

        Set<Long> interactableDistrictIds = gamePageService.getInteractableDistrictIds(
                userInformationPresenter.getId(), gameSessionId).getInteractableDistricts();

        gameCanvas = new Svg();
        gameCanvas.setSizeFull();

        Path border = new Path("border", DistrictPath.BORDER.getPath());
        border.setStroke("black", 3);

        gameCanvas.add(border);

        gameState.getDistricts().forEach(district -> {
            District districtSvg = new District(userInformationPresenter, district,
                    interactableDistrictIds.contains(district.getId()));
            districtSvg.setDraggable(true);
            gameCanvas.add(districtSvg);
        });


        Path danube = new Path("danube", DistrictPath.DANUBE.getPath());
        danube.setStroke("black", 0);
        danube.setFillColor("blue");

        gameCanvas.add(danube);
        if (userInformationPresenter.getId() == gameState.getActivePlayerId()) {
            gameCanvas.addDragStartListener(svgDragStartEvent -> {
                interact(Long.valueOf(svgDragStartEvent.getElement().getId()));
            });
        } else gameCanvas.getStyle().set("enabled", "false");
    }

    private void interact(Long districtId) {
        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        UUID gameSessionId = (UUID) VaadinSession.getCurrent().getAttribute("gameSessionId");

        if (HttpStatus.OK.equals(gamePageService.interact(userInformationPresenter.getId(), gameSessionId, districtId))) {
            Notification.show("Targeted district: "
                    + districtId, 1000, Notification.Position.MIDDLE);
        } else {
            Notification.show("Can't target district: "
                    + districtId, 1000, Notification.Position.MIDDLE);
        }

    }

    private void checkQuestion() {
        //instead of getting the profile info from the backend, we get it from the vaadin session
        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        UUID gameSessionId = (UUID) VaadinSession.getCurrent().getAttribute("gameSessionId");

        GameState gameState = gamePageService.getGameState(userInformationPresenter.getId(), gameSessionId);

        if (gameState.getCurrentSubPhase().equals(SubPhaseEnum.QUESTION_TRIVIA) && triviaDialog == null) {
            initTriviaDialog();
        } else if (gameState.getCurrentSubPhase().equals(SubPhaseEnum.QUESTION_GUESSING) && guessingDialog == null) {
            initGuessingDialog();
        }
    }

    private void initTriviaDialog() {
        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        UUID gameSessionId = (UUID) VaadinSession.getCurrent().getAttribute("gameSessionId");

        TriviaQuestionPresenter triviaQuestion =
                gamePageService.getTriviaQuestion(gameSessionId);

        triviaDialog = new Dialog();
        triviaDialog.setCloseOnOutsideClick(false);
        triviaDialog.setCloseOnEsc(false);
        VerticalLayout dialogContainer = new VerticalLayout();

        dialogContainer.add(new Label(triviaQuestion.getQuestion()));
        Button answer1 = new Button(triviaQuestion.getAnswers().get(0));
        answer1.addClickListener(buttonClickEvent -> {
            gamePageService.answerTriviaQuestion(userInformationPresenter.getId(),
                    gameSessionId, triviaQuestion.getQuestionId(), triviaQuestion.getAnswers().get(0));
            triviaDialog.close();
        });

        Button answer2 = new Button(triviaQuestion.getAnswers().get(1));
        answer2.addClickListener(buttonClickEvent -> {
            gamePageService.answerTriviaQuestion(userInformationPresenter.getId(),
                    gameSessionId, triviaQuestion.getQuestionId(), triviaQuestion.getAnswers().get(1));
            triviaDialog.close();
        });

        Button answer3 = new Button(triviaQuestion.getAnswers().get(2));
        answer3.addClickListener(buttonClickEvent -> {
            gamePageService.answerTriviaQuestion(userInformationPresenter.getId(),
                    gameSessionId, triviaQuestion.getQuestionId(), triviaQuestion.getAnswers().get(2));
            triviaDialog.close();
        });

        dialogContainer.add(answer1);
        dialogContainer.add(answer2);
        dialogContainer.add(answer3);

        triviaDialog.add(dialogContainer);
        triviaDialog.open();
    }

    private void initGuessingDialog() {
        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        UUID gameSessionId = (UUID) VaadinSession.getCurrent().getAttribute("gameSessionId");

        GuessingQuestionPresenter guessingQuestion =
                gamePageService.getGuessingQuestion(gameSessionId);

        guessingDialog = new Dialog();
        guessingDialog.setCloseOnOutsideClick(false);
        guessingDialog.setCloseOnEsc(false);
        VerticalLayout dialogContainer = new VerticalLayout();

        dialogContainer.add(new Label(guessingQuestion.getQuestion()));
        TextField answer = new TextField("Answer: ");
        Button submitButton = new Button("Submit");
        submitButton.addClickListener(buttonClickEvent -> {
            log.info("answer value: " + answer.getValue());
            if (answer.getValue() != null && !answer.isEmpty()) {
                try {
                    Long answerValue = Long.valueOf(answer.getValue());
                } catch (NumberFormatException e) {
                    answer.clear();
                    Notification.show("Enter numbers only!", 1000, Notification.Position.MIDDLE);
                    return;
                }

                gamePageService.answerGuessingQuestion(userInformationPresenter.getId(),
                        gameSessionId, guessingQuestion.getQuestionId(), answer.getValue());
                guessingDialog.close();
            } else log.error("answer value is null or empty upon interact!");
        });
        submitButton.addClickShortcut(Key.ENTER);

        dialogContainer.add(answer);
        dialogContainer.add(submitButton);

        guessingDialog.add(dialogContainer);
        guessingDialog.open();
    }

    private GameState checkGameAvailable() {
        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        UUID gameSessionId = (UUID) VaadinSession.getCurrent().getAttribute("gameSessionId");

        GameState gameState;

        try {
            gameState = gamePageService.getGameState(userInformationPresenter.getId(), gameSessionId);
        } catch (HttpClientErrorException e) {
            log.info("gamaState not found, redirecting to homepage");
            getUI().ifPresent(ui -> ui.navigate(BudFoglaloHomePage.class));
            return null;
        }

        return gameState;
    }

    private boolean checkGameOver() {
        GameState gameState = checkGameAvailable();

        if (gameState != null && gameState.getCurrentPhase().equals(PhaseEnum.GAME_OVER)) {
            //Possible outcomes are here:
            if (gameState.getPlayers().size() < 2) {
                initGameOverDialog("Other player(s) left the game. :'(", null);
            } else if (!lastState.getCurrentPhase().equals(PhaseEnum.WAR_SIX)) {
                refreshGame();
                initGameOverDialog("Player named " + gameState.getPlayers().stream().filter(
                        playerPresenter -> playerPresenter.getId().equals(
                                gameState.getActivePlayerId())).findFirst().get().getUserName() + " conquered the whole city," +
                        " eliminating all who stood in the way...", null);
            } else if (lastState.getCurrentPhase().equals(PhaseEnum.WAR_SIX)) {
                refreshGame();
                initGameOverDialog("We ran out of war phases, and no one could conquer the whole city.",
                        getPlayerRanking(gameState));
            } else {
                log.error("Unknown reason for game over!" + gameState.toString());
            }
            return true;
        }
        return false;
    }

    private List<String> getPlayerRanking(GameState gameState) {
        List<String> playerRankings = gameState.getPlayers().stream().sorted(
                Comparator.comparing(PlayerPresenter::getPoints).reversed())
                .map(player -> player.getUserName() + " - Points: " + player.getPoints())
                .collect(Collectors.toList());
        return playerRankings;
    }

    private void initGameOverDialog(String message, List<String> players) {
        gameOverDialog = new Dialog();
        gameOverDialog.setCloseOnOutsideClick(true);
        gameOverDialog.setCloseOnEsc(true);

        VerticalLayout dialogContainer = new VerticalLayout();
        dialogContainer.add(new Label("Game Over!"));
        dialogContainer.add(new Label(message));
        if (players != null) {
            dialogContainer.add(new Label("Rankings:"));
            players.forEach(player -> dialogContainer.add(new Label(player)));
        }

        gameOverDialog.addDialogCloseActionListener(dialogCloseActionEvent ->

        {
            gameOverDialog.close();
            getUI().ifPresent(ui -> ui.navigate(BudFoglaloHomePage.class));
        });

        gameOverDialog.add(dialogContainer);

        gameOverDialog.open();

        if (registration != null) {
            registration.remove();
        }

    }

    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
//        log.info("beforeEnter fires, reroute to login");
        if (VaadinSession.getCurrent().getAttribute("token") == null) {
            beforeEnterEvent.forwardTo(BudFoglaloLoginPage.class);
            return;
        }
        if (VaadinSession.getCurrent().getAttribute("user") == null) {
            log.info("userInformationPresenter is null, redirecting to homepage");
            beforeEnterEvent.forwardTo(BudFoglaloHomePage.class);
            return;
        }
        if (checkGameAvailable() == null) {
            beforeEnterEvent.forwardTo(BudFoglaloHomePage.class);
        }
    }

    @Override
    public void beforeLeave(BeforeLeaveEvent event) {
        VaadinSession.getCurrent().setAttribute("gameSessionId", null);
        // Turn off  Refresh
        if (registration != null) {
            registration.remove();
        }
    }

    @Override
    public void onAttach(AttachEvent event) {
        //getting the user information from the backend, and saving it to the session
        VaadinSession.getCurrent().setAttribute("user", homePageService.getUserProfile());

        sendHeartbeat();
        refreshGame();
        checkQuestion();

        UI.getCurrent().setPollInterval(3000);
        registration = UI.getCurrent().addPollListener(pollEvent -> {
            if (!checkGameOver()) {
                refreshGame();
                sendHeartbeat();
                checkQuestion();
            }
        });
    }
}
