package hu.peet.budfoglalo.client.ui.enums;

public enum SubPhaseEnum {
    TARGET,
    QUESTION_TRIVIA,
    QUESTION_GUESSING,
}
