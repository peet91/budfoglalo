package hu.peet.budfoglalo.client.ui.pages.profile;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import hu.peet.budfoglalo.client.ui.pages.home.BudFoglaloHomePage;
import hu.peet.budfoglalo.client.ui.pages.login.BudFoglaloLoginPage;
import hu.peet.budfoglalo.client.ui.presenter.user.ProfileUpdateRequest;
import hu.peet.budfoglalo.client.ui.presenter.user.UserInformationPresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

@Route("profile")
@Theme(value = Lumo.class, variant = Lumo.DARK)
public class BudFoglaloProfilePage extends VerticalLayout implements BeforeEnterObserver {
    private final Logger log =
            LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ProfilePageService profilePageService;

    private HorizontalLayout headerLayout;

    private FormLayout profileForm;

    Binder<ProfileUpdateRequest> profileBinder;

    private TextField usernameField;
    private EmailField emailField;
    private TextField firstNameField;
    private TextField lastNameField;
    private HorizontalLayout actions;
    private Button saveButton;
    private Button backButton;

    public BudFoglaloProfilePage() {
        setSpacing(false);
        setPadding(false);
        this.addClassName("budfoglalo.profile");
        this.getStyle().set("width", "40%");
        this.getStyle().set("padding", "20px");
        this.getStyle().set("margin-left", "auto");
        this.getStyle().set("margin-right", "auto");
        this.setAlignSelf(Alignment.CENTER);
        this.setAlignItems(Alignment.CENTER);
        this.setJustifyContentMode(JustifyContentMode.CENTER);

        initHeader();

        initProfileForm();

        this.add(headerLayout);
        this.add(profileForm);

        log.info("Instantiated BudFoglaloProfilePage");
    }

    private void initHeader() {
        //header component
        headerLayout = new HorizontalLayout();
        headerLayout.setWidthFull();
        headerLayout.setHeight("10%");
        headerLayout.setSpacing(true);
        headerLayout.setPadding(true);
        headerLayout.setAlignSelf(Alignment.CENTER);
        headerLayout.setAlignItems(Alignment.CENTER);
        headerLayout.setJustifyContentMode(JustifyContentMode.CENTER);

        Icon headerIcon = new Icon(VaadinIcon.VAADIN_H);
        headerIcon.setSize("40px");

        Label headerTitle = new Label("BudFoglalo");
        headerTitle.setSizeUndefined();
        headerTitle.getStyle().set("font-size", "30px");

        headerLayout.add(headerIcon);
        headerLayout.add(headerTitle);
    }

    private void initProfileForm() {
        //instead of getting the profile info from the backend, we get it from the vaadin session
        UserInformationPresenter userInformationPresenter =
                (UserInformationPresenter) VaadinSession.getCurrent().getAttribute("user");

        //TODO dirty hack :'(
        if (VaadinSession.getCurrent().getAttribute("user") == null) {
            getUI().ifPresent(ui -> ui.navigate(BudFoglaloLoginPage.class));
            profileForm = new FormLayout();
            return;
        }

        profileForm = new FormLayout();
        profileForm.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1)); //TODO
        profileForm.getStyle().set("padding", "10px");
        profileForm.getStyle().set("horizontal-spacing", "20px");

        usernameField = new TextField();
        usernameField.setLabel("Username");
        usernameField.setClassName("username");
        usernameField.setValue(userInformationPresenter.getUserName());
        usernameField.setEnabled(false);

        emailField = new EmailField();
        emailField.setLabel("Email");
        emailField.setClassName("email");
        emailField.setValue(userInformationPresenter.getEmail());

        firstNameField = new TextField();
        firstNameField.setLabel("First name");
        firstNameField.setClassName("firstname");
        firstNameField.setValue(userInformationPresenter.getFirstName());

        lastNameField = new TextField();
        lastNameField.setLabel("Last Name");
        lastNameField.setClassName("lastname");
        lastNameField.setValue(userInformationPresenter.getLastName());

        profileBinder = new Binder<>(ProfileUpdateRequest.class);

        profileBinder.forField(usernameField).asRequired("Username is required!").withValidator(
                userName -> userName.length() >= 3,
                "UserName should be at least 3 characters long!").bind(
                ProfileUpdateRequest::getUsername, ProfileUpdateRequest::setUsername);

        profileBinder.forField(emailField).asRequired("E-mail is required!").withValidator(
                new EmailValidator("Invalid e-mail format!")).bind(
                ProfileUpdateRequest::getEmail, ProfileUpdateRequest::setEmail);

        profileBinder.forField(firstNameField).bind(ProfileUpdateRequest::getFirstName, ProfileUpdateRequest::setFirstName);
        profileBinder.forField(lastNameField).bind(ProfileUpdateRequest::getLastName, ProfileUpdateRequest::setLastName);

        actions = new HorizontalLayout();
        actions.getStyle().set("align-items", "center");
        actions.getStyle().set("margin-top", "20%");
        actions.getStyle().set("margin-left", "10%");
        actions.getStyle().set("margin-right", "10%");
        actions.getStyle().set("padding", "5%");

        saveButton = new Button("Save");
        saveButton.getStyle().set("align", "left");
        saveButton.addClickListener(buttonClickEvent -> saveButtonClick());

        backButton = new Button("Back");
        backButton.getStyle().set("align", "right");
        backButton.addClickListener(buttonClickEvent -> backButtonClick());


        actions.add(saveButton);
        actions.add(backButton);

        profileForm.add(usernameField);
        profileForm.add(emailField);
        profileForm.add(firstNameField);
        profileForm.add(lastNameField);
        profileForm.add(actions);
    }

    private void saveButtonClick() {
        BinderValidationStatus<ProfileUpdateRequest> status = profileBinder.validate();

        if (status.isOk()) {
            saveProfile();
        } else log.error("Form validation error!");
    }

    private void saveProfile() {
        ProfileUpdateRequest profileUpdateRequest = new ProfileUpdateRequest();

        try {
            profileBinder.writeBean(profileUpdateRequest);
            if (profilePageService.updateUserProfile(profileUpdateRequest).equals(HttpStatus.OK)) {
                Notification.show("Profile updated").setPosition(Notification.Position.MIDDLE);
            }
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    private void backButtonClick() {
        profileBinder.removeBean();
        backButton.getUI().ifPresent(ui -> ui.navigate(BudFoglaloHomePage.class));
    }

    private void initProfile() {
        //getting the user information from the backend, and saving it to the session
        VaadinSession.getCurrent().setAttribute("user", profilePageService.getUserProfile());
    }

    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        if (VaadinSession.getCurrent().getAttribute("token") == null) {
            beforeEnterEvent.forwardTo(BudFoglaloLoginPage.class);
        }
    }

    @Override
    public void onAttach(AttachEvent event) {
        initProfile();
    }
}
