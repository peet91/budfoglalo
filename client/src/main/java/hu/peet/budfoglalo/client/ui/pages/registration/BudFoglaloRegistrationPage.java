package hu.peet.budfoglalo.client.ui.pages.registration;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.data.validator.RegexpValidator;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import hu.peet.budfoglalo.client.ui.presenter.user.RegistrationRequest;
import hu.peet.budfoglalo.client.ui.presenter.user.UserRegistrationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


@Route("registration")
@Theme(value = Lumo.class, variant = Lumo.DARK)
public class BudFoglaloRegistrationPage extends VerticalLayout {
    private final Logger log =
            LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RegistrationPageService registrationPageService;

    private HorizontalLayout headerLayout;

    private FormLayout registrationForm;

    Binder<RegistrationRequest> registrationBinder;

    private TextField usernameField;
    private EmailField emailField;
    private PasswordField passwordField;
    private HorizontalLayout actions;
    private Button registrationButton;
    private Button loginButton;

    public BudFoglaloRegistrationPage() {
        setSpacing(false);
        setPadding(false);
        this.addClassName("budfoglalo.registration");
        this.getStyle().set("width", "40%");
        this.getStyle().set("padding", "20px");
        this.getStyle().set("margin-left", "auto");
        this.getStyle().set("margin-right", "auto");
        this.setAlignSelf(Alignment.CENTER);
        this.setAlignItems(Alignment.CENTER);
        this.setJustifyContentMode(JustifyContentMode.CENTER);

        initHeader();

        initRegistrationForm();

        this.add(headerLayout);
        this.add(registrationForm);

        log.info("Instantiated BudFoglaloRegistrationPage");
    }

    private void initHeader() {
        //header component
        headerLayout = new HorizontalLayout();
        headerLayout.setWidthFull();
        headerLayout.setHeight("10%");
        headerLayout.setSpacing(true);
        headerLayout.setPadding(true);
        headerLayout.setAlignSelf(Alignment.CENTER);
        headerLayout.setAlignItems(Alignment.CENTER);
        headerLayout.setJustifyContentMode(JustifyContentMode.CENTER);

        Icon headerIcon = new Icon(VaadinIcon.VAADIN_H);
        headerIcon.setSize("40px");

        Label headerTitle = new Label("BudFoglalo");
        headerTitle.setSizeUndefined();
        headerTitle.getStyle().set("font-size", "30px");

        headerLayout.add(headerIcon);
        headerLayout.add(headerTitle);
    }

    private void initRegistrationForm() {
        //form component
        registrationForm = new FormLayout();
        registrationForm.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1)); //TODO
        registrationForm.getStyle().set("padding", "10px");
        registrationForm.getStyle().set("horizontal-spacing", "20px");

        usernameField = new TextField();
        usernameField.setLabel("Username");
        usernameField.setPlaceholder("NoobMaster69");
        usernameField.addClassName("username");

        emailField = new EmailField("E-mail");
        emailField.setPlaceholder("tony.stark@starkindustries.com");

        passwordField = new PasswordField("Password");
        passwordField.setPlaceholder("WAR MACHINE ROX");

        registrationBinder = new Binder<>(RegistrationRequest.class);

        registrationBinder.forField(usernameField).asRequired("Username is required!").withValidator(
                userName -> userName.length() >= 3,
                "UserName should be at least 3 characters long!").bind(
                RegistrationRequest::getUsername, RegistrationRequest::setUsername);

        registrationBinder.forField(emailField).asRequired("E-mail is required!").withValidator(
                new EmailValidator("Invalid e-mail format!")).bind(
                RegistrationRequest::getEmail, RegistrationRequest::setEmail);

        registrationBinder.forField(passwordField).asRequired("Password is required!").withValidator(
                new RegexpValidator("Password must contain at least eight characters," +
                        " at least one number and both lower and uppercase letters and special characters",
                        "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-.]).{8,}$")
        ).bind(RegistrationRequest::getPassword, RegistrationRequest::setPassword);

        actions = new HorizontalLayout();
        actions.getStyle().set("align-items", "center");
        actions.getStyle().set("margin-top", "20%");
        actions.getStyle().set("margin-left", "10%");
        actions.getStyle().set("margin-right", "10%");
        actions.getStyle().set("padding", "5%");

        registrationButton = new Button("Register");
        registrationButton.getStyle().set("align", "left");
        registrationButton.addClickListener(buttonClickEvent -> registrationButtonClick());
        registrationButton.addClickShortcut(Key.ENTER);

        loginButton = new Button("Login");
        loginButton.getStyle().set("align", "right");
        loginButton.addClickListener(buttonClickEvent ->
                loginButton.getUI().ifPresent(ui -> ui.navigate("login")));

        actions.add(registrationButton);
        actions.add(loginButton);

        registrationForm.add(usernameField);
        registrationForm.add(emailField);
        registrationForm.add(passwordField);
        registrationForm.add(actions);
    }

    private void registrationButtonClick() {
        BinderValidationStatus<RegistrationRequest> status = registrationBinder.validate();

        if (status.isOk()) {
            registerUser();
        } else log.error("Form validation error!");
    }

    private void registerUser() {
        RegistrationRequest registrationRequest = new RegistrationRequest();

        try {
            registrationBinder.writeBean(registrationRequest);
            registrationRequest.setPassword(registrationRequest.getPassword());
        } catch (ValidationException e) {
            e.printStackTrace();
        }

        UserRegistrationResponse response = registrationPageService.createRegistrationRequest(registrationRequest);

        if (response != null) {
            Notification.show("Registration successful with id: " +
                    response.getId() + " now you can log in.", 5000, Notification.Position.MIDDLE);
            //TODO figure out how could we wait until the notification disappear, and navigate after that
            getUI().ifPresent(ui -> ui.navigate("login"));
        }
    }
}
