package hu.peet.budfoglalo.client.ui.pages.registration;

import com.vaadin.flow.component.notification.Notification;
import hu.peet.budfoglalo.client.ui.exception.UserNameOrEmailExistsException;
import hu.peet.budfoglalo.client.ui.presenter.user.RegistrationRequest;
import hu.peet.budfoglalo.client.ui.presenter.user.UserRegistrationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class RegistrationPageService {
    private final Logger log =
            LoggerFactory.getLogger(this.getClass());

    RestTemplate restTemplate = new RestTemplate();

    @Value("${budfoglalo.app.backend.url}")
    private String backendUrl;

    @Value("${budfoglalo.app.backend.port}")
    private String backendPort;

    private static final String URL_POSTFIX = "/api/user/register";


    public UserRegistrationResponse createRegistrationRequest(RegistrationRequest registrationRequest) {
        String registrationEndpointUrl = backendUrl + ":" + backendPort + URL_POSTFIX;
        try {
            ResponseEntity<UserRegistrationResponse> responseEntity =
                    restTemplate.postForEntity(registrationEndpointUrl,
                            registrationRequest, UserRegistrationResponse.class);
            if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
                log.info("Registration successful sent:" + registrationRequest.toString());
                return responseEntity.getBody();
            } else {
                log.info("response status nok: " + responseEntity.getStatusCode());
                return null;
            }
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            Notification.show("username or e-mail address already exists").setPosition(Notification.Position.MIDDLE);
            return null;
        }
    }
}
