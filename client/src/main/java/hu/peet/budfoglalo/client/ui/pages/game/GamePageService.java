package hu.peet.budfoglalo.client.ui.pages.game;

import com.vaadin.flow.server.VaadinSession;
import hu.peet.budfoglalo.client.ui.presenter.district.InteractableDistrictPresenter;
import hu.peet.budfoglalo.client.ui.presenter.game.GameState;
import hu.peet.budfoglalo.client.ui.presenter.game.GameStateUpdateRequest;
import hu.peet.budfoglalo.client.ui.presenter.game.LeaveGameRequest;
import hu.peet.budfoglalo.client.ui.presenter.question.AnswerRequestPresenter;
import hu.peet.budfoglalo.client.ui.presenter.question.GuessingQuestionPresenter;
import hu.peet.budfoglalo.client.ui.presenter.question.TriviaQuestionPresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class GamePageService {

    private final Logger log =
            LoggerFactory.getLogger(this.getClass());

    @Value("${budfoglalo.app.backend.url}")
    private String backendUrl;

    @Value("${budfoglalo.app.backend.port}")
    private String backendPort;

    private static final String HEARTBEAT_URL_POSTFIX = "/api/game/heartbeat/{playerId}";
    private static final String GAME_STATE_URL_POSTFIX = "/api/game/state";
    private static final String GAME_INTERACT_URL_POSTFIX = "/api/game/interact";
    private static final String LEAVE_GAME_URL_POSTFIX = "/api/game/leave";

    private static final String INTERACTABLE_DISTRICT_URL_POSTFIX = "/api/district/interactable";

    private static final String TRIVIA_QUESTION_URL_POSTFIX = "/api/question/trivia/get";
    private static final String TRIVIA_ANSWER_URL_POSTFIX = "/api/question/trivia/answer";

    private static final String GUESSING_QUESTION_URL_POSTFIX = "/api/question/guessing/get";
    private static final String GUESSING_ANSWER_URL_POSTFIX = "/api/question/guessing/answer";

    public void sendHeartbeat(Long playerId, UUID gameSessionId) {
        RestTemplate restTemplate = new RestTemplate();
        String heartbeatEndpointUrl = backendUrl + ":" + backendPort + HEARTBEAT_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(heartbeatEndpointUrl);

        restTemplate.postForLocation(builder.queryParam("gameSessionId", gameSessionId).
                buildAndExpand(playerId).toUriString(), httpEntity);
    }

    public GameState getGameState(Long playerId, UUID gameSessionId) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String gameStateEndpointUrl = backendUrl + ":" + backendPort + GAME_STATE_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(gameStateEndpointUrl);

        ResponseEntity<GameState> responseEntity =
                restTemplate.exchange(builder.
                                queryParam("playerId", playerId).
                                queryParam("gameSessionId", gameSessionId).build().toUriString(),
                        HttpMethod.GET, httpEntity, GameState.class);

        return responseEntity.getBody();
    }

    public HttpStatus interact(Long playerId, UUID gameSessionId, long districtId) {
        RestTemplate restTemplate = new RestTemplate();
        String gameStateEndpointUrl = backendUrl + ":" + backendPort + GAME_INTERACT_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());

        GameStateUpdateRequest gameStateUpdateRequest =
                new GameStateUpdateRequest(gameSessionId, playerId, districtId);
        HttpEntity httpEntity = new HttpEntity(gameStateUpdateRequest, httpHeaders);

        ResponseEntity<GameState> responseEntity = null;

        try {
            responseEntity =
                    restTemplate.exchange(gameStateEndpointUrl, HttpMethod.POST, httpEntity, GameState.class);
            return responseEntity.getStatusCode();
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            return e.getStatusCode();
        }
    }

    public void leave(Long playerId, UUID gameSessionId) {
        RestTemplate restTemplate = new RestTemplate();
        String gameStateEndpointUrl = backendUrl + ":" + backendPort + LEAVE_GAME_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());

        LeaveGameRequest leaveGameRequest = new LeaveGameRequest(gameSessionId, playerId);

        HttpEntity httpEntity = new HttpEntity(leaveGameRequest, httpHeaders);

        ResponseEntity responseEntity =
                restTemplate.exchange(gameStateEndpointUrl, HttpMethod.POST, httpEntity, Object.class);
    }

    public TriviaQuestionPresenter getTriviaQuestion(UUID gameSessionId) {
        RestTemplate restTemplate = new RestTemplate();
        String gameStateEndpointUrl = backendUrl + ":" + backendPort + TRIVIA_QUESTION_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(gameStateEndpointUrl);

        ResponseEntity<TriviaQuestionPresenter> responseEntity =
                restTemplate.exchange(builder.
                                queryParam("gameSessionId", gameSessionId).build().toUriString(),
                        HttpMethod.GET, httpEntity, TriviaQuestionPresenter.class);

        return responseEntity.getBody();
    }

    public void answerTriviaQuestion(Long playerId, UUID gameSessionId, Long questionId, String answer) {
        RestTemplate restTemplate = new RestTemplate();
        String gameStateEndpointUrl = backendUrl + ":" + backendPort + TRIVIA_ANSWER_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());

        AnswerRequestPresenter answerRequestPresenter =
                new AnswerRequestPresenter(playerId, gameSessionId, questionId, answer);

        HttpEntity httpEntity = new HttpEntity(answerRequestPresenter, httpHeaders);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(gameStateEndpointUrl);

        ResponseEntity responseEntity =
                restTemplate.exchange(builder.
                                queryParam("playerId", playerId).
                                queryParam("gameSessionId", gameSessionId).build().toUriString(),
                        HttpMethod.POST, httpEntity, Object.class);
    }

    public GuessingQuestionPresenter getGuessingQuestion(UUID gameSessionId) {
        RestTemplate restTemplate = new RestTemplate();
        String gameStateEndpointUrl = backendUrl + ":" + backendPort + GUESSING_QUESTION_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(gameStateEndpointUrl);

        ResponseEntity<GuessingQuestionPresenter> responseEntity =
                restTemplate.exchange(builder.
                                queryParam("gameSessionId", gameSessionId).build().toUriString(),
                        HttpMethod.GET, httpEntity, GuessingQuestionPresenter.class);

        return responseEntity.getBody();
    }

    public void answerGuessingQuestion(Long playerId, UUID gameSessionId, Long questionId, String answer) {
        RestTemplate restTemplate = new RestTemplate();
        String gameStateEndpointUrl = backendUrl + ":" + backendPort + GUESSING_ANSWER_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());

        AnswerRequestPresenter answerRequestPresenter =
                new AnswerRequestPresenter(playerId, gameSessionId, questionId, answer);

        HttpEntity httpEntity = new HttpEntity(answerRequestPresenter, httpHeaders);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(gameStateEndpointUrl);

        ResponseEntity responseEntity =
                restTemplate.exchange(builder.
                                queryParam("playerId", playerId).
                                queryParam("gameSessionId", gameSessionId).build().toUriString(),
                        HttpMethod.POST, httpEntity, Object.class);
    }

    public InteractableDistrictPresenter getInteractableDistrictIds(Long playerId, UUID gameSessionId) {
        RestTemplate restTemplate = new RestTemplate();
        String gameStateEndpointUrl = backendUrl + ":" + backendPort + INTERACTABLE_DISTRICT_URL_POSTFIX;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(VaadinSession.getCurrent().getAttribute("token").toString());
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(gameStateEndpointUrl);

        ResponseEntity<InteractableDistrictPresenter> responseEntity =
                restTemplate.exchange(builder.
                                queryParam("playerId", playerId).
                                queryParam("gameSessionId", gameSessionId).build().toUriString(),
                        HttpMethod.GET, httpEntity, InteractableDistrictPresenter.class);

        return responseEntity.getBody();
    }
}
