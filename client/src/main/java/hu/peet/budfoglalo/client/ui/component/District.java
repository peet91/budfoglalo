package hu.peet.budfoglalo.client.ui.component;

import com.vaadin.flow.component.svg.elements.Path;
import hu.peet.budfoglalo.client.ui.presenter.district.DistrictPresenter;
import hu.peet.budfoglalo.client.ui.presenter.user.UserInformationPresenter;

public class District extends Path {
    public District(String id, String path) {
        super(id, path);
    }

    public District(UserInformationPresenter userInformationPresenter,
                    DistrictPresenter districtPresenter, boolean interactable) {
        super(String.valueOf(districtPresenter.getId()),
                DistrictPath.valueOf(districtPresenter.getName()).getPath());

        // own base
        if (userInformationPresenter.getId() == districtPresenter.getOwnerId() && districtPresenter.getBase() &&
                0L == districtPresenter.getInvaderId()) {
            this.setFillColor("#006400");
        }
        // enemy base
        else if (0L != districtPresenter.getOwnerId() && districtPresenter.getBase() &&
                0L == districtPresenter.getInvaderId()) {
            this.setFillColor("#8B0000");
        }
        // own district
        else if (userInformationPresenter.getId() == districtPresenter.getOwnerId()
                && 0L == districtPresenter.getInvaderId()) {
            this.setFillColor("#008000");
        }
        // enemy district
        else if (userInformationPresenter.getId() != districtPresenter.getOwnerId() &&
                0L != districtPresenter.getOwnerId() && !districtPresenter.getBase() &&
                0L == districtPresenter.getInvaderId()) {
            this.setFillColor("#FF0000");
        }
        // invaded by player
        else if (0L == districtPresenter.getOwnerId() &&
                userInformationPresenter.getId() == districtPresenter.getInvaderId()) {
            this.setFillColor("#3CB371");
        }
        // invaded by an enemy
        else if (0L == districtPresenter.getOwnerId() && 0L != districtPresenter.getInvaderId() &&
                userInformationPresenter.getId() != districtPresenter.getInvaderId()) {
            this.setFillColor("#FF6347");
        }
        // enemy district invaded by player
        else if (userInformationPresenter.getId() != districtPresenter.getOwnerId() &&
                userInformationPresenter.getId() == districtPresenter.getInvaderId()) {
            this.setFillColor("#FF7F50");
        }
        // player district invaded by enemy
        else if (0L != districtPresenter.getInvaderId() &&
                userInformationPresenter.getId() == districtPresenter.getOwnerId()) {
            this.setFillColor("#FFA500");
        }
        // free
        else if (0L == districtPresenter.getOwnerId() && 0L == districtPresenter.getInvaderId()) {
            if (interactable) {
                this.setFillColor("#90EE90");
            } else {
                this.setFillColor("#FFFAF0");
            }
        } else {
            this.setFillColor("black");
        }

        this.setStroke("black", 1);
    }
}
