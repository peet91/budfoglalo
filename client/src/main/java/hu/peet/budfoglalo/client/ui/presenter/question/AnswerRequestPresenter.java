package hu.peet.budfoglalo.client.ui.presenter.question;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnswerRequestPresenter {

    @JsonProperty
    private Long playerId;

    @JsonProperty
    private UUID uuid;

    @JsonProperty
    private Long questionId;

    @JsonProperty
    private String answer;
}
