-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: budfoglalo
-- ------------------------------------------------------
-- Server version	5.7.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `district`
--

DROP TABLE IF EXISTS `district`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `district` (
  `id` bigint(20) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `district`
--

LOCK TABLES `district` WRITE;
/*!40000 ALTER TABLE `district` DISABLE KEYS */;
INSERT INTO `district` VALUES (1,'I'),(2,'II'),(3,'III'),(4,'IV'),(5,'V'),(6,'VI'),(7,'VII'),(8,'VIII'),(9,'IX'),(10,'X'),(11,'XI'),(12,'XII'),(13,'XIII'),(14,'XIV'),(15,'XV'),(16,'XVI'),(17,'XVII'),(18,'XVIII'),(19,'XIX'),(20,'XX'),(21,'XXI'),(22,'XXII'),(23,'XXIII');
/*!40000 ALTER TABLE `district` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `district_neighbours`
--

DROP TABLE IF EXISTS `district_neighbours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `district_neighbours` (
  `id` bigint(20) NOT NULL,
  `district_id` bigint(20) NOT NULL,
  `neighbour_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `district_id_idx` (`district_id`),
  KEY `neighbour_id_idx` (`neighbour_id`),
  CONSTRAINT `district_id` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `neighbour_id` FOREIGN KEY (`neighbour_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `district_neighbours`
--

LOCK TABLES `district_neighbours` WRITE;
/*!40000 ALTER TABLE `district_neighbours` DISABLE KEYS */;
INSERT INTO `district_neighbours` VALUES (1,1,11),(2,1,12),(3,1,2),(4,1,5),(5,2,1),(6,2,12),(7,2,3),(8,2,13),(9,2,5),(10,3,2),(11,3,4),(12,3,13),(13,4,3),(14,4,15),(15,4,13),(16,4,14),(17,5,1),(18,5,2),(19,5,13),(20,5,6),(21,5,7),(22,5,8),(23,5,9),(24,6,5),(25,6,13),(26,6,14),(27,6,7),(28,7,5),(29,7,6),(30,7,14),(31,7,8),(32,8,5),(33,8,7),(34,8,14),(35,8,10),(36,8,9),(37,9,11),(38,9,5),(39,9,8),(40,9,10),(41,9,19),(42,9,20),(43,9,21),(44,10,9),(45,10,8),(46,10,14),(47,10,16),(48,10,17),(49,10,18),(50,10,19),(51,11,22),(52,11,12),(53,11,1),(54,11,9),(55,11,21),(56,12,11),(57,12,1),(58,12,2),(59,13,2),(60,13,3),(61,13,4),(62,13,14),(63,13,6),(64,13,5),(65,14,8),(66,14,7),(67,14,6),(68,14,13),(69,14,4),(70,14,15),(71,14,16),(72,14,10),(73,15,14),(74,15,4),(75,15,16),(76,16,10),(77,16,14),(78,16,15),(79,16,17),(80,17,18),(81,17,10),(82,17,16),(83,18,23),(84,18,19),(85,18,10),(86,18,17),(87,19,23),(88,19,20),(89,19,9),(90,19,10),(91,19,18),(92,20,23),(93,20,21),(94,20,9),(95,20,19),(96,21,22),(97,21,11),(98,21,9),(99,21,20),(100,21,23),(101,22,11),(102,22,21),(103,23,21),(104,23,20),(105,23,19),(106,23,18);
/*!40000 ALTER TABLE `district_neighbours` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guessing_question`
--

DROP TABLE IF EXISTS `guessing_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `guessing_question` (
  `id` bigint(20) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guessing_question`
--

LOCK TABLES `guessing_question` WRITE;
/*!40000 ALTER TABLE `guessing_question` DISABLE KEYS */;
INSERT INTO `guessing_question` VALUES (1,'A Biblia szerint hány éves volt Sára, amikor megszülte Izsákot?',90),(2,'Hány \"fodor van szoknyámon\" a népdal szerint?',13),(3,'Hány állandó tagja van az ENSZ Biztonsági Tanácsának?',5),(4,'Hány bájt egy kilobájt?',1024),(5,'Hány éjszakán át mesélt Seherezádé a mesegyűjtemény címe szerint?',1001),(6,'Hány évesen lett tábornok Bonaparte Napóleon?',24),(7,'Hány évig élt Matuzsálem?',969),(8,'Hány évig létezett a Német Demokratikus Köztársaság?',41),(9,'Hány évig létezett az Osztrák-Magyar Monarchia?',51),(10,'Hány fokosak az egyenlő oldalú háromszög belső szögei?',60),(11,'Hány golyóval kell játszani a karambol nevű biliárdjátékot?',3),(12,'Hány húrja van a hegedűnek?',4),(13,'Hány István néven uralkodó magyar király volt?',5),(14,'Hány játékos játszhatja egyszerre a rablóultit?',3),(15,'Hány játékos küzd egy csapatból egyszerre a vízilabdában?',7),(16,'Hány játékos van egyszerre a pályán egy kosárlabda-csapatból?',5),(17,'Hány karátos a színarany?',24),(18,'Hány képviselői hely van a magyar országgyűlésben?',386),(19,'Hány kerekű volt Carl Benz első, benzinmotorral hajtott járműve?',3),(20,'Hány kerülete van Budapestnek?',23),(21,'Hány király uralkodott Mátyás néven Magyarországon?',2),(22,'Hány korongból áll egy malom az azonos nevű játékban?',3),(23,'Hány lapból áll egy csomag magyar kártya?',32),(24,'Hány magyar királyt avattak szentté?',2),(25,'Hány másodperc van egy órában?',3600),(26,'Hány megyéje van Magyarországnak?',19),(27,'Hány mezőből áll egy sakktábla?',64),(28,'Hány nappal követi a pünkösd a húsvétot?',50),(29,'Hány négyzetméter egy hektár?',10000),(30,'Hány órából áll egy hét?',168),(31,'Hány perc egy fertályóra?',15),(32,'Hány pont alatt nem szabad megállni a huszonegyes nevű kártyajátékban?',15),(33,'Hány pont van egy dobókockán?',21),(34,'Hány pontja van az első két lapból annak a kártyásnak, akinek Black Jackje van?',21),(35,'Hány pontot ér, ha a darts-tábla közepébe találunk?',50),(36,'Hány stációt járt végig Jézus Krisztus a keresztúton?',14),(37,'Hány szabályos háromszög határol egy oktaédert?',8),(38,'Hány tejfogunk van?',20),(39,'Hány ütést szabad vinnie az ultiban annak, aki \"betlit\" vállal?',0),(40,'Hány vércsoportba sorolható az ember vére?',4);
/*!40000 ALTER TABLE `guessing_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `level_boundary`
--

DROP TABLE IF EXISTS `level_boundary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `level_boundary` (
  `id` bigint(20) NOT NULL,
  `xp` bigint(20) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `level_boundary`
--

LOCK TABLES `level_boundary` WRITE;
/*!40000 ALTER TABLE `level_boundary` DISABLE KEYS */;
INSERT INTO `level_boundary` VALUES (1,0,1),(2,1000,2),(3,2500,3),(4,5000,4),(5,10000,5),(6,15000,6),(7,20000,7),(8,30000,8),(9,40000,9),(10,50000,10);
/*!40000 ALTER TABLE `level_boundary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ROLE_USER'),(2,'ROLE_ADMIN');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trivia_answer`
--

DROP TABLE IF EXISTS `trivia_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trivia_answer` (
  `id` bigint(20) NOT NULL,
  `answer` varchar(45) CHARACTER SET utf8 NOT NULL,
  `correct` tinyint(4) NOT NULL,
  `question_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `question_idx` (`question_id`),
  CONSTRAINT `question_id` FOREIGN KEY (`question_id`) REFERENCES `trivia_question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trivia_answer`
--

LOCK TABLES `trivia_answer` WRITE;
/*!40000 ALTER TABLE `trivia_answer` DISABLE KEYS */;
INSERT INTO `trivia_answer` VALUES (1,'1001. jan.1 .',1,1),(2,'1001. jan. 11.',0,1),(3,'1001. jan. 21.',0,1),(4,'1479. október 13-án',1,2),(5,'1467. november 5-én',0,2),(6,'1456. március 18-án',0,2),(7,'egyszemu óriások',1,3),(8,'félig ember, félig ló alakú lények',0,3),(9,'emberevo óriások',0,3),(10,'IV. Béla',1,4),(11,'III. Béla',0,4),(12,'IV. (Kun) László',0,4),(13,'Wesselényi Miklós',1,5),(14,'Széchenyi István',0,5),(15,'Deák Ferenc',0,5),(16,'James Cook',1,6),(17,'Magellán',0,6),(18,'Marco Polo',0,6),(19,'1957. márc. 7-től',1,7),(20,'1976. jan. 7-től',0,7),(21,'1966. okt. 21-től',0,7),(22,'996-ban',1,8),(23,'998-ban',0,8),(24,'976-ban',0,8),(25,'George W. Bush',0,9),(26,'Ronald Reagan',0,9),(27,'Bill Clinton',1,9),(28,'Morzsinai Erzsébet',0,10),(29,'Szilágyi Erzsébet',1,10),(30,'Anjou Mária',0,10),(31,'Dunáról',1,11),(32,'Tiszáról',0,11),(33,'Szamosról',0,11),(34,'az óriás',0,12),(35,'a zsiráf',0,12),(36,'az öreg hölgy',1,12),(37,'Cipő',1,13),(38,'Cipó',0,13),(39,'Hobó',0,13),(40,'Balga',1,14),(41,'Bamba',0,14),(42,'Suta',0,14),(43,'János Zsigmond',0,15),(44,'Károly Gusztáv',0,15),(45,'János Károly',1,15),(46,'kappanviadal',0,16),(47,'tyúkper',1,16),(48,'bolhaper',0,16),(49,'zuzmó',1,17),(50,'moha',0,17),(51,'alga',0,17),(52,'cvikker',0,18),(53,'cefre',1,18),(54,'cekker',0,18),(55,'Hédi',0,19),(56,'Paula',1,19),(57,'Lujza',0,19),(58,'bonsai',1,20),(59,'banzáj',0,20),(60,'harakiri',0,20),(61,'El Nino',0,21),(62,'Mucho Macho',0,21),(63,'El Mariachi',1,21),(64,'Nagy Testvér',0,22),(65,'Apa',0,22),(66,'Anya',1,22),(67,'tinktúra',0,23),(68,'partitúra',1,23),(69,'matúra',0,23),(70,'Ilma',0,24),(71,'Irma',0,24),(72,'Vilma',1,24),(73,'Margit',0,25),(74,'Beatrix',1,25),(75,'Erzsébet',0,25),(76,'Ábrahám',0,26),(77,'Ádám',0,26),(78,'Ábel',1,26),(79,'pontifex',0,27),(80,'haruspex',0,27),(81,'druida',1,27),(82,'telehírnök',0,28),(83,'telefonhírmondó',1,28),(84,'telefonhíradó',0,28),(85,'Izsák',1,29),(86,'Izmael',0,29),(87,'Lót',0,29),(88,'Erzsébet',0,30),(89,'Zita',1,30),(90,'Gizella',0,30),(91,'Miltiadész',0,31),(92,'Leónidasz',1,31),(93,'Menelaosz',0,31),(94,'Ottó',0,32),(96,'Rudolf',1,32),(97,'Calypso',1,33),(98,'Kirké',0,33),(99,'Pénelopé',0,33),(100,'Magdolna',0,34),(101,'Mária',1,34),(102,'Erzsébet',0,34),(103,'Ábel',0,35),(104,'Áron',1,35),(105,'Bálám',0,35),(106,'sütik',1,36),(107,'varrják',0,36),(108,'kovácsolják',1,36),(109,'borissza',0,37),(110,'borseprő',0,37),(111,'borkóstoló',1,37),(112,'hinajána',0,38),(113,'szangha',1,38),(114,'dharma',0,38),(115,'karuná',0,39),(116,'nirvána',1,39),(117,'muditá',0,39),(118,'heraldika',1,40),(119,'paleográfia',0,40),(120,'numizmatika',0,40),(955,'Ferdinánd',0,32);
/*!40000 ALTER TABLE `trivia_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trivia_question`
--

DROP TABLE IF EXISTS `trivia_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trivia_question` (
  `id` bigint(20) NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trivia_question`
--

LOCK TABLES `trivia_question` WRITE;
/*!40000 ALTER TABLE `trivia_question` DISABLE KEYS */;
INSERT INTO `trivia_question` VALUES (1,'Mikor volt István, királlyá koronázása?'),(2,'Mikor volt a kenyérmezei csata?'),(3,'Kik voltak a Küklopszok a görög mitológiában?'),(4,'Ki volt a második honalapító?'),(5,'Ki volt Az árvízi hajós?'),(6,'Ki fedezte fel a déli sarkkört?'),(7,'Mikortól lehet Magyarországon öttöslottóval játszani?'),(8,'Mikor alapították a Pannonhalmi apátságot?'),(9,'1995-ben ki volt az USA elnöke?'),(10,'Ki volt Hunyadi Mátyás anyja?'),(11,'Hej, melyik folyó felől fúj a szél a népdal szerint?'),(12,'Hogy \"becézik\" a párizsiak az Eiffel-tornyot?'),(13,'Hogy becézik a Republic együttes frontemberét?'),(14,'Hogy hívják a főhős szolgálóját Vörösmarty Mihály Csongor és Tündéjében?'),(15,'Hogy hívják a jelenlegi spanyol királyt?'),(16,'Hogy hívják a jelentéktelen ügyben folytatott pert?'),(17,'Hogy hívják a moszatok és a gombák hasznos együttéléséből létrejövő növényi telepet?'),(18,'Hogy hívják a pálinkafőzéshez erjesztett gyümölcslét?'),(19,'Hogy hívják a rajzfilmhős Mézga Géza feleségét?'),(20,'Hogy hívják a törpedíszfák kialakításának kertművészetét?'),(21,'Hogy hívják Antonio Banderast a Desperado című filmben?'),(22,'Hogy hívják az űrhajó számítógépét A nyolcadik utas: a halál című sci-fiben?'),(23,'Hogy hívják azt a kottát, amelyből a karmester vezényel?'),(24,'Hogy hívják Frédi feleségét a Flinstone család című rajzfilmsorozatban?'),(25,'Hogy hívják Hollandia királynőjét?'),(26,'Hogy hívják Káin testvérét a Bibliában?'),(27,'Hogy hívták a kelta papokat?'),(28,'Hogy hívták a rádió elődjének számító telefonos szolgáltatást?'),(29,'Hogy hívták Ábrahám és Sára idős korukban született gyermekét?'),(30,'Hogy hívták az utolsó magyar király feleségét?'),(31,'Hogy hívták azt a spártai királyt, aki a Thermopülai-szorost védte a perzsákkal szemben?'),(32,'Hogy hívták Ferenc József és Erzsébet királyné egyetlen fiúgyermekét?'),(33,'Hogy hívták Jacques-Yves Cousteau világhírű óceánjáró hajóját?'),(34,'Hogy hívták Jézus édesanyját?'),(35,'Hogy hívták Mózes bátyját?'),(36,'Hogy készül a bagett?'),(37,'Hogy nevezik a borok minőségét érzékszerveivel vizsgáló szakértőt?'),(38,'Hogy nevezik a buddhista szerzetesrendet?'),(39,'Hogy nevezik a buddhista vallásban a teljes megnyugvás állapotát?'),(40,'Hogy nevezik a címerekkel foglalkozó történeti segédtudományt?');
/*!40000 ALTER TABLE `trivia_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `user_information_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_mg37soyogp425sa163bxgf6ax` (`user_information_id`),
  UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`),
  UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`),
  CONSTRAINT `FKfpiv0glwtg1a4q42jtyisb67u` FOREIGN KEY (`user_information_id`) REFERENCES `user_information` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin@budfoglalo.com','$2a$10$BtIIGT6T95C9.rIApREW6u8P3j0ciyVpIbf/Q/0RzaY/xzCXXgKoS','admin',1),(2,'demouser1@budfoglalo.com','$2a$10$V2f4.c.L4Xv1kiRtUFwJd.NSN7BrlEyVJDXO11GSTN/QrpzW69Ozu','demoUser1',2),(3,'demoUser2@budfoglalo.com','$2a$10$m3QXDoXGXTXqBgfENO5OeOkDxpnH7rT3JACydiuKYRCtJKwn/rr6W','demoUser2',3),(4,'demoUser3@demo.com','$2a$10$zYAs7hpoUAKC4l7rhtYb5ezSoIL9dUuzHs4fVCaKStgM9xbzWg2Yq','demoUser3',4),(5,'sajt@kukac.com','$2a$10$N9XPmk4zw5LlDfP8GmbamOLGtgZtZ9llBfvt/zcNhrH950ZdO3AXG','sajtkukac',5),(6,'asd@asd.asd','$2a$10$ThKmYb/T0oeIZ/XZ0LjbTugiwL6Dhz.DePdfQtHhqNqdle3aOjI8a','asdasd',6),(7,'awerf@asefd.com','$2a$10$H39IzfTcSg.q9cqZmUO04.kpqwqMZzjuTBV3hUwpyKcOvkacaSFuO','demoUser4',8);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_information`
--

DROP TABLE IF EXISTS `user_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_information` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT '',
  `last_name` varchar(255) DEFAULT '',
  `level` int(11) DEFAULT '1',
  `registered` date DEFAULT NULL,
  `xp` bigint(20) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_information`
--

LOCK TABLES `user_information` WRITE;
/*!40000 ALTER TABLE `user_information` DISABLE KEYS */;
INSERT INTO `user_information` VALUES (1,NULL,NULL,1,'2021-01-23',0),(2,'John','Doe',7,'2021-01-26',22100),(3,'ConanDaBarbarian','',5,'2021-02-01',14000),(4,'','',1,'2021-02-01',0),(5,'','',1,'2021-02-01',0),(6,'','',1,'2021-02-02',0),(8,'','',1,'2021-03-19',0);
/*!40000 ALTER TABLE `user_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_roles` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKrhfovtciq1l558cw6udg0h0d3` (`role_id`),
  CONSTRAINT `FK55itppkw3i07do3h7qoclqd4k` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKrhfovtciq1l558cw6udg0h0d3` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(1,2);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-19  1:48:46
