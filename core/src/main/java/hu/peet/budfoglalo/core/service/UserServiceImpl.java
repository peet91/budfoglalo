package hu.peet.budfoglalo.core.service;

import hu.peet.budfoglalo.core.CoreApplication;
import hu.peet.budfoglalo.core.enums.RoleEnum;
import hu.peet.budfoglalo.core.exception.UserNameOrEmailExistsException;
import hu.peet.budfoglalo.core.model.Role;
import hu.peet.budfoglalo.core.model.User;
import hu.peet.budfoglalo.core.presenter.user.LoginRequest;
import hu.peet.budfoglalo.core.presenter.user.ProfileUpdateRequest;
import hu.peet.budfoglalo.core.presenter.user.RegistrationRequest;
import hu.peet.budfoglalo.core.presenter.user.UserRegistrationResponse;
import hu.peet.budfoglalo.core.repository.RoleRepository;
import hu.peet.budfoglalo.core.repository.UserRepository;
import hu.peet.budfoglalo.core.security.JwtResponse;
import hu.peet.budfoglalo.core.security.JwtUtils;
import hu.peet.budfoglalo.core.service.interfaces.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    private static final Logger log =
            LoggerFactory.getLogger(CoreApplication.class);

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    JwtUtils jwtUtils;

    public UserRegistrationResponse register(RegistrationRequest registrationRequest) throws UserNameOrEmailExistsException {
        if (userRepository.existsByUsername(registrationRequest.getUsername())) {
            throw new UserNameOrEmailExistsException("Error: Username is already taken!");
        }

        if (userRepository.existsByEmail(registrationRequest.getEmail())) {
            throw new UserNameOrEmailExistsException("Error: Email is already in use!");
        }

        User user = new User();
        user.setUsername(registrationRequest.getUsername());
        user.setPassword(passwordEncoder.encode(registrationRequest.getPassword()));
        user.setEmail(registrationRequest.getEmail());
        Set<Role> roles = new HashSet<>();
        Role userRole = roleRepository.findByName(RoleEnum.ROLE_USER).orElseThrow(() ->
                new RuntimeException("Error: Role is not found."));
        roles.add(userRole);
        user.setRoles(roles);

        userRepository.save(user);

        return new UserRegistrationResponse(user.getId());
    }

    public void update(ProfileUpdateRequest profileUpdateRequest) {
        User user = userRepository.findByUsername(profileUpdateRequest.getUsername()).get();

        if (!profileUpdateRequest.getEmail().equals(user.getEmail())) {
            user.setEmail(profileUpdateRequest.getEmail());
        }
        if (!profileUpdateRequest.getFirstName().equals(user.getUserInformation().getFirstName())) {
            user.getUserInformation().setFirstName(profileUpdateRequest.getFirstName());
        }
        if (!profileUpdateRequest.getLastName().equals(user.getUserInformation().getLastName())) {
            user.getUserInformation().setLastName(profileUpdateRequest.getLastName());
        }

        userRepository.save(user);
        userRepository.flush();
    }

    public JwtResponse login(LoginRequest loginRequest) throws AuthenticationException {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles);
    }

    @Override
    @Transactional(readOnly = true)
    public User findByUserName(String username) throws UsernameNotFoundException {

        User user = userRepository.findByUsername(username).orElseThrow(() ->
                new UsernameNotFoundException("User not found: " + username));
        return user;
    }

    @Override
    @Transactional(readOnly = true)
    public User findByEmail(String email) throws UsernameNotFoundException {

        User user = userRepository.findByEmail(email).orElseThrow(() ->
                new UsernameNotFoundException("User not found by email: " + email));

        return user;
    }

    public User getUserById(long id) {
        return userRepository.findById(id).get();
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public void deleteUserById(long id) {
        User user= userRepository.findById(id).orElseThrow(() ->
                new UsernameNotFoundException("User not found by email: " + id));
        userRepository.delete(user);
        userRepository.flush();
    }
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username).
                orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
        return UserDetailsImpl.build(user);
    }
}
