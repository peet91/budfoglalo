package hu.peet.budfoglalo.core.presenter.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegistrationRequest {
    @JsonProperty
    private String username;

    @NotNull
    @NotEmpty
    @JsonProperty
    private String password;

    @JsonProperty
    private String email;
}
