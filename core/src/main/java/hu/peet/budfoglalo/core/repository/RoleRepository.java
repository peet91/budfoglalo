package hu.peet.budfoglalo.core.repository;

import hu.peet.budfoglalo.core.model.Role;
import hu.peet.budfoglalo.core.enums.RoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleEnum roleName);
}
