package hu.peet.budfoglalo.core;

import hu.peet.budfoglalo.core.repository.RoleRepository;
import hu.peet.budfoglalo.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DemoDataLoader {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @EventListener
    public void appReady(ApplicationReadyEvent event) {
//        loadRoles();
//        loadUsers();
    }

    private void loadRoles() {
//        roleRepository.save(new Role("USER"));
//        roleRepository.save(new Role("ADMIN"));
    }

    private void loadUsers() {
//        User admin = new User();
//        admin.setUsername("admin");
//        admin.setEmail("admin@demo.com");
//        admin.setPassword(passwordEncoder.encode("Almafa123."));
//
//        User demoUser1 = new User();
//        demoUser1.setUsername("demoUser1");
//        demoUser1.setUsername("demoUser1@demo.com");
//        demoUser1.setPassword(passwordEncoder.encode("Almafa123."));
//
//        User demoUser2 = new User();
//        demoUser2.setUsername("demoUser2");
//        demoUser2.setUsername("demoUser2@demo.com");
//        demoUser2.setPassword(passwordEncoder.encode("Almafa123."));
//
//        userRepository.save(admin);
//        userRepository.save(demoUser1);
//        userRepository.save(demoUser2);
//
//        admin.setRoles(new HashSet<>(Arrays.asList(
//                roleRepository.findByName("USER"),
//                roleRepository.findByName("ADMIN"))));
//        demoUser1.setRoles(new HashSet<>(Arrays.asList(roleRepository.findByName("USER"))));
//        demoUser2.setRoles(new HashSet<>(Arrays.asList(roleRepository.findByName("USER"))));
//
//        userRepository.save(admin);
//        userRepository.save(demoUser1);
//        userRepository.save(demoUser2);
    }

    private void loadTriviaAnswers() {

    }
}
