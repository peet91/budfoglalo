package hu.peet.budfoglalo.core.service.interfaces;

import hu.peet.budfoglalo.core.presenter.lobby.PlayerReadyRequest;
import hu.peet.budfoglalo.core.presenter.lobby.CreateLobbyRequest;
import hu.peet.budfoglalo.core.presenter.lobby.PlayerPresenter;
import hu.peet.budfoglalo.core.service.Lobby;
import org.springframework.web.client.ResourceAccessException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public interface GameMasterService {
    void addPlayerToPool(Long userId);

    void heartbeat(Long userId);

    List<PlayerPresenter> getActivePlayers(Long userId);

    UUID createLobby(CreateLobbyRequest createLobbyRequest);

    Map<UUID, Lobby> listPlayerLobbies(Long playerId);

    void acceptGameRequest(PlayerReadyRequest playerReadyRequest) throws ResourceAccessException;

    void denyGameRequest(PlayerReadyRequest playerReadyRequest) throws ResourceAccessException;
}
