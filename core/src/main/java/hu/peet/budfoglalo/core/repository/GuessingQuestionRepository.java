package hu.peet.budfoglalo.core.repository;

import hu.peet.budfoglalo.core.model.GuessingQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GuessingQuestionRepository extends JpaRepository<GuessingQuestion, Long> {

}
