package hu.peet.budfoglalo.core.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Map;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Lobby {
    private UUID lobbyId;

    private Long creatorId;

    private Map<Long, Boolean> players;

    private LocalTime createdTime;

    public void readyPlayer(Long playerId) {
        players.put(playerId, true);
    }

    public boolean isAllPlayersReady() {
        return players.values().stream().filter(ready -> ready.equals(Boolean.TRUE)).count() == players.size();
    }

    public void addPlayerToLobby(Long playerId, Boolean ready) {
        players.put(playerId, ready);
    }

    public void removePlayerFromLobby(Long playerId) {
        players.remove(playerId);
    }
}
