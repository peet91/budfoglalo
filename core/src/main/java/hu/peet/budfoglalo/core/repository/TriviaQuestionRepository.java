package hu.peet.budfoglalo.core.repository;

import hu.peet.budfoglalo.core.model.TriviaQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TriviaQuestionRepository extends JpaRepository<TriviaQuestion, Long> {
}
