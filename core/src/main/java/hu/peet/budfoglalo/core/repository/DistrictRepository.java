package hu.peet.budfoglalo.core.repository;

import hu.peet.budfoglalo.core.model.District;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DistrictRepository extends JpaRepository<District, Long> {
}
