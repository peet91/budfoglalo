package hu.peet.budfoglalo.core.repository;

import hu.peet.budfoglalo.core.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    @Query("select t from User t where t.email = :usernameOrEmail OR t.username = :usernameOrEmail")
    Optional<User> findByEmailOrUsername(@Param("usernameOrEmail") String usernameOfEmail);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
