package hu.peet.budfoglalo.core.service;

import hu.peet.budfoglalo.core.model.District;
import hu.peet.budfoglalo.core.enums.PhaseEnum;
import hu.peet.budfoglalo.core.enums.SubPhaseEnum;
import hu.peet.budfoglalo.core.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Setter
@NoArgsConstructor
public class GameSession {
    @Getter
    private UUID uuid;

    @Getter
    private List<User> players;

    @Getter
    private int activePlayerIndex;

    @Getter
    private Long activePlayerId;

    @Getter
    private List<District> districts;

    @Getter
    private PhaseEnum currentPhase;

    @Getter
    private SubPhaseEnum currentSubPhase;

    public GameSession(UUID uuid) {
        this.uuid = uuid;
        this.players = new ArrayList<>();
        this.districts = new ArrayList<>();
    }

    public Boolean containsPlayer(Long playerId) {
        return players.stream().filter(user -> user.getId().equals(playerId)).findFirst().isPresent();
    }

    public User getPlayer(Long playerId) {
        return players.stream().filter(user -> user.getId().equals(playerId)).findFirst().get();
    }

    public void removePlayers(List<Long> playersToRemove) {
        players.removeIf(player -> playersToRemove.contains(player.getId()));
    }

    //The return value indicates, that every player did some action in the phase
    public Boolean nextPlayer() {
        if (activePlayerIndex < players.size() - 1) {
            activePlayerIndex++;
            activePlayerId = players.get(activePlayerIndex).getId();
            return false;
        } else return true;
    }

    public void initPlayerOrder() {
        Collections.shuffle(players);
        activePlayerIndex = 0;
        activePlayerId = players.get(activePlayerIndex).getId();
    }
}
