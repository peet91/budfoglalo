package hu.peet.budfoglalo.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.security.auth.login.LoginException;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class UserNotFoundException extends LoginException {
    public UserNotFoundException(final String message) {
        super(message);
    }
}