package hu.peet.budfoglalo.core.service.interfaces;

import hu.peet.budfoglalo.core.presenter.question.AnswerRequestPresenter;
import hu.peet.budfoglalo.core.service.GuessingQuestionWrapper;
import hu.peet.budfoglalo.core.service.TriviaQuestionWrapper;

import java.util.List;
import java.util.UUID;

public interface QuestionService {
   TriviaQuestionWrapper getActiveTriviaQuestion(UUID uuid);

   void removeActiveTriviaQuestion(UUID uuid);

   void answerActiveTriviaQuestion(AnswerRequestPresenter answerRequestPresenter);

   void addActiveTriviaQuestion(UUID uuid, List<Long> playerIds);

   void getTriviaQuestionBatch();

   GuessingQuestionWrapper getActiveGuessingQuestion(UUID uuid);

   void removeActiveGuessingQuestion(UUID uuid);

   void answerActiveGuessingQuestion(AnswerRequestPresenter answerRequestPresenter);

   void addActiveGuessingQuestion(UUID uuid, List<Long> playerIds);

   void getGuessingQuestionBatch();
}