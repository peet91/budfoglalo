package hu.peet.budfoglalo.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.PRECONDITION_FAILED)
public class GameSessionNotFoundException extends RuntimeException {
    public GameSessionNotFoundException(final String message) {
        super(message);
    }
}