package hu.peet.budfoglalo.core.presenter.lobby;

import com.fasterxml.jackson.annotation.JsonProperty;
import hu.peet.budfoglalo.core.service.Lobby;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Data
@NoArgsConstructor
public class LobbiesPresenter {

    @JsonProperty
    private List<LobbyPresenter> lobbies;

    public LobbiesPresenter(Map<UUID, Lobby> lobbies) {
        this.lobbies = new ArrayList<>();
        lobbies.entrySet().forEach(lobby -> this.lobbies.add(new LobbyPresenter(lobby.getValue())));
    }
}
