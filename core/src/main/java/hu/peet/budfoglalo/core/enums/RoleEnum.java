package hu.peet.budfoglalo.core.enums;

public enum RoleEnum {
    ROLE_USER,
    ROLE_ADMIN
}
