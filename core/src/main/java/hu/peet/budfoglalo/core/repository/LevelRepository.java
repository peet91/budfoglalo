package hu.peet.budfoglalo.core.repository;

import hu.peet.budfoglalo.core.model.Level;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LevelRepository extends JpaRepository<Level, Long> {
}
