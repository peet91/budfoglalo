package hu.peet.budfoglalo.core.presenter.lobby;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivePlayerListPresenter {
    @JsonProperty
    List<PlayerPresenter> activePlayers;
}
