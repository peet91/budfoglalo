package hu.peet.budfoglalo.core.enums;

public enum SubPhaseEnum {
    TARGET,
    QUESTION_TRIVIA,
    QUESTION_GUESSING,
}
