package hu.peet.budfoglalo.core.model;

import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "trivia_answer")
public class TriviaAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "answer")
    private String answer;

    @Column(name = "correct", columnDefinition = "TINYINT(4)")
    private Boolean correct;

    @Column(name = "question_id")
    private Long questionId;
}
