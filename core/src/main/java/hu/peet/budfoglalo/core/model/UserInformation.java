package hu.peet.budfoglalo.core.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "user_information")
public class UserInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @CreatedDate
    private LocalDate registered;

    @Column(name = "first_name", columnDefinition = "varchar(255) default ''")
    private String firstName = "";

    @Column(name = "last_name", columnDefinition = "varchar(255) default ''")
    private String lastName = "";

    @Column(columnDefinition = "bigint default 0")
    private Long xp = 0L;

    @Column(columnDefinition = "integer default 1")
    private Integer level = 1;

    public void addXp(Long xp) {
        this.xp += xp;
    }
}
