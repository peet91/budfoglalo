package hu.peet.budfoglalo.core.service.interfaces;

import hu.peet.budfoglalo.core.model.User;
import hu.peet.budfoglalo.core.presenter.user.*;
import hu.peet.budfoglalo.core.security.JwtResponse;

import java.util.List;

public interface UserService {
    UserRegistrationResponse register(RegistrationRequest registrationRequest);

    void update(ProfileUpdateRequest profileUpdateRequest);

    JwtResponse login(LoginRequest loginRequest);

    User findByUserName(String userName);

    User findByEmail(String email);

    User getUserById(long id);

    List<User> getUsers();
}
