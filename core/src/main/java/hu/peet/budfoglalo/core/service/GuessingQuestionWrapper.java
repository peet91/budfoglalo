package hu.peet.budfoglalo.core.service;

import hu.peet.budfoglalo.core.model.GuessingQuestion;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class GuessingQuestionWrapper {

    private GuessingQuestion guessingQuestion;

    private Map<Long, String> playerAnswers;

    public GuessingQuestionWrapper(List<Long> playerIds, GuessingQuestion guessingQuestion) {
        this.guessingQuestion = guessingQuestion;
        playerAnswers = new HashMap<>();
        playerIds.stream().forEach(playerId -> playerAnswers.put(playerId, null));
    }

    public boolean allPlayersAnswered() {
        return playerAnswers.entrySet().stream().filter(
                playerAnswer -> playerAnswer.getValue() != null).count() == playerAnswers.size();
    }

    public Long evaluateAnswers() {
//        System.err.println("Evaluation player answers:");
//        System.err.println(guessingQuestion.getQuestion());
//        System.err.println(guessingQuestion.getAnswer().toString());
//        System.err.println("playerAnswers:");
//        System.err.println(playerAnswers.toString());

        SecureRandom random = new SecureRandom();

        Map.Entry<Long, String> winner = playerAnswers.entrySet().stream()
                .collect(Collectors.toList()).get(random.nextInt(playerAnswers.size()));

        Long castedPlayerAnswer;

        try {
            castedPlayerAnswer = Long.valueOf(winner.getValue());
        } catch (NumberFormatException e) {
            castedPlayerAnswer = 0L;
        }

        Long winnerDiff = Math.abs(guessingQuestion.getAnswer() - castedPlayerAnswer);

        for (Map.Entry playerAnswer : playerAnswers.entrySet()) {
            try {
                castedPlayerAnswer = Long.valueOf(playerAnswer.getValue().toString());
            } catch (NumberFormatException e) {
                castedPlayerAnswer = 0L;
            }
            if (Math.abs(guessingQuestion.getAnswer() - castedPlayerAnswer) < winnerDiff) {
                winnerDiff = Math.abs(guessingQuestion.getAnswer() - castedPlayerAnswer);
                winner = playerAnswer;
            }
        }
        return Long.valueOf(winner.getKey().toString());
    }
}
