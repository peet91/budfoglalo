package hu.peet.budfoglalo.core.presenter.lobby;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateLobbyRequest {

    @JsonProperty
    private Long playerId;

    @JsonProperty
    private List<Long> opponentIds;
}
