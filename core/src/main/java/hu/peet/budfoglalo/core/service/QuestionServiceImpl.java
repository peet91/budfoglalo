package hu.peet.budfoglalo.core.service;

import hu.peet.budfoglalo.core.model.GuessingQuestion;
import hu.peet.budfoglalo.core.model.TriviaQuestion;
import hu.peet.budfoglalo.core.presenter.question.AnswerRequestPresenter;
import hu.peet.budfoglalo.core.repository.GuessingQuestionRepository;
import hu.peet.budfoglalo.core.repository.TriviaQuestionRepository;
import hu.peet.budfoglalo.core.service.interfaces.QuestionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.SecureRandom;
import java.util.*;

@Service
public class QuestionServiceImpl implements QuestionService {

    private static final Logger log =
            LoggerFactory.getLogger(QuestionService.class);

    @Autowired
    private TriviaQuestionRepository triviaQuestionRepository;

    @Autowired
    private GuessingQuestionRepository guessingQuestionRepository;

    private int triviaQuestionBatchSize = 5;

    private List<TriviaQuestion> triviaQuestions = new ArrayList<>();

    private int guessingQuestionBatchSize = 5;

    private List<GuessingQuestion> guessingQuestions = new ArrayList<>();

    private Map<UUID, TriviaQuestionWrapper> activeTriviaQuestions = new HashMap<>();
    private Map<UUID, GuessingQuestionWrapper> activeGuessingQuestions = new HashMap<>();

    @Override
    public TriviaQuestionWrapper getActiveTriviaQuestion(UUID uuid) {
        return activeTriviaQuestions.get(uuid);
    }

    @Override
    public void removeActiveTriviaQuestion(UUID uuid) {
        activeTriviaQuestions.remove(uuid);
    }

    @Override
    public void answerActiveTriviaQuestion(AnswerRequestPresenter answerRequestPresenter) {
        TriviaQuestionWrapper activeTriviaQuestion = activeTriviaQuestions.get(answerRequestPresenter.getUuid());
        activeTriviaQuestion.getPlayerAnswers().put(answerRequestPresenter.getPlayerId(), answerRequestPresenter.getAnswer());
    }

    @Override
    public void addActiveTriviaQuestion(UUID uuid, List<Long> playerIds) {
        if (triviaQuestions.isEmpty()) {
            getTriviaQuestionBatch();
        }
        activeTriviaQuestions.put(uuid, new TriviaQuestionWrapper(playerIds,
                triviaQuestions.remove(triviaQuestions.size() - 1)));
    }

    @Override
    public void getTriviaQuestionBatch() {
        SecureRandom random = new SecureRandom();

        long triviaQuestionSize = triviaQuestionRepository.count();

        if (triviaQuestionBatchSize > triviaQuestionSize) {
            throw new RuntimeException("Batch Size is bigger than trivia question database");
        }

        triviaQuestions.addAll(triviaQuestionRepository.findAll(
                PageRequest.of(random.nextInt(Math.toIntExact(triviaQuestionSize) / triviaQuestionBatchSize),
                        triviaQuestionBatchSize)).getContent());
        Collections.shuffle(triviaQuestions);
    }

    @Override
    public GuessingQuestionWrapper getActiveGuessingQuestion(UUID uuid) {
        return activeGuessingQuestions.get(uuid);
    }

    @Override
    public void removeActiveGuessingQuestion(UUID uuid) {
        activeGuessingQuestions.remove(uuid);
    }

    @Override
    public void answerActiveGuessingQuestion(AnswerRequestPresenter answerRequestPresenter) {
        log.info(answerRequestPresenter.toString());
        GuessingQuestionWrapper activeGuessingQuestion = activeGuessingQuestions.get(answerRequestPresenter.getUuid());
        activeGuessingQuestion.getPlayerAnswers().put(answerRequestPresenter.getPlayerId(), answerRequestPresenter.getAnswer());
    }

    @Override
    public void addActiveGuessingQuestion(UUID uuid, List<Long> playerIds) {
        if (guessingQuestions.isEmpty()) {
            getGuessingQuestionBatch();
        }
        activeGuessingQuestions.put(uuid, new GuessingQuestionWrapper(playerIds,
                guessingQuestions.remove(guessingQuestions.size() - 1)));
    }

    @Override
    public void getGuessingQuestionBatch() {
        SecureRandom random = new SecureRandom();

        long guessingQuestionSize = guessingQuestionRepository.count();

        if (guessingQuestionBatchSize > guessingQuestionSize) {
            throw new RuntimeException("Batch Size is bigger than guessing question database");
        }

        guessingQuestions.addAll(guessingQuestionRepository.findAll(
                PageRequest.of(random.nextInt(Math.toIntExact(guessingQuestionSize) / guessingQuestionBatchSize),
                        guessingQuestionBatchSize)).getContent());
        Collections.shuffle(guessingQuestions);
    }
}
