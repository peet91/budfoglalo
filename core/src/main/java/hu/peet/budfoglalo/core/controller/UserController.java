package hu.peet.budfoglalo.core.controller;

import hu.peet.budfoglalo.core.exception.UserNameOrEmailExistsException;
import hu.peet.budfoglalo.core.presenter.user.*;
import hu.peet.budfoglalo.core.security.JwtResponse;
import hu.peet.budfoglalo.core.service.GameMasterServiceImpl;
import hu.peet.budfoglalo.core.service.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping(value = "/api/user")
public class UserController {

    private static final Logger log =
            LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private GameMasterServiceImpl gameMasterService;

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JwtResponse> login(@Valid @RequestBody LoginRequest loginRequest) throws ResponseStatusException {
        log.info("Login attempt with " + loginRequest.toString());
        try {
            JwtResponse jwtResponse = userService.login(loginRequest);
            gameMasterService.addPlayerToPool(jwtResponse.getId());
            return ResponseEntity.ok(jwtResponse);
        } catch (AuthenticationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED, "Incorrect username or password!");
        }
    }

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserRegistrationResponse> register(@Valid @RequestBody RegistrationRequest registrationRequest)
            throws UserNameOrEmailExistsException {
        log.info("Registration attempt with: " + registrationRequest.toString());
        try {
            return ResponseEntity.ok(userService.register(registrationRequest));
        } catch (UserNameOrEmailExistsException e) {
            log.info(e.getMessage());
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping(value = "/profile", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserInformationPresenter> getUserProfile(Principal principal) {
        return ResponseEntity.ok(new UserInformationPresenter(userService.findByUserName(principal.getName())));
    }

    @PostMapping(value = "/profile/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpStatus updateUserProfile(@Valid @RequestBody ProfileUpdateRequest profileUpdateRequest, Principal principal) {
        if (principal.getName().equals(profileUpdateRequest.getUsername())) {
            userService.update(profileUpdateRequest);
            return HttpStatus.OK;
        } else {
            return HttpStatus.UNAUTHORIZED;
        }
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserInformationPresenter> getUser(@PathVariable Long id) {
        return ResponseEntity.ok(new UserInformationPresenter(userService.getUserById(id)));
    }

    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserListPresenter> getUsers() {
        return ResponseEntity.ok(new UserListPresenter((userService.getUsers())));
    }
}
