package hu.peet.budfoglalo.core.presenter.district;

import com.fasterxml.jackson.annotation.JsonProperty;
import hu.peet.budfoglalo.core.model.District;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Transient;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DistrictPresenter {

    @JsonProperty
    private Long id;

    @JsonProperty
    private String name;

    @JsonProperty
    private Boolean base;

    @JsonProperty
    private Integer health;

    @JsonProperty
    private Long value;

    @JsonProperty
    private Long ownerId;

    @JsonProperty
    private Long invaderId;

    public DistrictPresenter(District district) {
        this.id = district.getId();
        this.name = district.getName();
        this.base = district.getBase();
        this.health = district.getHealth();
        this.value = district.getValue();
        this.ownerId = district.getOwnerId();
        this.invaderId = district.getInvaderId();
    }
}
