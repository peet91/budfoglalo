package hu.peet.budfoglalo.core.presenter.question;

import com.fasterxml.jackson.annotation.JsonProperty;
import hu.peet.budfoglalo.core.model.TriviaQuestion;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TriviaQuestionPresenter {

    @JsonProperty
    private Long questionId;

    @JsonProperty
    private String question;

    @JsonProperty
    private List<String> answers;

    public TriviaQuestionPresenter(TriviaQuestion triviaQuestion) {
        this.questionId = triviaQuestion.getId();
        this.question = triviaQuestion.getQuestion();
        this.answers = triviaQuestion.getAnswers().stream().map(
                triviaAnswer -> triviaAnswer.getAnswer()).collect(Collectors.toList());
        Collections.shuffle(this.answers);
    }
}
