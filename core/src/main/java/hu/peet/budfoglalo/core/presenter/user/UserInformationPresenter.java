package hu.peet.budfoglalo.core.presenter.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import hu.peet.budfoglalo.core.model.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class UserInformationPresenter {
    @JsonProperty
    private long id;

    @JsonProperty
    private String userName;

    @JsonProperty
    private String password;

    @JsonProperty
    private String email;

    @JsonProperty
    private LocalDate registered;

    @JsonProperty
    private String firstName;

    @JsonProperty
    private String lastName;

    @JsonProperty
    private long xp;

    @JsonProperty
    private int level;

    public UserInformationPresenter(User user) {
        this.id = user.getId();
        this.userName = user.getUsername();
        this.email = user.getEmail();
        this.registered = user.getUserInformation().getRegistered();
        this.firstName = user.getUserInformation().getFirstName();
        this.lastName = user.getUserInformation().getLastName();
        this.xp = user.getUserInformation().getXp();
        this.level = user.getUserInformation().getLevel();
    }
}
