package hu.peet.budfoglalo.core.model;

import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "district")
public class District {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Transient
    private Boolean base = false;

    @Transient
    private Integer health = 1;

    @Transient
    private Long value = 0L;

    @Transient
    private Long ownerId = 0L;

    @Transient
    private Long invaderId = 0L;
}
