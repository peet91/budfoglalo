package hu.peet.budfoglalo.core.presenter.lobby;

import hu.peet.budfoglalo.core.service.Lobby;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LobbyPresenter {
    private UUID lobbyId;

    private Long creatorId;

    private Map<Long, Boolean> players;

    public LobbyPresenter(Lobby lobby) {
        this.lobbyId = lobby.getLobbyId();
        this.creatorId = lobby.getCreatorId();
        this.players = lobby.getPlayers();
    }
}
