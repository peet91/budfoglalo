package hu.peet.budfoglalo.core.service;

import hu.peet.budfoglalo.core.enums.PhaseEnum;
import hu.peet.budfoglalo.core.enums.SubPhaseEnum;
import hu.peet.budfoglalo.core.exception.GameSessionNotFoundException;
import hu.peet.budfoglalo.core.exception.GameStateException;
import hu.peet.budfoglalo.core.model.District;
import hu.peet.budfoglalo.core.model.DistrictNeighbour;
import hu.peet.budfoglalo.core.model.Level;
import hu.peet.budfoglalo.core.model.User;
import hu.peet.budfoglalo.core.presenter.game.GameState;
import hu.peet.budfoglalo.core.presenter.game.GameStateUpdateRequest;
import hu.peet.budfoglalo.core.presenter.game.LeaveGameRequest;
import hu.peet.budfoglalo.core.presenter.question.AnswerRequestPresenter;
import hu.peet.budfoglalo.core.repository.DistrictNeighbourRepository;
import hu.peet.budfoglalo.core.repository.DistrictRepository;
import hu.peet.budfoglalo.core.repository.LevelRepository;
import hu.peet.budfoglalo.core.repository.UserRepository;
import hu.peet.budfoglalo.core.service.interfaces.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.ResourceAccessException;

import java.time.Duration;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@EnableScheduling
public class GameServiceImpl implements GameService {
    //SURVEY SLAYER!!! \m/

    private static final Logger log =
            LoggerFactory.getLogger(GameService.class);

    @Autowired
    private QuestionServiceImpl questionService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LevelRepository levelRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private DistrictNeighbourRepository districtNeighbourRepository;

    private Map<UUID, GameSession> gameSessions = new HashMap<>();

    @Scheduled(fixedDelay = 120000)
    public void cleanUpGameSessions() {
        List<UUID> gameSessionsToRemove = new ArrayList<>();

        gameSessions.entrySet().stream().forEach(gameSession -> {
            if (gameSession.getValue().getCurrentPhase().equals(PhaseEnum.GAME_OVER)) {
                gameSessionsToRemove.add(gameSession.getKey());
                log.info("Removed gameSession " + gameSession.toString() + " due to game over!");
            }
        });
        gameSessionsToRemove.forEach(gameSessionId -> gameSessions.remove(gameSessionId));
    }

    @Scheduled(fixedDelay = 45000)
    public void checkInGameHeartbeats() {
        gameSessions.entrySet().stream().forEach(gameSession -> {
            List<Long> playersToRemove = new ArrayList<>();

            playersToRemove.addAll(gameSession.getValue().getPlayers().stream().filter(player ->
                    Duration.between(player.getLastHeartbeat(), LocalTime.now()).toMillis() >= 30000)
                    .map(User::getId).collect(Collectors.toList()));

            if (!playersToRemove.isEmpty()) {
                log.info("Removed players " + playersToRemove.toString() + " from game: "
                        + gameSession.getKey() + " due to missing heartbeat!");
                gameSession.getValue().removePlayers(playersToRemove);
            }

            if (gameSession.getValue().getPlayers().size() < 2) {
                gameSession.getValue().setCurrentPhase(PhaseEnum.GAME_OVER);
            }
        });
    }

    @Override
    public void heartbeat(Long playerId, UUID gameSessionId) {
//        log.info("heartbeat for user: " + userId);
        gameSessions.get(gameSessionId).getPlayer(playerId).setLastHeartbeat(LocalTime.now());
    }

    @Override
    public Optional<UUID> getPlayerGame(Long playerId) {
        return gameSessions.entrySet().stream().filter(gameSession ->
                gameSession.getValue().containsPlayer(playerId) &&
                        !gameSession.getValue().getCurrentPhase().equals(PhaseEnum.GAME_OVER)).findFirst().map(Map.Entry::getKey);
    }

    @Override
    public UUID initGameSession(UUID uuid, List<Long> playerIds) {
        GameSession gameSession = new GameSession(uuid);

        gameSession.setDistricts(districtRepository.findAll());

        playerIds.stream().forEach(playerID ->
                gameSession.getPlayers().add(userRepository.findById(playerID).orElseThrow(() ->
                        new UsernameNotFoundException("User not found during gameSession init! id: " + playerID))));

        gameSession.getPlayers().stream().forEach(user -> user.setLastHeartbeat(LocalTime.now()));

        gameSession.initPlayerOrder();

        gameSession.setCurrentPhase(PhaseEnum.BASE_DISTRICT_SELECT);
        gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);

        questionService.getTriviaQuestionBatch();
        questionService.getGuessingQuestionBatch();

        gameSessions.put(uuid, gameSession);

        log.info("Game session created with id: " + gameSession.getUuid() + " with players: " +
                gameSession.getPlayers().stream().map(User::getId).collect(Collectors.toList()));

        return uuid;
    }

    @Override
    public GameState getGameState(Long playerId, UUID gameSessionId) throws
            ResourceAccessException, GameSessionNotFoundException {
        if (!gameSessions.containsKey(gameSessionId)) {
            throw new GameSessionNotFoundException("Game Session not found: " + gameSessionId);
        }
        if (gameSessions.get(gameSessionId).containsPlayer(playerId)) {
            return new GameState(gameSessions.get(gameSessionId));
        } else throw new ResourceAccessException("Player " + playerId + " doesn't belong to game: " + gameSessionId);
    }

    @Override
    public GameState updateGameState(GameStateUpdateRequest gameStateUpdateRequest)
            throws ResourceAccessException, GameStateException {
        GameSession gameSession = gameSessions.get(gameStateUpdateRequest.getUuid());
        if (!gameSession.containsPlayer(gameStateUpdateRequest.getPlayerId())) {
            throw new ResourceAccessException("Player " + gameStateUpdateRequest.getPlayerId()
                    + " doesn't belong to game: " + gameStateUpdateRequest.getUuid());
        }
        if (gameSession.getActivePlayerId() != gameStateUpdateRequest.getPlayerId()) {
            throw new GameStateException("Player " + gameStateUpdateRequest.getPlayerId()
                    + " is not the active player!");
        }
        handlePhase(gameSession, gameStateUpdateRequest.getPlayerId(), gameStateUpdateRequest.getUpdatedDistrictId());
        return new GameState(gameSession);
    }

    @Override
    public GameState updateGameState(AnswerRequestPresenter answerRequestPresenter)
            throws ResourceAccessException, GameStateException {
        GameSession gameSession = gameSessions.get(answerRequestPresenter.getUuid());
        if (!gameSession.containsPlayer(answerRequestPresenter.getPlayerId())) {
            throw new ResourceAccessException("Player " + answerRequestPresenter.getPlayerId()
                    + " doesn't belong to game: " + answerRequestPresenter.getUuid());
        }
        handlePhase(gameSession, answerRequestPresenter.getPlayerId(), null);
        return new GameState(gameSession);
    }

    public void leaveGame(LeaveGameRequest leaveGameRequest) {
        GameSession gameSession = gameSessions.get(leaveGameRequest.getUuid());
        if (!gameSession.containsPlayer(leaveGameRequest.getPlayerId())) {
            throw new ResourceAccessException("Player " + leaveGameRequest.getPlayerId()
                    + " doesn't belong to game: " + leaveGameRequest.getUuid());
        }

        gameSession.getPlayers().remove(gameSession.getPlayers().stream().filter(user ->
                user.getId().equals(leaveGameRequest.getPlayerId())).findFirst().get());
        gameSession.getDistricts().stream().forEach(district -> {
            if (district.getOwnerId().equals(leaveGameRequest.getPlayerId()) ||
                    district.getInvaderId().equals(leaveGameRequest.getPlayerId())) {
                district.setBase(false);
                district.setHealth(1);
                district.setValue(0L);
                district.setInvaderId(0L);
                district.setOwnerId(0L);
            }
        });

        log.info("Player with id: " + leaveGameRequest.getPlayerId() + " left the game " + leaveGameRequest.getUuid());

        if (gameSession.getPlayers().size() < 2) {
            log.info("Not enough players in game " + leaveGameRequest.getUuid() + " ending game...");
            gameSession.setCurrentPhase(PhaseEnum.GAME_OVER);
            handleGameOverPhase(gameSession);
        }
    }

    private void handlePhase(GameSession gameSession, Long playerId, Long districtId) throws GameStateException {
        switch (gameSession.getCurrentPhase()) {
            case BASE_DISTRICT_SELECT: {
                handleBaseDistrictPhase(gameSession, playerId, districtId);
                break;
            }
            case EXPANSION_ONE: {
                handleExpansionPhase(gameSession, playerId, districtId, PhaseEnum.EXPANSION_TWO);
                break;
            }
            case EXPANSION_TWO: {
                handleExpansionPhase(gameSession, playerId, districtId, PhaseEnum.EXPANSION_THREE);
                break;
            }
            case EXPANSION_THREE: {
                handleExpansionPhase(gameSession, playerId, districtId, PhaseEnum.EXPANSION_FOUR);
                break;
            }
            case EXPANSION_FOUR: {
                handleExpansionPhase(gameSession, playerId, districtId, PhaseEnum.DIVISION);
                break;
            }
            case DIVISION: {
                handleDivisionPhase(gameSession, playerId, districtId);
                break;
            }
            case WAR_ONE: {
                handleWarPhase(gameSession, playerId, districtId, PhaseEnum.WAR_TWO);
                break;
            }
            case WAR_TWO: {
                handleWarPhase(gameSession, playerId, districtId, PhaseEnum.WAR_THREE);
                break;
            }
            case WAR_THREE: {
                handleWarPhase(gameSession, playerId, districtId, PhaseEnum.WAR_FOUR);
                break;
            }
            case WAR_FOUR: {
                handleWarPhase(gameSession, playerId, districtId, PhaseEnum.WAR_FIVE);
                break;
            }
            case WAR_FIVE: {
                handleWarPhase(gameSession, playerId, districtId, PhaseEnum.WAR_SIX);
                break;
            }
            case WAR_SIX: {
                handleWarPhase(gameSession, playerId, districtId, PhaseEnum.GAME_OVER);
                break;
            }
            case GAME_OVER: {
                handleGameOverPhase(gameSession);
                break;
            }
            default: {
                throw new RuntimeException("Unknown phase: " + gameSession.getCurrentPhase());
            }
        }
        updatePlayerPoints(gameSession);
    }

    private void handleBaseDistrictPhase(GameSession gameSession, Long playerId, Long districtId) {
        occupyBase(gameSession, playerId, districtId);
        if (gameSession.nextPlayer()) {
            gameSession.setCurrentPhase(PhaseEnum.EXPANSION_ONE);
            gameSession.initPlayerOrder();
        }
    }

    private void handleExpansionPhase(GameSession gameSession, Long playerId, Long districtId, PhaseEnum nextPhase) {
        if (gameSession.getCurrentSubPhase().equals(SubPhaseEnum.TARGET)) {
            targetDistrict(gameSession, playerId, districtId);
            if (gameSession.nextPlayer()) {
                gameSession.setCurrentSubPhase(SubPhaseEnum.QUESTION_TRIVIA);
                questionService.addActiveTriviaQuestion(gameSession.getUuid(),
                        gameSession.getPlayers().stream().map(user -> user.getId()).collect(Collectors.toList()));
            }
        } else if (gameSession.getCurrentSubPhase().equals(SubPhaseEnum.QUESTION_TRIVIA)) {
            if (questionService.getActiveTriviaQuestion(gameSession.getUuid()).allPlayersAnswered()) {
                handleDistrictOccupation(gameSession);
                gameSession.setCurrentPhase(nextPhase);
                gameSession.initPlayerOrder();
                questionService.addActiveTriviaQuestion(gameSession.getUuid(),
                        gameSession.getPlayers().stream().map(User::getId).collect(Collectors.toList()));

                if (nextPhase.equals(PhaseEnum.DIVISION)) {
                    gameSession.setCurrentSubPhase(SubPhaseEnum.QUESTION_GUESSING);
                    questionService.addActiveGuessingQuestion(gameSession.getUuid(),
                            gameSession.getPlayers().stream().map(user -> user.getId()).collect(Collectors.toList()));
                    handlePhase(gameSession, playerId, districtId);
                } else {
                    gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);
                }
            }
        }
    }

    private void handleDistrictOccupation(GameSession gameSession) {
        questionService.getActiveTriviaQuestion(gameSession.getUuid()).evaluateAnswers().stream().forEach(
                winnerPlayerId -> {
                    Long targetDistrictId = gameSession.getDistricts().stream().filter(district ->
                            district.getInvaderId().equals(winnerPlayerId)).findFirst().get().getId();
                    occupyDistrict(gameSession, targetDistrictId);
                });

        gameSession.getDistricts().stream().forEach(district -> district.setInvaderId(0L));
    }

    private void handleDivisionPhase(GameSession gameSession, Long playerId, Long districtId) {
        if (gameSession.getCurrentSubPhase().equals(SubPhaseEnum.QUESTION_GUESSING)) {
            if (questionService.getActiveGuessingQuestion(gameSession.getUuid()).allPlayersAnswered()) {
                gameSession.setActivePlayerId(questionService.getActiveGuessingQuestion(
                        gameSession.getUuid()).evaluateAnswers());
                gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);
                questionService.removeActiveGuessingQuestion(gameSession.getUuid());
            }
        } else if (gameSession.getCurrentSubPhase().equals(SubPhaseEnum.TARGET)) {
            targetDistrict(gameSession, playerId, districtId);
            occupyDistrict(gameSession, districtId);

            if (!getUnoccupiedDistrictIds(gameSession).isEmpty()) {
                gameSession.setCurrentSubPhase(SubPhaseEnum.QUESTION_GUESSING);
                questionService.addActiveGuessingQuestion(gameSession.getUuid(),
                        gameSession.getPlayers().stream().map(user -> user.getId()).collect(Collectors.toList()));
                handlePhase(gameSession, playerId, districtId);
            } else {
                gameSession.setCurrentPhase(PhaseEnum.WAR_ONE);
                gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);
                gameSession.initPlayerOrder();
            }
        }
    }

    private void handleWarPhase(GameSession gameSession, Long playerId, Long districtId, PhaseEnum nextPhase) {
        if (gameSession.getCurrentSubPhase().equals(SubPhaseEnum.TARGET)) {
            List<Long> playersAtWar = new ArrayList<>();
            playersAtWar.add(playerId);
            playersAtWar.add(targetDistrict(gameSession, playerId, districtId));

            gameSession.setCurrentSubPhase(SubPhaseEnum.QUESTION_TRIVIA);
            questionService.addActiveTriviaQuestion(gameSession.getUuid(),
                    new ArrayList<>(playersAtWar));
        } else if (gameSession.getCurrentSubPhase().equals(SubPhaseEnum.QUESTION_TRIVIA)) {
            if (questionService.getActiveTriviaQuestion(gameSession.getUuid()).allPlayersAnswered()) {
                evaluateWarTriviaRound(gameSession, nextPhase);
            }
        } else if (gameSession.getCurrentSubPhase().equals(SubPhaseEnum.QUESTION_GUESSING)) {
            if (questionService.getActiveGuessingQuestion(gameSession.getUuid()).allPlayersAnswered()) {
                evaluateWarGuessingRound(gameSession, nextPhase);
            }
        }
    }

    private void handleGameOverPhase(GameSession gameSession) {
        log.info("GAME OVER, updating player xp, etc...");
        questionService.removeActiveTriviaQuestion(gameSession.getUuid());
        questionService.removeActiveGuessingQuestion(gameSession.getUuid());
        updatePlayerPoints(gameSession);
        gameSession.getPlayers().forEach(user -> {
            user.getUserInformation().addXp(user.getPoints());

            user.getUserInformation().setLevel(levelRepository.findAll().stream().filter(level ->
                    level.getXp() < user.getUserInformation().getXp()).max(
                    Comparator.comparing(Level::getLevel)).get().getLevel());
            userRepository.save(user);
        });

        //TODO possible extensions to create statistics, etc...
    }

    private void evaluateWarTriviaRound(GameSession gameSession, PhaseEnum nextPhase) {
        List<Long> correctPlayers = questionService.getActiveTriviaQuestion(gameSession.getUuid()).evaluateAnswers();

        if (correctPlayers.size() == 2) {
            log.info("Both players answered correctly, initiating guessing round");
            gameSession.setCurrentSubPhase(SubPhaseEnum.QUESTION_GUESSING);
            questionService.addActiveGuessingQuestion(gameSession.getUuid(),
                    new ArrayList<>(questionService.getActiveTriviaQuestion(
                            gameSession.getUuid()).getPlayerAnswers().keySet()));
            questionService.removeActiveTriviaQuestion(gameSession.getUuid());
            handleWarPhase(gameSession, null, null, nextPhase);
        } else if (correctPlayers.size() == 1) {
            log.info("Player " + correctPlayers.get(0) + " won the trivia war round!");
            handleWarRoundWinner(gameSession, correctPlayers.get(0), nextPhase);
        } else {
            handleWarRoundWinner(gameSession, null, nextPhase);
        }
    }

    private void evaluateWarGuessingRound(GameSession gameSession, PhaseEnum nextPhase) {
        Long winnerId = questionService.getActiveGuessingQuestion(gameSession.getUuid()).evaluateAnswers();
        handleWarRoundWinner(gameSession, winnerId, nextPhase);
        questionService.removeActiveGuessingQuestion(gameSession.getUuid());
        log.info("Player " + winnerId + " won the war guessing round!");
    }

    private void handleWarRoundWinner(GameSession gameSession, Long winnerId, PhaseEnum nextPhase) {
        District targetedDistrict = gameSession.getDistricts().stream().filter(district ->
                district.getInvaderId() != 0L).findFirst().get();

        if (winnerId == null || !winnerId.equals(targetedDistrict.getInvaderId())) {
            log.info("invader failed, clearing up invaderId, calling next player");
            //clearing targeted district
            targetedDistrict.setInvaderId(0L);
            if (gameSession.nextPlayer()) {
                gameSession.setCurrentPhase(nextPhase);
                if (gameSession.getCurrentPhase().equals(PhaseEnum.GAME_OVER)) {
                    handlePhase(gameSession, null, null);
                }
                gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);
                gameSession.initPlayerOrder();
            }
            //there are more players in the phase, activate next player
            gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);
        } else if (winnerId.equals(targetedDistrict.getInvaderId())) {
            //attacker won the round, occupy base
            if (occupyDistrict(gameSession, targetedDistrict.getId())) {
                //no more players in the phase, go to next phase
                if (gameSession.nextPlayer()) {
                    gameSession.setCurrentPhase(nextPhase);
                    if (gameSession.getCurrentPhase().equals(PhaseEnum.GAME_OVER)) {
                        handlePhase(gameSession, null, null);
                    }
                    gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);
                    gameSession.initPlayerOrder();
                }
                //there are more players in the phase, activate next player
                gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);
            } else {
                //attacker won the round, but attacking a base, so continue the siege
                gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);
                handleWarPhase(gameSession, winnerId, targetedDistrict.getId(), nextPhase);
            }
        }

        //TODO this alternative solution differentiates the case when the defending player won for possible rewarding
        /*
        if (winnerId != null) {
            log.info("winner id not null, handling occupation");
            if (targetedDistrict.getInvaderId().equals(winnerId)) {
                //attacker won the round, occupy base
                if (occupyDistrict(gameSession, targetedDistrict.getId())) {
                    //no more players in the phase, go to next phase
                    if (gameSession.nextPlayer()) {
                        gameSession.setCurrentPhase(nextPhase);
                        gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);
                        gameSession.initPlayerOrder();
                    }
                    //there are more players in the phase, activate next player
                    gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);
                } else {
                    //attacker won the round, but attacking a base, so continue the siege
                    gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);
                    handleWarPhase(gameSession, winnerId, targetedDistrict.getId(), nextPhase);
                }
            } else {
                //TODO here we should add some kind of reward to the defending player
                log.info("defender player won the round, clearing up invader, calling next player");
                //clearing targeted district
                targetedDistrict.setInvaderId(0L);
                if (gameSession.nextPlayer()) {
                    gameSession.setCurrentPhase(nextPhase);
                    gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);
                    gameSession.initPlayerOrder();
                }
                //there are more players in the phase, activate next player
                gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);
            }
        } else {
            log.info("winnerId is null, clearing up invader, calling next player");
            //clearing targeted district
            targetedDistrict.setInvaderId(0L);
            if (gameSession.nextPlayer()) {
                gameSession.setCurrentPhase(nextPhase);
                gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);
                gameSession.initPlayerOrder();
            }
            //there are more players in the phase, activate next player
            gameSession.setCurrentSubPhase(SubPhaseEnum.TARGET);
        }
        */
    }

    private boolean occupyDistrict(GameSession gameSession, Long districtId) {
        District targetDistrict = gameSession.getDistricts().stream().filter(
                district -> district.getId().equals(districtId)).findFirst().get();

        targetDistrict.setHealth((targetDistrict.getHealth() - 1));
        Long oldOwnerId = targetDistrict.getOwnerId();
        Long invader = targetDistrict.getInvaderId();

        if (targetDistrict.getHealth() == 0) {
            targetDistrict.setOwnerId(targetDistrict.getInvaderId());
            targetDistrict.setInvaderId(0L);
            targetDistrict.setHealth(1);
            updateDistrictValue(gameSession.getCurrentPhase(), targetDistrict);
            log.info("Player " + targetDistrict.getOwnerId() + " occupied district: " + targetDistrict.getId());

            //if the enemy base gets occupied, then the enemy is defeated and the invader gets all of his/her districts
            if (targetDistrict.getBase()) {
                targetDistrict.setValue(500L);
                gameSession.getDistricts().stream().filter(
                        district -> district.getOwnerId() == oldOwnerId).forEach(district -> {
                    district.setInvaderId(invader);
                    occupyDistrict(gameSession, district.getId());
                });
                log.info("Player " + targetDistrict.getOwnerId() + " defeated player: " + oldOwnerId
                        + " and occupied all his/her districts");
                gameSession.setCurrentPhase(PhaseEnum.GAME_OVER);
                handlePhase(gameSession, null, null);
            }
            return true;
        }
        log.info("Player " + targetDistrict.getOwnerId() + " damaged district: " + targetDistrict.getId()
                + " remaining health: " + targetDistrict.getHealth());
        return false;
    }

    private void updatePlayerPoints(GameSession gameSession) {
        gameSession.getPlayers().forEach(player ->
                player.setPoints(getPlayerDistricts(gameSession,
                        player.getId()).stream().mapToLong(District::getValue).sum()));
    }

    private void updateDistrictValue(PhaseEnum phaseEnum, District targetDistrict) {
        Long districtValue = 0L;
        switch (phaseEnum) {
            case EXPANSION_ONE:
            case EXPANSION_TWO:
            case EXPANSION_THREE:
            case EXPANSION_FOUR: {
                districtValue = 100L;
                break;
            }
            case DIVISION: {
                districtValue = 200L;
                break;
            }
            case WAR_ONE:
            case WAR_TWO:
            case WAR_THREE:
            case WAR_FOUR:
            case WAR_FIVE:
            case WAR_SIX:
                districtValue = 300L;
                break;
        }
        targetDistrict.setValue(districtValue);
    }

    private Long targetDistrict(GameSession gameSession, Long playerId, Long districtId) throws GameStateException {
        if (districtId != null) {
            District targetDistrict = gameSession.getDistricts().stream().filter(
                    district -> district.getId().equals(districtId)).findFirst().get();

            switch (gameSession.getCurrentPhase()) {
                case EXPANSION_ONE:
                case EXPANSION_TWO:
                case EXPANSION_THREE:
                case EXPANSION_FOUR:
                case DIVISION: {
                    if (!getInteractableDistrictIdsDuringExpansionAndDivision(gameSession, playerId).contains(districtId)) {
                        throw new GameStateException("Player " + playerId + " can't target district " + districtId);
                    }
                    break;
                }
                case WAR_ONE:
                case WAR_TWO:
                case WAR_THREE:
                case WAR_FOUR:
                case WAR_FIVE:
                case WAR_SIX:
                    if (!getInteractableDistrictsDuringWar(gameSession, playerId).contains(districtId)) {
                        throw new GameStateException("Player " + playerId + " can't target district " + districtId);
                    }
                    break;
            }

            targetDistrict.setInvaderId(playerId);
            log.info("Player " + playerId + " targeted district: " + targetDistrict.getId());
            return targetDistrict.getOwnerId();
        }
        throw new RuntimeException("districtId is null!");
    }

    private void occupyBase(GameSession gameSession, Long playerId, Long districtId) throws GameStateException {
        District targetDistrict = gameSession.getDistricts().stream().filter(
                district -> district.getId().equals(districtId)).findFirst().get();

        if (targetDistrict.getOwnerId() != 0L) {
            throw new GameStateException("District " + targetDistrict.getId() +
                    " is already owned by: " + targetDistrict.getOwnerId());
        }

        if (getDistrictNeighbours(gameSession, districtId).stream().anyMatch(district -> district.getOwnerId() != 0L)) {
            throw new GameStateException("District " + targetDistrict.getId() +
                    " is too close to an enemy base!");
        }

        targetDistrict.setInvaderId(0L);
        targetDistrict.setOwnerId(playerId);
        targetDistrict.setBase(true);
        targetDistrict.setHealth(3);
        targetDistrict.setValue(500L);

        log.info("Player " + playerId + " occupied base district: " + targetDistrict.getId());
    }

    private List<District> getDistrictNeighbours(GameSession gameSession, Long districtId) {
        List<Long> neighbourIds = districtNeighbourRepository.findAllByDistrictId(districtId).stream().map(
                DistrictNeighbour::getNeighbourId).collect(Collectors.toList());
        return gameSession.getDistricts().stream().filter(district ->
                neighbourIds.contains(district.getId())).collect(Collectors.toList());
    }

    private List<Long> getDistrictNeighbourIds(Long districtId) {
        return districtNeighbourRepository.findAllByDistrictId(districtId).stream().map(
                DistrictNeighbour::getNeighbourId).collect(Collectors.toList());
    }

    public Set<Long> getInteractableDistricts(@RequestParam Long playerId, @RequestParam UUID gameSessionId) {
        GameSession gameSession = gameSessions.get(gameSessionId);

        switch (gameSession.getCurrentPhase()) {
            case BASE_DISTRICT_SELECT: {
                return getInteractableDistrictIdsDuringBaseSelection(gameSession, playerId);
            }
            case EXPANSION_ONE:
            case EXPANSION_TWO:
            case EXPANSION_THREE:
            case EXPANSION_FOUR:
            case DIVISION: {
                return getInteractableDistrictIdsDuringExpansionAndDivision(gameSession, playerId);
            }
            case WAR_ONE:
            case WAR_TWO:
            case WAR_THREE:
            case WAR_FOUR:
            case WAR_FIVE:
            case WAR_SIX: {
                return getInteractableDistrictsDuringWar(gameSession, playerId);
            }
            case GAME_OVER: {
                return new HashSet<>();
            }
            default: {
                throw new RuntimeException("Unknown phase: " + gameSession.getCurrentPhase());
            }
        }
    }

    private Set<Long> getInteractableDistrictIdsDuringBaseSelection(GameSession gameSession, Long playerId) {
        List<Long> enemyDistrictsAndNeighbours = new ArrayList<>();

        enemyDistrictsAndNeighbours.addAll(getEnemyDistrictIds(gameSession, playerId));
        getEnemyDistrictIds(gameSession, playerId).stream().forEach(districtId ->
                enemyDistrictsAndNeighbours.addAll(getDistrictNeighbourIds(districtId)));

        Set<Long> baseCandidates = gameSession.getDistricts().stream().map(District::getId).
                collect(Collectors.toSet());
        baseCandidates.removeAll(enemyDistrictsAndNeighbours);
        return baseCandidates;
    }

    private Set<Long> getInteractableDistrictIdsDuringExpansionAndDivision(GameSession gameSession, Long playerId) {
        Set<Long> reachableDistrictIds = getReachableDistrictIds(gameSession, playerId);
        Set<Long> enemyDistrictIds = getEnemyDistrictIds(gameSession, playerId);
        Set<Long> targetedDistrictIds = getTargetedDistrictIds(gameSession);
        reachableDistrictIds.removeAll(enemyDistrictIds);
        reachableDistrictIds.removeAll(targetedDistrictIds);

//        log.info("Player id: " + playerId + " reachableDistrictIds: " + reachableDistrictIds.toString());
        //in case the player is surrounded with enemy districts
        if (reachableDistrictIds.isEmpty()) {
//            log.info("Player with id: " + playerId + " is surrounded, can choose freely.");
            return getUnoccupiedDistrictIds(gameSession);
        }

        return reachableDistrictIds;
    }

    private Set<Long> getInteractableDistrictsDuringWar(GameSession gameSession, Long playerId) {
        return getReachableDistrictIds(gameSession, playerId);
    }

    private Set<Long> getReachableDistrictIds(GameSession gameSession, Long playerId) {
        Set<Long> ownedDistricts = getPlayerDistrictIds(gameSession, playerId);

        Set<Long> reachableDistrictIds = new HashSet<>();
        ownedDistricts.forEach(districtId -> getDistrictNeighbourIds(districtId).forEach(neighbourId -> {
            reachableDistrictIds.add(neighbourId);
        }));
        reachableDistrictIds.removeAll(ownedDistricts);
        return reachableDistrictIds;
    }

    private Set<Long> getUnoccupiedDistrictIds(GameSession gameSession) {
        return gameSession.getDistricts().stream().filter(district ->
                0L == district.getOwnerId()).map(District::getId).collect(Collectors.toSet());
    }

    private Set<District> getPlayerDistricts(GameSession gameSession, Long playerId) {
        return gameSession.getDistricts().stream().filter(district ->
                playerId == district.getOwnerId()).collect(Collectors.toSet());
    }

    private Set<Long> getPlayerDistrictIds(GameSession gameSession, Long playerId) {
        return getPlayerDistricts(gameSession, playerId).stream().map(
                District::getId).collect(Collectors.toSet());
    }

    private Set<Long> getEnemyDistrictIds(GameSession gameSession, Long playerId) {
        return gameSession.getDistricts().stream().filter(district ->
                0L != district.getOwnerId() && !district.getOwnerId().equals(playerId))
                .map(District::getId).collect(Collectors.toSet());
    }

    private Set<Long> getTargetedDistrictIds(GameSession gameSession) {
        return gameSession.getDistricts().stream().filter(district ->
                0L != district.getInvaderId()).map(District::getId).collect(Collectors.toSet());
    }
}
