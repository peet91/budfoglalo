package hu.peet.budfoglalo.core.controller;

import hu.peet.budfoglalo.core.presenter.district.InteractableDistrictPresenter;
import hu.peet.budfoglalo.core.service.GameServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "/api/district")
public class DistrictController {

    @Autowired
    GameServiceImpl gameService;

    @GetMapping(value = "/interactable", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<InteractableDistrictPresenter> getInteractableDistricts(
            @RequestParam Long playerId, @RequestParam UUID gameSessionId) {
        return ResponseEntity.ok(
                new InteractableDistrictPresenter(gameService.getInteractableDistricts(playerId, gameSessionId)));
    }
}