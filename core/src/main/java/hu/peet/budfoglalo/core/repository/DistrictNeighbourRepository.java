package hu.peet.budfoglalo.core.repository;

import hu.peet.budfoglalo.core.model.DistrictNeighbour;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DistrictNeighbourRepository extends JpaRepository<DistrictNeighbour, Long> {
    public List<DistrictNeighbour> findAllByDistrictId(Long districtId);
}
