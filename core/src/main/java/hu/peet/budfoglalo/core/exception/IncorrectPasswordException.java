package hu.peet.budfoglalo.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.security.auth.login.LoginException;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class IncorrectPasswordException extends LoginException {
    public IncorrectPasswordException(final String message) {
        super(message);
    }
}