package hu.peet.budfoglalo.core.presenter.lobby;

import com.fasterxml.jackson.annotation.JsonProperty;
import hu.peet.budfoglalo.core.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlayerPresenter {
    @JsonProperty
    private Long id;

    @JsonProperty
    private String userName;

    @JsonProperty
    private Integer level;

    @JsonProperty
    private Long points;

    private LocalTime lastHeartbeat;

    public PlayerPresenter(User user) {
        this.id = user.getId();
        this.userName = user.getUsername();
        this.level = user.getUserInformation().getLevel();
        this.points = user.getPoints();
        this.lastHeartbeat = LocalTime.now();
    }
}
