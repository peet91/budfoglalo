package hu.peet.budfoglalo.core.service;

import hu.peet.budfoglalo.core.model.TriviaQuestion;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class TriviaQuestionWrapper {
    private TriviaQuestion triviaQuestion;

    private Map<Long, String> playerAnswers;

    public TriviaQuestionWrapper(List<Long> playerIds, TriviaQuestion triviaQuestion) {
        this.triviaQuestion = triviaQuestion;
        playerAnswers = new HashMap<>();
        playerIds.stream().forEach(playerId -> playerAnswers.put(playerId, null));
    }

    public boolean allPlayersAnswered() {
        return playerAnswers.entrySet().stream().filter(
                playerAnswer -> playerAnswer.getValue() != null).count() == playerAnswers.size();
    }

    public List<Long> evaluateAnswers() {
//        System.err.println("Evaluation player answers:");
//        System.err.println(triviaQuestion.getQuestion());
//        System.err.println(triviaQuestion.getAnswers().toString());
//        System.err.println("playerAnswers:");
//        System.err.println(playerAnswers.toString());

        List<Long> correctPlayerIds = playerAnswers.entrySet().stream().filter(
                playerAnswer -> playerAnswer.getValue().equals(triviaQuestion.getAnswers().stream().filter(
                        triviaAnswer -> triviaAnswer.getCorrect()).findFirst().get().getAnswer())).map(
                Map.Entry::getKey).collect(Collectors.toList());
        return correctPlayerIds;
    }
}
