package hu.peet.budfoglalo.core.controller;

import hu.peet.budfoglalo.core.presenter.question.AnswerRequestPresenter;
import hu.peet.budfoglalo.core.presenter.question.GuessingQuestionPresenter;
import hu.peet.budfoglalo.core.presenter.question.TriviaQuestionPresenter;
import hu.peet.budfoglalo.core.service.GameServiceImpl;
import hu.peet.budfoglalo.core.service.QuestionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(value = "/api/question")
public class QuestionController {

    @Autowired
    private QuestionServiceImpl questionService;

    @Autowired
    private GameServiceImpl gameService;

    @GetMapping(value = "/trivia/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TriviaQuestionPresenter> getTriviaQuestion(@RequestParam UUID gameSessionId) {
        return ResponseEntity.ok(new TriviaQuestionPresenter(questionService.getActiveTriviaQuestion(
                gameSessionId).getTriviaQuestion()));
    }

    @PostMapping(value = "/trivia/answer", consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpStatus answerTriviaQuestion(@RequestBody AnswerRequestPresenter answerRequestPresenter) {
        questionService.answerActiveTriviaQuestion(answerRequestPresenter);
        gameService.updateGameState(answerRequestPresenter);
        return HttpStatus.OK;
    }

    @GetMapping(value = "/guessing/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuessingQuestionPresenter> getGuessingQuestion(@RequestParam UUID gameSessionId) {
        return ResponseEntity.ok(new GuessingQuestionPresenter(questionService.getActiveGuessingQuestion(
                gameSessionId).getGuessingQuestion()));
    }

    @PostMapping(value = "/guessing/answer", consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpStatus answerGuessingQuestion(@RequestBody AnswerRequestPresenter answerRequestPresenter) {
        questionService.answerActiveGuessingQuestion(answerRequestPresenter);
        gameService.updateGameState(answerRequestPresenter);
        return HttpStatus.OK;
    }
}
