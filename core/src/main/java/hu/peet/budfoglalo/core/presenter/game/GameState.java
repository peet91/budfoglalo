package hu.peet.budfoglalo.core.presenter.game;

import com.fasterxml.jackson.annotation.JsonProperty;
import hu.peet.budfoglalo.core.enums.PhaseEnum;
import hu.peet.budfoglalo.core.enums.SubPhaseEnum;
import hu.peet.budfoglalo.core.presenter.district.DistrictPresenter;
import hu.peet.budfoglalo.core.presenter.lobby.PlayerPresenter;
import hu.peet.budfoglalo.core.service.GameSession;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GameState {
    @JsonProperty
    private List<DistrictPresenter> districts;

    @JsonProperty
    private PhaseEnum currentPhase;

    @JsonProperty
    private SubPhaseEnum currentSubPhase;

    @JsonProperty
    private List<PlayerPresenter> players;

    @JsonProperty
    private Long activePlayerId;

    public GameState(GameSession gameSession) {
        this.districts = new ArrayList<>();
        gameSession.getDistricts().forEach(district -> this.districts.add(new DistrictPresenter(district)));
        this.currentPhase = gameSession.getCurrentPhase();
        this.currentSubPhase = gameSession.getCurrentSubPhase();
        this.players = new ArrayList<>();
        gameSession.getPlayers().forEach(user -> this.players.add(new PlayerPresenter(user)));
        this.activePlayerId = gameSession.getActivePlayerId();
    }
}
