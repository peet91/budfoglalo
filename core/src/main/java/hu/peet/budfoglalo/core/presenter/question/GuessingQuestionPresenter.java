package hu.peet.budfoglalo.core.presenter.question;

import com.fasterxml.jackson.annotation.JsonProperty;
import hu.peet.budfoglalo.core.model.GuessingQuestion;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GuessingQuestionPresenter {

    @JsonProperty
    private Long questionId;

    @JsonProperty
    private String question;

    public GuessingQuestionPresenter(GuessingQuestion guessingQuestion) {
        this.questionId = guessingQuestion.getId();
        this.question = guessingQuestion.getQuestion();
    }
}
