package hu.peet.budfoglalo.core.controller;

import hu.peet.budfoglalo.core.exception.GameSessionNotFoundException;
import hu.peet.budfoglalo.core.exception.GameStateException;
import hu.peet.budfoglalo.core.presenter.game.GameState;
import hu.peet.budfoglalo.core.presenter.game.GameStateUpdateRequest;
import hu.peet.budfoglalo.core.presenter.game.LeaveGameRequest;
import hu.peet.budfoglalo.core.presenter.lobby.ActivePlayerListPresenter;
import hu.peet.budfoglalo.core.presenter.lobby.CreateLobbyRequest;
import hu.peet.budfoglalo.core.presenter.lobby.LobbiesPresenter;
import hu.peet.budfoglalo.core.presenter.lobby.PlayerReadyRequest;
import hu.peet.budfoglalo.core.service.GameMasterServiceImpl;
import hu.peet.budfoglalo.core.service.GameServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@RestController
@RequestMapping(value = "/api/game")
public class GameController {

    private static final Logger log =
            LoggerFactory.getLogger(GameController.class);

    @Autowired
    private GameMasterServiceImpl gameMasterService;

    @Autowired
    private GameServiceImpl gameService;


    @GetMapping(value = "/active_players")
    public ResponseEntity<ActivePlayerListPresenter> getActivePlayers(@RequestParam Long playerId) {
        return ResponseEntity.ok(new ActivePlayerListPresenter(gameMasterService.getActivePlayers(playerId)));
    }

    @PostMapping(value = "/heartbeat/{playerId}")
    public HttpStatus heartBeat(@PathVariable Long playerId, @RequestParam(required = false) UUID gameSessionId) {
        if (gameSessionId == null) {
            gameMasterService.heartbeat(playerId);
        } else {
            gameService.heartbeat(playerId, gameSessionId);
        }
        return HttpStatus.OK;
    }

    @PostMapping(value = "/create_lobby", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UUID> createLobby(@RequestBody CreateLobbyRequest createLobbyRequest) {
        return ResponseEntity.ok(gameMasterService.createLobby(createLobbyRequest));
    }

    @GetMapping(value = "/lobbies/{playerId}")
    public ResponseEntity<LobbiesPresenter> listPlayerLobbies(@PathVariable Long playerId) {
        return ResponseEntity.ok(new LobbiesPresenter(gameMasterService.listPlayerLobbies(playerId)));
    }

    @PostMapping(value = "/ready", consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpStatus acceptGameInvite(@RequestBody PlayerReadyRequest playerReadyRequest) {

        try {
            gameMasterService.acceptGameRequest(playerReadyRequest);
            return  HttpStatus.OK;
        } catch (ResourceAccessException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping(value = "/deny", consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpStatus denyGameInvite(@RequestBody PlayerReadyRequest playerReadyRequest) {
        try {
            gameMasterService.denyGameRequest(playerReadyRequest);
            return HttpStatus.OK;
        } catch (ResourceAccessException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UUID> getPlayerGame(@RequestParam Long playerId) throws GameSessionNotFoundException {
        if (gameService.getPlayerGame(playerId).isPresent()) {
            return ResponseEntity.ok(gameService.getPlayerGame(playerId).get());
        } else throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED);
    }

    @GetMapping(value = "/state", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GameState> getGameState(@RequestParam Long playerId, @RequestParam UUID gameSessionId) {
        try {
            return ResponseEntity.ok(gameService.getGameState(playerId, gameSessionId));

        } catch (ResourceAccessException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (GameSessionNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.PRECONDITION_FAILED, e.getMessage());
        }
    }

    @PostMapping(value = "/interact", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GameState> interact(@RequestBody GameStateUpdateRequest gameStateUpdateRequest) {
        try {
            return ResponseEntity.ok(gameService.updateGameState(gameStateUpdateRequest));
        } catch (ResourceAccessException rae) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED, rae.getMessage());
        } catch (GameStateException gse) {
            throw new ResponseStatusException(
                    HttpStatus.PRECONDITION_FAILED, gse.getMessage());
        }
    }

    @PostMapping(value = "/leave", consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpStatus leave(@RequestBody LeaveGameRequest leaveGameRequest) {
        try {
            gameService.leaveGame(leaveGameRequest);
            return HttpStatus.OK;
        } catch (ResourceAccessException rae) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED, rae.getMessage());
        } catch (GameStateException gse) {
            throw new ResponseStatusException(
                    HttpStatus.PRECONDITION_FAILED, gse.getMessage());
        }
    }
}
