package hu.peet.budfoglalo.core.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "username", unique = true)
    @Size(max = 255)
    private String username;

    @NotNull
    @Column(name = "password")
    private String password;

    @NotNull
    @Column(nullable = false, unique = true)
    @Email
    @Size(max = 255)
    private String email;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @OneToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_information_id", referencedColumnName = "id")
    private UserInformation userInformation = new UserInformation();

    //strictly game session related, non-persistable data
    @Transient
    private Long points = 0L;

    @Transient
    private LocalTime lastHeartbeat;

    public User(@NotNull @Size(max = 255) String username, @NotNull String password,
                @NotNull @Email @Size(max = 255) String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }
}
