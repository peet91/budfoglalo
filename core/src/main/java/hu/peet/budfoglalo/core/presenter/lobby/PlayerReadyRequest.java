package hu.peet.budfoglalo.core.presenter.lobby;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlayerReadyRequest {

    @JsonProperty
    private Long playerId;

    @JsonProperty()
    private UUID uuid;
}
