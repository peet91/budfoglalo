package hu.peet.budfoglalo.core.presenter.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import hu.peet.budfoglalo.core.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor
public class UserListPresenter {
    @JsonProperty
    private List<UserInformationPresenter> userList;

    @JsonCreator
    public UserListPresenter(List<User> userList) {
        this.userList = new ArrayList<>();
        userList.stream().forEach(user -> this.userList.add(new UserInformationPresenter(user)));
    }
}
