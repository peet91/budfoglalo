package hu.peet.budfoglalo.core.service;

import hu.peet.budfoglalo.core.model.User;
import hu.peet.budfoglalo.core.presenter.lobby.PlayerReadyRequest;
import hu.peet.budfoglalo.core.presenter.lobby.CreateLobbyRequest;
import hu.peet.budfoglalo.core.presenter.lobby.PlayerPresenter;
import hu.peet.budfoglalo.core.service.interfaces.GameMasterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import java.time.Duration;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@EnableScheduling
public class GameMasterServiceImpl implements GameMasterService {

    private static final Logger log =
            LoggerFactory.getLogger(GameMasterService.class);

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private GameServiceImpl gameService;

    private Map<Long, PlayerPresenter> availablePlayers = new HashMap<>();

    private Map<UUID, Lobby> lobbies = new HashMap<>();

    private final int lobbySize = 2;

    @Override
    public void addPlayerToPool(Long userId) {
        User user = userService.getUserById(userId);
        availablePlayers.putIfAbsent(userId, new PlayerPresenter(user));
        log.info("Player with id: " + userId +
                " added to active player pool. Active players:  " + availablePlayers.toString());
    }

    @Override
    public void heartbeat(Long userId) {
//        log.info("heartbeat for user: " + userId);
        if (availablePlayers.containsKey(userId)) {
            availablePlayers.get(userId).setLastHeartbeat(LocalTime.now());
        } else {
            addPlayerToPool(userId);
        }
    }

    @Scheduled(fixedDelay = 5000)
    public void cleanUpActivePlayers() {
//        log.info("cleanUpActivePlayers fires");
        List<Long> playersToRemove = new ArrayList<>();

        availablePlayers.entrySet().forEach(player -> {
            if (Duration.between(player.getValue().getLastHeartbeat(), LocalTime.now()).toMillis() >= 15000) {
                log.info("Removed player " + player.toString() + " due to missing heartbeat!");
                playersToRemove.add(player.getKey());
            }
        });
        playersToRemove.forEach(playerId -> availablePlayers.remove(playerId));
    }

    @Scheduled(fixedDelay = 30000)
    public void cleanUpLobbies() {
//        log.info("cleanUpLobbies fires");
        List<UUID> lobbiesToRemove = new ArrayList<>();

        lobbies.entrySet().stream().forEach(lobby -> {
            if (Duration.between(lobby.getValue().getCreatedTime(), LocalTime.now()).toMinutes() >= 2) {
                log.info("Removed lobby " + lobby.toString() + " due to game invite acceptance timeout!");
                lobbiesToRemove.add(lobby.getKey());
            }
        });
        lobbiesToRemove.forEach(lobbyId -> lobbies.remove(lobbyId));
    }

    @Override
    public List<PlayerPresenter> getActivePlayers(Long userId) {
        return availablePlayers.entrySet().stream().filter(
                activePlayer -> userId != activePlayer.getValue().getId()).map(
                Map.Entry::getValue).collect(Collectors.toList());
    }

    @Override
    public UUID createLobby(CreateLobbyRequest createLobbyRequest) {
        UUID uuid = UUID.randomUUID();
        Lobby lobby = new Lobby(uuid, createLobbyRequest.getPlayerId(), new HashMap<>(), LocalTime.now());
        lobby.addPlayerToLobby(createLobbyRequest.getPlayerId(), true);
        createLobbyRequest.getOpponentIds().stream().forEach(
                opponentId -> lobby.addPlayerToLobby(opponentId, false));
        lobbies.put(uuid, lobby);
        log.info("Player with id: " + createLobbyRequest.getPlayerId() +
                " created game request for players: " + createLobbyRequest.getOpponentIds().toString() +
                " created lobby with id: " + uuid);
        return uuid;
    }

    @Override
    public Map<UUID, Lobby> listPlayerLobbies(Long playerId) {
        return lobbies.entrySet().stream().filter(
                lobby -> lobby.getValue().getPlayers().keySet().contains(playerId)).collect(
                Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @Override
    public void acceptGameRequest(PlayerReadyRequest playerReadyRequest) throws ResourceAccessException {
        if (lobbies.get(playerReadyRequest.getUuid()).getPlayers().containsKey(playerReadyRequest.getPlayerId())) {
            Lobby lobby = lobbies.get(playerReadyRequest.getUuid());
            lobby.readyPlayer(playerReadyRequest.getPlayerId());
            log.info("Player with id: " + playerReadyRequest.getPlayerId() +
                    " accepted game request: " + playerReadyRequest.getUuid());

            if (lobby.isAllPlayersReady()) {
                gameService.initGameSession(playerReadyRequest.getUuid(),
                        new ArrayList<>(lobbies.get(playerReadyRequest.getUuid()).getPlayers().keySet()));
                log.info("Lobby with id: " + playerReadyRequest.getUuid() + " has all players ready, initiating game.");

                lobby.getPlayers().entrySet().forEach(player -> availablePlayers.remove(player));
                lobbies.remove(playerReadyRequest.getUuid());
                log.info("Lobby with id: " + playerReadyRequest.getUuid() + " removed.");
            }
        } else throw new ResourceAccessException("Player " + playerReadyRequest.getPlayerId()
                + "doesn't belong to game:" + playerReadyRequest.getUuid());
    }

    @Override
    public void denyGameRequest(PlayerReadyRequest playerReadyRequest) throws ResourceAccessException {
        if (lobbies.get(playerReadyRequest.getUuid()).getPlayers().containsKey(playerReadyRequest.getPlayerId())) {
            Lobby lobby = lobbies.get(playerReadyRequest.getUuid());
            lobby.removePlayerFromLobby(playerReadyRequest.getPlayerId());
            log.info("Player with id: " + playerReadyRequest.getPlayerId() +
                    " denied game request." + playerReadyRequest.getUuid());

            if (lobby.getPlayers().size() < lobbySize) {
                lobbies.remove(playerReadyRequest.getUuid());
                log.info("Lobby with id: " + playerReadyRequest.getUuid() + " missing players, and gets removed.");
            }
        } else throw new ResourceAccessException("Player " + playerReadyRequest.getPlayerId()
                + "doesn't belong to game:" + playerReadyRequest.getUuid());
    }
}
