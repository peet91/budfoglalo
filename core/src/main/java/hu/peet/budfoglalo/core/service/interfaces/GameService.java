package hu.peet.budfoglalo.core.service.interfaces;

import hu.peet.budfoglalo.core.presenter.game.GameState;
import hu.peet.budfoglalo.core.presenter.game.GameStateUpdateRequest;
import hu.peet.budfoglalo.core.presenter.question.AnswerRequestPresenter;
import org.springframework.web.client.ResourceAccessException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GameService {
    UUID initGameSession(UUID uuid, List<Long> playerIds);

    GameState getGameState(Long playerId, UUID gameSessionId) throws ResourceAccessException;

    GameState updateGameState(GameStateUpdateRequest gameStateUpdateRequest) throws ResourceAccessException;

    GameState updateGameState(AnswerRequestPresenter answerRequestPresenter) throws ResourceAccessException;

    Optional<UUID> getPlayerGame(Long playerId);

    void heartbeat(Long playerId, UUID gameSessionId);
}
