package hu.peet.budfoglalo.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class UserNameOrEmailExistsException extends RuntimeException {
    public UserNameOrEmailExistsException(final String message) {
        super(message);
    }
}