package hu.peet.budfoglalo.core.controller;

import com.google.common.truth.Truth;
import hu.peet.budfoglalo.core.presenter.district.InteractableDistrictPresenter;
import hu.peet.budfoglalo.core.service.GameServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;


@ExtendWith(SpringExtension.class)
public class DistrictControllerTest {

    @InjectMocks
    DistrictController districtController;

    @Mock
    GameServiceImpl gameServiceMock;

    private static InteractableDistrictPresenter interactableDistrictPresenter;

    private static final HashSet<Long> INTERACTABLE_DISTRICTS = new HashSet(Arrays.asList(1L, 2L, 3L));

    @Before
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeEach
    public void initRequests() {

    }

    @Test
    public void getDistrictsTest() {
        Mockito.when(gameServiceMock.getInteractableDistricts(
                ArgumentMatchers.anyLong(), ArgumentMatchers.any(UUID.class)))
                .thenReturn(new HashSet<>(Arrays.asList(1L, 2L, 3L)));
        ResponseEntity<InteractableDistrictPresenter> responseEntity = districtController.
                getInteractableDistricts(1L, UUID.randomUUID());

        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Truth.assertThat(responseEntity.getBody().getInteractableDistricts())
                .containsExactlyElementsIn(INTERACTABLE_DISTRICTS);
    }
}
