package hu.peet.budfoglalo.core.service;

import com.google.common.truth.Truth;
import hu.peet.budfoglalo.core.model.GuessingQuestion;
import hu.peet.budfoglalo.core.repository.GuessingQuestionRepository;
import hu.peet.budfoglalo.core.repository.TriviaQuestionRepository;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
public class GuessingQuestionWrapperTest {

    @InjectMocks
    private QuestionServiceImpl questionService;

    @Mock
    private TriviaQuestionRepository triviaQuestionRepositoryMock;

    @Mock
    private GuessingQuestionRepository guessingQuestionRepositoryMock;

    private static GuessingQuestionWrapper guessingQuestionWrapper;

    private static final Long GUESSING_QUESTION_1_ID = 1L;
    private static final String GUESSING_QUESTION_1_QUESTION = "question";
    private static final Long GUESSING_QUESTION_1_ANSWER = 42L;
    private static GuessingQuestion GUESSING_QUESTION_1 = new GuessingQuestion();

    @Before
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeEach
    public void initQuestion() {
        UUID uuid = UUID.randomUUID();
        GUESSING_QUESTION_1.setId(GUESSING_QUESTION_1_ID);
        GUESSING_QUESTION_1.setQuestion(GUESSING_QUESTION_1_QUESTION);
        GUESSING_QUESTION_1.setAnswer(GUESSING_QUESTION_1_ANSWER);

        Mockito.when(triviaQuestionRepositoryMock.count()).thenReturn(10L);
        Mockito.when(guessingQuestionRepositoryMock.count()).thenReturn(10L);

        Mockito.when(guessingQuestionRepositoryMock.findAll(ArgumentMatchers.isA(Pageable.class)))
                .thenReturn(new PageImpl<>(Arrays.asList(GUESSING_QUESTION_1)));

        questionService.addActiveGuessingQuestion(uuid, Arrays.asList(1L, 2L, 3L));
        guessingQuestionWrapper = questionService.getActiveGuessingQuestion(uuid);
    }

    @Test
    public void guessingQuestionAllPlayersAnsweredTest() {
        assertFalse(guessingQuestionWrapper.allPlayersAnswered());
        guessingQuestionWrapper.getPlayerAnswers().put(1L, "12424356");
        assertFalse(guessingQuestionWrapper.allPlayersAnswered());
        guessingQuestionWrapper.getPlayerAnswers().put(2L, "1345t345");
        guessingQuestionWrapper.getPlayerAnswers().put(3L, "452z425z");
        assertTrue(guessingQuestionWrapper.allPlayersAnswered());
    }

    @Test
    public void guessingQuestionEvaluationTest() {
        guessingQuestionWrapper.getPlayerAnswers().put(1L,
                guessingQuestionWrapper.getGuessingQuestion().getAnswer().toString());
        guessingQuestionWrapper.getPlayerAnswers().put(2L, "234526");
        guessingQuestionWrapper.getPlayerAnswers().put(3L, "8975432");

        Long winnerId = guessingQuestionWrapper.evaluateAnswers();

        assertEquals(1L, winnerId);
    }

    @Test
    public void equalAnswerRandomWinnerTest() {
        guessingQuestionWrapper.getPlayerAnswers().put(1L,
                guessingQuestionWrapper.getGuessingQuestion().getAnswer().toString());
        guessingQuestionWrapper.getPlayerAnswers().put(2L,
                guessingQuestionWrapper.getGuessingQuestion().getAnswer().toString());
        guessingQuestionWrapper.getPlayerAnswers().put(3L, "8975432");

        List<Long> winnerIdList = new ArrayList<>();
        for (int i = 0; i < 20; ++i) {
            winnerIdList.add(guessingQuestionWrapper.evaluateAnswers());
        }
        Truth.assertThat(winnerIdList).contains(1L);
        Truth.assertThat(winnerIdList).contains(2L);
        Truth.assertThat(winnerIdList).doesNotContain(3L);
    }
}
