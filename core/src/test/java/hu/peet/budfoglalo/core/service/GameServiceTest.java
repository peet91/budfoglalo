package hu.peet.budfoglalo.core.service;

import hu.peet.budfoglalo.core.exception.GameSessionNotFoundException;
import hu.peet.budfoglalo.core.exception.GameStateException;
import hu.peet.budfoglalo.core.model.District;
import hu.peet.budfoglalo.core.model.User;
import hu.peet.budfoglalo.core.presenter.game.GameStateUpdateRequest;
import hu.peet.budfoglalo.core.repository.DistrictNeighbourRepository;
import hu.peet.budfoglalo.core.repository.DistrictRepository;
import hu.peet.budfoglalo.core.repository.LevelRepository;
import hu.peet.budfoglalo.core.repository.UserRepository;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.ResourceAccessException;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
public class GameServiceTest {

    @InjectMocks
    private GameServiceImpl gameService;

    @Mock
    private QuestionServiceImpl questionServiceMock;

    @Mock
    private UserRepository userRepositoryMock;

    @Mock
    private LevelRepository levelRepositoryMock;

    @Mock
    private DistrictRepository districtRepositoryMock;

    @Mock
    private DistrictNeighbourRepository districtNeighbourRepositoryMock;

    private static final Long USER_1_ID = 1L;
    private static User USER_1;
    private static final Long USER_2_ID = 2L;
    private static User USER_2;
    private static final Long USER_3_ID = 3L;
    private static User USER_3;
    private static long DISTRICT_1_ID = 11;
    private static District DISTRICT_1;


    @Before
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeEach
    public void initUser() {
        USER_1 = new User();
        USER_1.setId(USER_1_ID);
        USER_2 = new User();
        USER_2.setId(USER_2_ID);
        USER_3 = new User();
        USER_3.setId(USER_3_ID);
        DISTRICT_1 = new District();
        DISTRICT_1.setId(DISTRICT_1_ID);
    }

    @Test
    public void createGameSessionTest() {
        UUID uuid = UUID.randomUUID();

        assertThrows(GameSessionNotFoundException.class, () -> gameService.getGameState(1L, uuid));
        assertThrows(GameSessionNotFoundException.class, () -> gameService.getGameState(2L, uuid));

        Mockito.when(userRepositoryMock.findById(1L)).thenReturn(Optional.of(USER_1));
        Mockito.when(userRepositoryMock.findById(2L)).thenReturn(Optional.of(USER_2));
        Mockito.when(userRepositoryMock.findById(3L)).thenReturn(Optional.of(USER_3));

        gameService.initGameSession(uuid, Arrays.asList(1L, 2L));

        assertTrue(gameService.getPlayerGame(1L).isPresent());
        assertTrue(gameService.getPlayerGame(2L).isPresent());

        assertNotNull(gameService.getGameState(1L, uuid));
        assertNotNull(gameService.getGameState(2L, uuid));
    }

    @Test
    public void getGameSessionTest() {
        UUID uuid = UUID.randomUUID();

        assertThrows(GameSessionNotFoundException.class, () -> gameService.getGameState(1L, uuid));
        assertThrows(GameSessionNotFoundException.class, () -> gameService.getGameState(2L, uuid));

        Mockito.when(userRepositoryMock.findById(1L)).thenReturn(Optional.of(USER_1));
        Mockito.when(userRepositoryMock.findById(2L)).thenReturn(Optional.of(USER_2));
        Mockito.when(userRepositoryMock.findById(3L)).thenReturn(Optional.of(USER_3));

        gameService.initGameSession(uuid, Arrays.asList(1L, 2L));

        assertTrue(gameService.getPlayerGame(1L).isPresent());
        assertTrue(gameService.getPlayerGame(2L).isPresent());

        assertNotNull(gameService.getGameState(1L, uuid));
        assertNotNull(gameService.getGameState(2L, uuid));
    }

    @Test
    public void getGameStateTest() {
        UUID uuid = UUID.randomUUID();

        assertThrows(GameSessionNotFoundException.class, () -> gameService.getGameState(1L, uuid));
        assertThrows(GameSessionNotFoundException.class, () -> gameService.getGameState(2L, uuid));

        Mockito.when(userRepositoryMock.findById(1L)).thenReturn(Optional.of(USER_1));
        Mockito.when(userRepositoryMock.findById(2L)).thenReturn(Optional.of(USER_2));
        Mockito.when(userRepositoryMock.findById(3L)).thenReturn(Optional.of(USER_3));

        gameService.initGameSession(uuid, Arrays.asList(1L, 2L));

        assertNotNull(gameService.getGameState(1L, uuid));
        assertNotNull(gameService.getGameState(2L, uuid));
    }

    @Test
    public void getGameStateForRestrictedGameTest() {
        UUID uuid = UUID.randomUUID();

        assertThrows(GameSessionNotFoundException.class, () -> gameService.getGameState(1L, uuid));
        assertThrows(GameSessionNotFoundException.class, () -> gameService.getGameState(2L, uuid));

        Mockito.when(userRepositoryMock.findById(1L)).thenReturn(Optional.of(USER_1));
        Mockito.when(userRepositoryMock.findById(2L)).thenReturn(Optional.of(USER_2));
        Mockito.when(userRepositoryMock.findById(3L)).thenReturn(Optional.of(USER_3));

        gameService.initGameSession(uuid, Arrays.asList(1L, 2L));

        assertFalse(gameService.getPlayerGame(4L).isPresent());

        assertThrows(ResourceAccessException.class, () -> gameService.getGameState(4L, uuid));
    }

    @Test
    public void updateGameStateTest() {
        UUID uuid = UUID.randomUUID();

        Mockito.when(userRepositoryMock.findById(1L)).thenReturn(Optional.of(USER_1));
        Mockito.when(userRepositoryMock.findById(2L)).thenReturn(Optional.of(USER_2));
        Mockito.when(userRepositoryMock.findById(3L)).thenReturn(Optional.of(USER_3));

        Mockito.when(districtRepositoryMock.findAll()).thenReturn(Arrays.asList(DISTRICT_1));

        gameService.initGameSession(uuid, Arrays.asList(1L, 2L));

        GameStateUpdateRequest gameStateUpdateRequest = new GameStateUpdateRequest();
        gameStateUpdateRequest.setPlayerId(gameService.getGameState(USER_1_ID, uuid).getActivePlayerId());
        gameStateUpdateRequest.setUuid(uuid);
        gameStateUpdateRequest.setUpdatedDistrictId(11L);

        assertNotNull(gameService.updateGameState(gameStateUpdateRequest));
    }

    @Test
    public void notActivePlayerUpdateGameStateTest() {
        UUID uuid = UUID.randomUUID();

        Mockito.when(userRepositoryMock.findById(1L)).thenReturn(Optional.of(USER_1));
        Mockito.when(userRepositoryMock.findById(2L)).thenReturn(Optional.of(USER_2));
        Mockito.when(userRepositoryMock.findById(3L)).thenReturn(Optional.of(USER_3));

        gameService.initGameSession(uuid, Arrays.asList(1L, 2L));

        GameStateUpdateRequest gameStateUpdateRequest = new GameStateUpdateRequest();
        gameStateUpdateRequest.setPlayerId(gameService.getGameState(1L, uuid).getPlayers().stream()
                .filter(playerPresenter -> !playerPresenter.getId().equals(
                        gameService.getGameState(1L, uuid).getActivePlayerId())).findFirst().get().getId());
        gameStateUpdateRequest.setUuid(uuid);
        gameStateUpdateRequest.setUpdatedDistrictId(11L);

        Mockito.when(districtRepositoryMock.findById(11L)).thenReturn(Optional.of(DISTRICT_1));

        assertThrows(GameStateException.class, () -> gameService.updateGameState(gameStateUpdateRequest));
    }

    @Test
    public void updateGameStateForRestrictedGameTest() {
        UUID uuid = UUID.randomUUID();

        Mockito.when(userRepositoryMock.findById(1L)).thenReturn(Optional.of(USER_1));
        Mockito.when(userRepositoryMock.findById(2L)).thenReturn(Optional.of(USER_2));
        Mockito.when(userRepositoryMock.findById(3L)).thenReturn(Optional.of(USER_3));

        gameService.initGameSession(uuid, Arrays.asList(1L, 2L));

        GameStateUpdateRequest gameStateUpdateRequest = new GameStateUpdateRequest();
        gameStateUpdateRequest.setPlayerId(4L);
        gameStateUpdateRequest.setUuid(uuid);
        gameStateUpdateRequest.setUpdatedDistrictId(11L);

        assertThrows(ResourceAccessException.class, () -> gameService.updateGameState(gameStateUpdateRequest));
    }
}