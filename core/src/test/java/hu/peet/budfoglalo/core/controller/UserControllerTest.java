package hu.peet.budfoglalo.core.controller;

import hu.peet.budfoglalo.core.enums.RoleEnum;
import hu.peet.budfoglalo.core.exception.UserNameOrEmailExistsException;
import hu.peet.budfoglalo.core.presenter.user.LoginRequest;
import hu.peet.budfoglalo.core.presenter.user.RegistrationRequest;
import hu.peet.budfoglalo.core.presenter.user.UserRegistrationResponse;
import hu.peet.budfoglalo.core.security.JwtResponse;
import hu.peet.budfoglalo.core.service.GameMasterServiceImpl;
import hu.peet.budfoglalo.core.service.UserServiceImpl;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
public class UserControllerTest {

    @InjectMocks
    UserController userController;

    @Mock
    UserServiceImpl userServiceMock;

    @Mock
    GameMasterServiceImpl gameMasterServiceMock;

    private static final String USERNAME_1 = "demoUser1";
    private static final String PASSWORD_1 = "Almafa123.";
    private static LoginRequest LOGIN_REQUEST_1;

    private static final String USERNAME_2 = "demoUser6";
    private static final String PASSWORD_2 = "Almafa123.";
    private static final String E_MAIL_1 = "test123@email.com";
    private static RegistrationRequest REGISTRATION_REQUEST_1;

    private static final List<String> ROLES = Collections.singletonList("ROLE_USER");
    private static JwtResponse JWT_RESPONSE;

    private static final String INVALID_LOGIN_MESSAGE = "Incorrect username or password!";
    private static final String USERNAME_EXISTS_MESSAGE = "Error: Username is already taken!";
    private static final String EMAIL_EXISTS_MESSAGE = "Error: Email is already in use!";

    @Before
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeEach
    public void initRequests() {
        LOGIN_REQUEST_1 = new LoginRequest();
        LOGIN_REQUEST_1.setUsername(USERNAME_1);
        LOGIN_REQUEST_1.setPassword(PASSWORD_1);

        REGISTRATION_REQUEST_1 = new RegistrationRequest();
        REGISTRATION_REQUEST_1.setUsername(USERNAME_2);
        REGISTRATION_REQUEST_1.setPassword(PASSWORD_2);
        REGISTRATION_REQUEST_1.setEmail(E_MAIL_1);

        JWT_RESPONSE = new JwtResponse();
        JWT_RESPONSE.setUsername(USERNAME_1);
        JWT_RESPONSE.setRoles(ROLES);
    }

    @Test
    public void validLoginTest() {
        Mockito.when(userServiceMock.login(LOGIN_REQUEST_1)).thenReturn(JWT_RESPONSE);
        ResponseEntity<JwtResponse> loginResponse = userController.login(LOGIN_REQUEST_1);

        assertEquals(HttpStatus.OK, loginResponse.getStatusCode());
        assertEquals(LOGIN_REQUEST_1.getUsername(), loginResponse.getBody().getUsername());
        assertEquals(1, loginResponse.getBody().getRoles().size());
        assertEquals(RoleEnum.ROLE_USER.toString(), loginResponse.getBody().getRoles().get(0));
    }

    @Test
    public void invalidLoginTest() {
        Mockito.when(userServiceMock.login(LOGIN_REQUEST_1)).thenThrow(new ResponseStatusException(
                HttpStatus.UNAUTHORIZED, "Incorrect username or password!"));

        ResponseStatusException e =
                assertThrows(ResponseStatusException.class, () -> userController.login(LOGIN_REQUEST_1));

        assertEquals(HttpStatus.UNAUTHORIZED, e.getStatus());
        assertEquals(INVALID_LOGIN_MESSAGE, e.getReason());
    }

    @Test
    public void registrationTest() {
        Mockito.when(userServiceMock.register(REGISTRATION_REQUEST_1))
                .thenReturn(new UserRegistrationResponse(2));

        ResponseEntity<UserRegistrationResponse> registrationResponse = userController.register(REGISTRATION_REQUEST_1);

        assertEquals(HttpStatus.OK, registrationResponse.getStatusCode());
    }

    @Test
    public void usernameTakenTest() {
        Mockito.when(userServiceMock.register(REGISTRATION_REQUEST_1))
                .thenThrow(new UserNameOrEmailExistsException(
                        "Error: Username is already taken!"));

        ResponseStatusException e =
                assertThrows(ResponseStatusException.class, () -> userController.register(REGISTRATION_REQUEST_1));

        assertEquals(HttpStatus.CONFLICT, e.getStatus());
        assertEquals(USERNAME_EXISTS_MESSAGE, e.getReason());
    }

    @Test
    public void emailTakenTest() {
        Mockito.when(userServiceMock.register(REGISTRATION_REQUEST_1))
                .thenThrow(new UserNameOrEmailExistsException(
                        "Error: Email is already in use!"));

        ResponseStatusException e =
                assertThrows(ResponseStatusException.class, () -> userController.register(REGISTRATION_REQUEST_1));

        assertEquals(HttpStatus.CONFLICT, e.getStatus());
        assertEquals(EMAIL_EXISTS_MESSAGE, e.getReason());
    }
}
