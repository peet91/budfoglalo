package hu.peet.budfoglalo.core.service;

import com.google.common.truth.Truth;
import hu.peet.budfoglalo.core.model.User;
import hu.peet.budfoglalo.core.presenter.lobby.CreateLobbyRequest;
import hu.peet.budfoglalo.core.presenter.lobby.PlayerReadyRequest;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.ResourceAccessException;

import java.util.Arrays;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
public class GameMasterServiceTest {

    @InjectMocks
    private GameMasterServiceImpl gameMasterService;

    @Mock
    private UserServiceImpl userServiceMock;

    @Mock
    private GameServiceImpl gameServiceMock;

    private static final Long USER_1_ID = 1L;
    private static User USER_1;
    private static final Long USER_2_ID = 2L;
    private static User USER_2;
    private static final Long USER_3_ID = 3L;
    private static User USER_3;
    private static final Long USER_4_ID = 4L;
    @Before
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeEach
    public void initUser() {
        USER_1 = new User();
        USER_1.setId(USER_1_ID);
        USER_2 = new User();
        USER_2.setId(USER_2_ID);
        USER_3 = new User();
        USER_3.setId(USER_3_ID);
    }

    @Test
    public void addPlayerToPoolTest() {
        Mockito.when(userServiceMock.getUserById(USER_1_ID)).thenReturn(USER_1);

        gameMasterService.addPlayerToPool(USER_1_ID);
        assertTrue(gameMasterService.getActivePlayers(USER_2_ID).stream().anyMatch(playerPresenter ->
                playerPresenter.getId().equals(USER_1_ID)));
    }

    @Test
    public void inactivePlayerCleanUpTest() {
        Mockito.when(userServiceMock.getUserById(USER_1_ID)).thenReturn(USER_1);

        gameMasterService.addPlayerToPool(USER_1_ID);
        try {
            Thread.sleep(16000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        gameMasterService.cleanUpActivePlayers();
        assertFalse(gameMasterService.getActivePlayers(USER_2_ID).stream().anyMatch(playerPresenter ->
                playerPresenter.getId().equals(USER_1_ID)));
    }

    @Test
    public void createLobbyTest() {
        CreateLobbyRequest createLobbyRequest = new CreateLobbyRequest(USER_1_ID, Arrays.asList(USER_2_ID, USER_3_ID));
        UUID lobbyId = gameMasterService.createLobby(createLobbyRequest);
        assertFalse(gameMasterService.listPlayerLobbies(USER_1_ID).isEmpty());
        assertFalse(gameMasterService.listPlayerLobbies(USER_2_ID).isEmpty());
        assertFalse(gameMasterService.listPlayerLobbies(USER_3_ID).isEmpty());

        assertNotNull(gameMasterService.listPlayerLobbies(USER_1_ID).get(lobbyId));
        Lobby lobby = gameMasterService.listPlayerLobbies(USER_1_ID).get(lobbyId);
        assertEquals(lobby.getCreatorId(), USER_1_ID);
        assertTrue(lobby.getPlayers().keySet().contains(USER_1_ID));
        Truth.assertThat(lobby.getPlayers().keySet()).containsExactlyElementsIn(Arrays.asList(USER_1_ID, USER_2_ID, USER_3_ID));
    }

    @Test
    public void readyPlayerTest() {
        CreateLobbyRequest createLobbyRequest = new CreateLobbyRequest(USER_1_ID, Arrays.asList(USER_2_ID, USER_3_ID));
        UUID lobbyId = gameMasterService.createLobby(createLobbyRequest);
        PlayerReadyRequest playerReadyRequest = new PlayerReadyRequest();
        playerReadyRequest.setPlayerId(USER_2_ID);
        playerReadyRequest.setUuid(lobbyId);
        gameMasterService.acceptGameRequest(playerReadyRequest);
        Lobby lobby = gameMasterService.listPlayerLobbies(USER_1_ID).get(lobbyId);
        assertTrue(lobby.getPlayers().get(USER_2_ID));
        assertFalse(lobby.getPlayers().get(USER_3_ID));
        assertFalse(lobby.isAllPlayersReady());
    }

    @Test()
    public void readyPlayerForRestrictedLobbyTest() {
        CreateLobbyRequest createLobbyRequest = new CreateLobbyRequest(USER_1_ID, Arrays.asList(USER_2_ID, USER_3_ID));
        UUID lobbyId = gameMasterService.createLobby(createLobbyRequest);
        PlayerReadyRequest playerReadyRequest = new PlayerReadyRequest();
        playerReadyRequest.setPlayerId(USER_4_ID);
        playerReadyRequest.setUuid(lobbyId);
        assertThrows(ResourceAccessException.class, () -> gameMasterService.acceptGameRequest(playerReadyRequest));
    }


    @Test
    public void playerDenyTest() {
        CreateLobbyRequest createLobbyRequest = new CreateLobbyRequest(USER_1_ID, Arrays.asList(USER_2_ID, USER_3_ID));
        UUID lobbyId = gameMasterService.createLobby(createLobbyRequest);
        Lobby lobby = gameMasterService.listPlayerLobbies(USER_1_ID).get(lobbyId);

        assertFalse(lobby.getPlayers().get(USER_2_ID));
        assertFalse(lobby.getPlayers().get(USER_3_ID));

        PlayerReadyRequest playerReadyRequest = new PlayerReadyRequest();
        playerReadyRequest.setPlayerId(USER_3_ID);
        playerReadyRequest.setUuid(lobbyId);
        gameMasterService.denyGameRequest(playerReadyRequest);

        assertFalse(lobby.getPlayers().get(USER_2_ID));
        assertFalse(lobby.getPlayers().containsKey(USER_3_ID));
    }

    @Test
    public void playerDenyForRestrictedLobbyTest() {
        CreateLobbyRequest createLobbyRequest = new CreateLobbyRequest(USER_1_ID, Arrays.asList(USER_2_ID, USER_3_ID));
        UUID lobbyId = gameMasterService.createLobby(createLobbyRequest);
        PlayerReadyRequest playerReadyRequest = new PlayerReadyRequest();
        playerReadyRequest.setPlayerId(USER_4_ID);
        playerReadyRequest.setUuid(lobbyId);
        assertThrows(ResourceAccessException.class, () -> gameMasterService.denyGameRequest(playerReadyRequest));
    }


    @Test
    public void lobbyRemovalTest() {
        CreateLobbyRequest createLobbyRequest = new CreateLobbyRequest(USER_1_ID, Arrays.asList(USER_2_ID));
        UUID lobbyId = gameMasterService.createLobby(createLobbyRequest);
        Lobby lobby = gameMasterService.listPlayerLobbies(USER_1_ID).get(lobbyId);

        assertFalse(lobby.getPlayers().get(USER_2_ID));

        PlayerReadyRequest playerReadyRequest = new PlayerReadyRequest();
        playerReadyRequest.setPlayerId(USER_2_ID);
        playerReadyRequest.setUuid(lobbyId);
        gameMasterService.denyGameRequest(playerReadyRequest);

        assertNull(gameMasterService.listPlayerLobbies(USER_1_ID).get(lobbyId));
        assertNull(gameMasterService.listPlayerLobbies(USER_2_ID).get(lobbyId));
    }

    @Test
    public void createGameSessionTest() {
        CreateLobbyRequest createLobbyRequest = new CreateLobbyRequest(USER_1_ID, Arrays.asList(USER_2_ID, USER_3_ID));
        UUID lobbyId = gameMasterService.createLobby(createLobbyRequest);
        PlayerReadyRequest playerReadyRequest = new PlayerReadyRequest();
        playerReadyRequest.setPlayerId(USER_2_ID);
        playerReadyRequest.setUuid(lobbyId);
        gameMasterService.acceptGameRequest(playerReadyRequest);

        Lobby lobby = gameMasterService.listPlayerLobbies(USER_1_ID).get(lobbyId);
        assertFalse(lobby.isAllPlayersReady());

        playerReadyRequest.setPlayerId(USER_3_ID);
        gameMasterService.acceptGameRequest(playerReadyRequest);

        assertNull(gameMasterService.listPlayerLobbies(USER_1_ID).get(lobbyId));
    }
}
