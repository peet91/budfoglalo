package hu.peet.budfoglalo.core.service;

import hu.peet.budfoglalo.core.model.TriviaAnswer;
import hu.peet.budfoglalo.core.model.TriviaQuestion;
import hu.peet.budfoglalo.core.repository.GuessingQuestionRepository;
import hu.peet.budfoglalo.core.repository.TriviaQuestionRepository;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
public class TriviaQuestionWrapperTest {

    @InjectMocks
    private QuestionServiceImpl questionService;

    @Mock
    private TriviaQuestionRepository triviaQuestionRepositoryMock;

    @Mock
    private GuessingQuestionRepository guessingQuestionRepositoryMock;

    private static TriviaQuestionWrapper triviaQuestionWrapper;

    private static final Long TRIVIA_QUESTION_1_ID = 1L;
    private static final String TRIVIA_QUESTION_1_QUESTION = "question";
    private static TriviaQuestion TRIVIA_QUESTION_1 = new TriviaQuestion();
    private static final Long TRIVIA_ANSWER_1_ID = 1L;
    private static final String TRIVIA_ANSWER_1_ANSWER = "wrong";
    private static final Boolean TRIVIA_ANSWER_1_CORRECT = false;
    private static final Long TRIVIA_ANSWER_1_QUESTION_ID = 1L;
    private static TriviaAnswer TRIVIA_ANSWER_1 = new TriviaAnswer();
    private static final Long TRIVIA_ANSWER_2_ID = 1L;
    private static final String TRIVIA_ANSWER_2_ANSWER = "wrong";
    private static final Boolean TRIVIA_ANSWER_2_CORRECT = false;
    private static final Long TRIVIA_ANSWER_2_QUESTION_ID = 1L;
    private static TriviaAnswer TRIVIA_ANSWER_2 = new TriviaAnswer();
    private static final Long TRIVIA_ANSWER_3_ID = 1L;
    private static final String TRIVIA_ANSWER_3_ANSWER = "right";
    private static final Boolean TRIVIA_ANSWER_3_CORRECT = true;
    private static final Long TRIVIA_ANSWER_3_QUESTION_ID = 1L;
    private static TriviaAnswer TRIVIA_ANSWER_3 = new TriviaAnswer();

    @Before
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeEach
    public void initQuestion() {
        UUID uuid = UUID.randomUUID();
        TRIVIA_QUESTION_1.setId(TRIVIA_QUESTION_1_ID);
        TRIVIA_QUESTION_1.setQuestion(TRIVIA_QUESTION_1_QUESTION);
        TRIVIA_ANSWER_1.setId(TRIVIA_ANSWER_1_ID);
        TRIVIA_ANSWER_1.setAnswer(TRIVIA_ANSWER_1_ANSWER);
        TRIVIA_ANSWER_1.setCorrect(TRIVIA_ANSWER_1_CORRECT);
        TRIVIA_ANSWER_1.setQuestionId(TRIVIA_ANSWER_1_QUESTION_ID);
        TRIVIA_ANSWER_2.setId(TRIVIA_ANSWER_2_ID);
        TRIVIA_ANSWER_2.setAnswer(TRIVIA_ANSWER_2_ANSWER);
        TRIVIA_ANSWER_2.setCorrect(TRIVIA_ANSWER_2_CORRECT);
        TRIVIA_ANSWER_2.setQuestionId(TRIVIA_ANSWER_2_QUESTION_ID);
        TRIVIA_ANSWER_3.setId(TRIVIA_ANSWER_3_ID);
        TRIVIA_ANSWER_3.setAnswer(TRIVIA_ANSWER_3_ANSWER);
        TRIVIA_ANSWER_3.setCorrect(TRIVIA_ANSWER_3_CORRECT);
        TRIVIA_ANSWER_3.setQuestionId(TRIVIA_ANSWER_3_QUESTION_ID);

        TRIVIA_QUESTION_1.setAnswers(Arrays.asList(TRIVIA_ANSWER_1, TRIVIA_ANSWER_2, TRIVIA_ANSWER_3));

        Mockito.when(triviaQuestionRepositoryMock.count()).thenReturn(10L);
        Mockito.when(triviaQuestionRepositoryMock.count()).thenReturn(10L);

        Mockito.when(triviaQuestionRepositoryMock.findAll(ArgumentMatchers.isA(Pageable.class)))
                .thenReturn(new PageImpl<>(Arrays.asList(TRIVIA_QUESTION_1)));

        questionService.addActiveTriviaQuestion(uuid, Arrays.asList(1L, 2L, 3L));
        triviaQuestionWrapper = questionService.getActiveTriviaQuestion(uuid);
    }

    @Test
    public void triviaAllPlayersAnsweredTest() {
        assertFalse(triviaQuestionWrapper.allPlayersAnswered());
        triviaQuestionWrapper.getPlayerAnswers().put(1L, "answer1");
        assertFalse(triviaQuestionWrapper.allPlayersAnswered());
        triviaQuestionWrapper.getPlayerAnswers().put(2L, "answer2");
        triviaQuestionWrapper.getPlayerAnswers().put(3L, "answer3");
        assertTrue(triviaQuestionWrapper.allPlayersAnswered());
    }

    @Test
    public void triviaEvaluationTest() {
        triviaQuestionWrapper.getPlayerAnswers().put(1L, "wrong1");
        triviaQuestionWrapper.getPlayerAnswers().put(2L, "wrong2");
        triviaQuestionWrapper.getPlayerAnswers().put(3L,
                triviaQuestionWrapper.getTriviaQuestion().getAnswers().stream().filter(triviaAnswer ->
                        triviaAnswer.getCorrect()).findFirst().get().getAnswer());

        List<Long> winners = triviaQuestionWrapper.evaluateAnswers();

        assertEquals(1, winners.size());
        assertEquals(3L, winners.get(0));
    }
}
