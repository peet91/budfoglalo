echo "Building budfoglalo-core"
docker build -f core/Dockerfile --no-cache -t peet91/budfoglalo-core:latest --target core .

echo "Building budfoglalo-client"
docker build -f client/Dockerfile --no-cache -t peet91/budfoglalo-client:latest --target client .